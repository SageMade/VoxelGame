NOTE:
 - In order to actually run via the debugger, you must set the working directory to $(OutDir) instead of $(ProjectDir)
   - Properties -> Debugging -> Working Directory
   - This may already be set if the .user file is present (will not be on git)

   Use the sliders to control the bloom and specular components
   There is also a checkbox under lighting options to enable and disable normal mapping.

CONTROLS:
 - W/A/S/D to move camera
 - Arrow Keys to rotate camera
 - F to play pop sound
 - P to toggle the theme song
 - F2 to change display mode
 - F4 to reload all shaders

 - <Removed> F1 for wireframe
 - <Removed> F3 to display ligting volumes