#pragma once
#include "imgui/imgui.h"

#include <cstdint>

namespace imgui_impl {

	void ImGuiInit(uint32_t width, uint32_t height);
	void ImGuiDraw(ImDrawData* draw_data);

}