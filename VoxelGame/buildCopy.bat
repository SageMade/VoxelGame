echo off
set OutputPath=%1
set %PROCESSOR_ARCHITECTURE=%2
xcopy.exe "res\*" "%OutputPath%"  /e /c /f /y /s
xcopy.exe "dependencies\%PROCESSOR_ARCHITECTURE%\*" "%OutputPath%"  /e /c /f /y /s