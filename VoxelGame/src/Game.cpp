﻿/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Manages the core gameplay loop and initialization
	Modified by:
		Daniel Munusami
		Ivan Fedorov
			March 19, 2018 
				- Added loading lights from file
				- Added controls for dynamic lights
*/
#include "Game.h"

#include <time.h>

#include "tools\WorldGen.h"

#include "graphics\MeshData.h"
#include "gameplay\input\InputManager.h"

#include "tools/ThreadPool.h"
#include "ServiceProvider.h"

#include "graphics\RenderTarget.h"
#include "graphics\Texture3D.h"
#include "graphics\Texture1D.h"
#include "graphics\CommonTools.h"
#include "graphics\NoShadowLight.h"

#include "tools/AttribPool.h"

#include "tools/GeoUtils.h"

#include <iostream>

#include <imgui\imgui.h>
#include <imgui\imgui_impl.h>
#include "glad\glad.h"
#include "graphics\particles\ParticleEffect.h"
#include "graphics\particles\Renderer.h"
#include "graphics\particles\ParticleLayerSettings.h"
#include "graphics\ShadowCastLight.h"
#include "graphics\particles\TextureCollection.h"

#define REFRESH_RATE 1.0f / 60.0f

using namespace audio;
using namespace gameplay;
using namespace gameplay::input;
using namespace graphics;

enum DisplayMode {
	DeferredBloom     = 0x0,
	DeferredHighlight = 0x1,
	DeferredBlurred   = 0x2,
	Deferred          = 0x3,
	Albedo            = 0x4,
	Normals           = 0x5,
	ViewPos           = 0x6,
	LightingDiffuse   = 0x7,
	LightingSpecular  = 0x8,
	LinearDepth       = 0x9,
	LightDepth        = 0xA,
	 
	DISPM_MAX         = LightDepth
};

struct DELETE_ME_LATER {
	Mesh*	  Model;
	glm::vec3 Position;
	void(*Update)(DELETE_ME_LATER*, float);
};

typedef std::map<std::string, DELETE_ME_LATER> fml_replace;

template <typename ... TArgs>
std::string format(const char* fmt, TArgs... args) {
	size_t size = snprintf(nullptr, 0, fmt, args...);
	char* result = new char[size];
	sprintf(result, fmt, args...);
	std::string resultStr = result;
	return resultStr;
}


#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

#define GL_TIME_ELAPSED​ 0x88BF

bool dynamicLights = false;

void Toggle(tools::AttribPool* pool, const std::string& name) {
	pool->Set<bool>(name, !pool->Get<bool>(name));
}

Game::Game() {
	FILE_LOG(logDEBUG) << "Opening GLFW window...";
	myWindow = new Window(WINDOW_WIDTH, WINDOW_HEIGHT, "Voxel Game", false, true);
	FILE_LOG(logDEBUG) << "Window has been opened!";
	myWindow->AddResizeCallback([this](void* ctx, uint32_t w, uint32_t h) { __Resized(ctx, w, h); });

	InputManager::AddKeyEventCallback([](void*, int key, int sc, ButtonState state, int mods) {

		ImGuiIO& ctx = ImGui::GetIO();
		ctx.KeysDown[key] = state != ButtonReleased;

		ctx.KeyCtrl = ctx.KeysDown[GLFW_KEY_LEFT_CONTROL] || ctx.KeysDown[GLFW_KEY_RIGHT_CONTROL];
		ctx.KeyShift = ctx.KeysDown[GLFW_KEY_LEFT_SHIFT] || ctx.KeysDown[GLFW_KEY_RIGHT_SHIFT];
		ctx.KeyAlt = ctx.KeysDown[GLFW_KEY_LEFT_ALT] || ctx.KeysDown[GLFW_KEY_RIGHT_ALT];
		ctx.KeySuper = ctx.KeysDown[GLFW_KEY_LEFT_SUPER] || ctx.KeysDown[GLFW_KEY_RIGHT_SUPER];

	});

	InputManager::AddMouseScrollCallback([](void*, double x, double y) {
		ImGuiIO& io = ImGui::GetIO();
		io.MouseWheelH += (float)x;
		io.MouseWheel += (float)y;
	});

	InputManager::AddMouseButtonEventCallback([](void*, int btn, ButtonState state, int mods) {
		ImGuiIO& io = ImGui::GetIO();
		io.MouseDown[btn] = state != ButtonReleased;
	});

	InputManager::AddMouseMoveCallback([](void*, glm::vec2 pos, glm::vec2 dt) {
		ImGuiIO& io = ImGui::GetIO();
		io.MousePos = ImVec2(pos.x, pos.y);
	});

	InputManager::AddTextEventCallback([](void*, uint32_t cp, int mods) {
		ImGuiIO& io = ImGui::GetIO();
		if (cp > 0 && cp < 0x10000)
			io.AddInputCharacter((unsigned short)cp);
	});

	imgui_impl::ImGuiInit(WINDOW_WIDTH, WINDOW_HEIGHT);
}

Game::~Game() {
	delete myWindow;
}

void Game::Run() {
	Init();

	double myElapsedTime = 0;
	while (!myWindow->Closing()) {
		myWindow->Update();
		Update();
		if (myElapsedTime > REFRESH_RATE) {
			Render();
			myWindow->SwapBuffers();
		}
		myElapsedTime += myWindow->GetFrameDelta();
	}
}

RenderTarget* __ApplySwap(RenderTarget*& source, RenderTarget*& dest) {
	dest->Bind();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	source->BindTexture(GL_COLOR_ATTACHMENT0, 0);
	DrawFullscreenQuad();
	RenderTarget* temp = source;
	source = dest;
	dest = temp;
	return dest;
}

void Game::__LoadShaders() {

	// Fullscreen quad shader
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/fs_quad.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xSampler", 0);
		myPool->Remove("shader.fsQuad");
		myPool->Set("shader.fsQuad", newShader);
	}

	// LUT shader
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lut.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xSampler", 0);
		newShader->SetUniform("xLut", 1);
		myPool->Remove("shader.lut");
		myPool->Set("shader.lut", newShader);
	}

	// Bloom highlights
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/bloom.highlight.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xSampler", 0);
		newShader->SetUniform("xBloomThreshold", 0.2f);
		myPool->Remove("shader.bloom.highlights");
		myPool->Set("shader.bloom.highlights", newShader);

	}

	// Gaussian Blur 3
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/blur.gaussian.3.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xSampler", 0);
		newShader->SetUniform("xResolution", glm::vec2(800, 600));
		myPool->Remove("shader.blur.gaussian.3");
		myPool->Set("shader.blur.gaussian.3", newShader);

	}

	// Gaussian Blur 7
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/blur.gaussian.7.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xSampler", 0);
		newShader->SetUniform("xResolution", glm::vec2(800, 600));
		myPool->Remove("shader.blur.gaussian.7");
		myPool->Set("shader.blur.gaussian.7", newShader);

	}

	// Load the re-normalization shader
	{
		Shader *newShader2 = new Shader();
		newShader2->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader2->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/fs_renorm.fs");
		newShader2->Link();
		newShader2->Enable();
		newShader2->SetUniform("xSampler", 0);
		myPool->Remove("shader.fsQuad.renorm");
		myPool->Set("shader.fsQuad.renorm", newShader2);
	}

	// Load the depth linearization shader
	{
		Shader *newShader3 = new Shader();
		newShader3->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader3->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/fs_linearize.fs");
		newShader3->Link();
		newShader3->Enable();
		newShader3->SetUniform("xSampler", 0);
		myPool->Remove("shader.fsQuad.linearize");
		myPool->Set("shader.fsQuad.linearize", newShader3);
	}

	// Load the ambient lighting shader
	{
		Shader *newShader4 = new Shader();
		newShader4->LoadShaderPart(GL_VERTEX_SHADER, "shaders/lighting_ambient.vs");
		newShader4->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lighting_ambient.fs");
		newShader4->Link();
		newShader4->Enable();
		newShader4->SetUniform("xAlbedo", 0);
		newShader4->SetUniform("xNormals", 1);
		newShader4->SetUniform("xDepth", 2);
		newShader4->SetUniform("xViewPos", 3);
		newShader4->SetUniform("xAmbientFactor", glm::vec4(glm::vec3(0.3f), 1.0f));
		myPool->Remove("shader.lighting.ambient");
		myPool->Set("shader.lighting.ambient", newShader4);
	}

	// Load the point lighting shader
	{
		Shader *newShader5 = new Shader();
		newShader5->LoadShaderPart(GL_VERTEX_SHADER, "shaders/lighting_point.vs");
		newShader5->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lighting_point.fs");
		newShader5->Link();
		newShader5->Enable();
		newShader5->SetUniform("xAlbedo", 0);
		newShader5->SetUniform("xNormals", 1);
		newShader5->SetUniform("xDepth", 2);
		newShader5->SetUniform("xViewSpace", 3);

		newShader5->SetUniform("xDiffuseRamp", 4);
		newShader5->SetUniform("xSpecularRamp", 5);
		newShader5->SetUniform("xWorld", glm::mat4());
		newShader5->SetUniform("xView", glm::mat4());
		myPool->Remove("shader.lighting.point");
		myPool->Set("shader.lighting.point", newShader5);
	}

	// The point light debug shader
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/lighting_point.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lighting_pointdebug.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xAlbedo", 0);
		newShader->SetUniform("xNormals", 1);
		newShader->SetUniform("xDepth", 2);
		newShader->SetUniform("xViewSpace", 3);
		newShader->SetUniform("xWorld", glm::mat4());
		newShader->SetUniform("xView", glm::mat4());
		myPool->Remove("shader.lighting.pointdebug");
		myPool->Set("shader.lighting.pointdebug", newShader);
	}

	// Lighting composite shader
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lighting_combine.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xAlbedo", 0);
		newShader->SetUniform("xDiffuse", 1);
		newShader->SetUniform("xSpecular", 2);
		newShader->SetUniform("xViewSpace", 3);
		newShader->SetUniform("xSpecularMult", 1.0f);
		myPool->Remove("shader.lighting.combine");
		myPool->Set("shader.lighting.combine", newShader);
	}

	// Load the forward shader for atlas
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/forward.atlas.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/forward.atlas.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xWorld", glm::mat4());
		newShader->SetUniform("xTextureAtlas", 0);
		myPool->Remove("shader.forward");
		myPool->Set("shader.forward", newShader);
	}

	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/forward.atlas.normal.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/forward.atlas.normal.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xWorld", glm::mat4());
		newShader->SetUniform("xTextureAtlas", 0);
		newShader->SetUniform("xNormalAtlas", 1);
		myPool->Remove("shader.forward.normal");
		myPool->Set("shader.forward.normal", newShader);
	}

	// Load the forward shader for basic 2D texturing
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/forward.textured.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/forward.textured.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xWorld", glm::mat4());
		newShader->SetUniform("xSampler", 0);
		myPool->Remove("shader.forward.textured");
		myPool->Set("shader.forward.textured", newShader);
	}

	// Load the forward shader for flat shading
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/forward.flat.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/forward.flat.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xWorld", glm::mat4());
		newShader->SetUniform("xSampler", 0);
		myPool->Remove("shader.forward.flat");
		myPool->Set("shader.forward.flat", newShader);
	}

	// Load the lighting shader for the rim shading pass
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/lighting.rimlight.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lighting.rimlight.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xRimFactor", 4);
		newShader->SetUniform("xAlbedo", 0);
		newShader->SetUniform("xNormals", 1);
		newShader->SetUniform("xViewSpace", 3);
		myPool->Remove("shader.lighting.rim");
		myPool->Set("shader.lighting.rim", newShader);
	}

	// Load the depth passthrough shader
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/forward.depth.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/forward.depth.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xWorld", glm::mat4());
		myPool->Remove("shader.forward.depth");
		myPool->Set("shader.forward.depth", newShader);
	}

	// Load the shadow shader
	{
		Shader *newShader = new Shader();
		newShader->LoadShaderPart(GL_VERTEX_SHADER, "shaders/fs_quad.vs");
		newShader->LoadShaderPart(GL_FRAGMENT_SHADER, "shaders/lighting.shadow.fs");
		newShader->Link();
		newShader->Enable();
		newShader->SetUniform("xAlbedo", 0);
		newShader->SetUniform("xNormals", 1);
		newShader->SetUniform("xViewSpace", 3);
		newShader->SetUniform("xShadowMap", 4);
		newShader->SetUniform("xDiffuseRamp", 5);
		newShader->SetUniform("xSpecularRamp", 6);
		myPool->Remove("shader.lighting.shadow");
		myPool->Set("shader.lighting.shadow", newShader);
	}
}

void Game::__InitAudio() {

	// Creates a new audio engine
	myAudioEngine = new audio::AudioEngine();

	// Register it to the global service provider
	ServiceProvider::Register<audio::AudioEngine>(myAudioEngine);

	// Load in a sample sting
	uint32_t id = myAudioEngine->CreateNamedSource("audio/pop.wav", "pop");

	// Turn down the music
	myAudioEngine->SetGroupVolume(SoundMusic, 0.25f);

	// Load in the track lists
	myAudioEngine->CreateNamedSource("audio/music/theme.ogg", "theme1", SoundMusic, false, true);
	myAudioEngine->CreateNamedSource("audio/music/theme2.ogg", "theme2", SoundMusic, false, true);
	myAudioEngine->CreateNamedSource("audio/music/theme3.ogg", "theme3", SoundMusic, false, true);

	// Pop off a sound, why not
	myAudioEngine->FireSound(id);

	// Create an instance for and start playing the first theme song
	myPool->Set("audio.theme", myAudioEngine->CreateInstance("theme1"));
}

particles::ParticleEffect* LoadEffect(const char* fileName) {
	particles::ParticleEffect* result = nullptr;
	if (fileName[0] != '\0') {

		std::fstream stream;
		stream.open(fileName, std::ios::in | std::ios::binary);
		if (stream.good()) {
			particles::TextureCollection::ReadFromFile(stream);
			result = particles::ParticleEffect::ReadFromFile(stream);
			result->Init();
		}
		else {
			std::cout << "Failed to read file" << std::endl;
		}
		stream.close();
	}
	return result;
}

void Game::__InitGraphics() {

	particles::Renderer::Init();

	// Set initial GL states
	glEnable(GL_DEPTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);

	glGenQueries(1, &myQueryId);

	myPool->Set("texture.1d.black_to_white", new Texture1D("textures/gradient_btow.png"));
	myPool->Set("texture.1d.white_to_black", new Texture1D("textures/gradient_wtob.png"));
	myPool->Set("texture.1d.rainbow", new Texture1D("textures/ramp_rainbow.png"));
	myPool->Set("texture.1d.smoothstep", new Texture1D("textures/ramp_smoothstep.png"));
	myPool->Set("texture.1d.toon", new Texture1D("textures/ramp_toon.png"));

	myPool->Set("lighting.ramps.diffuse", myPool->Get<Texture1D*>("texture.1d.black_to_white"));
	myPool->Set("lighting.ramps.specular", myPool->Get<Texture1D*>("texture.1d.black_to_white"));
	myPool->Set("lighting.ramps.specular.power", 1.0f);
	myPool->Set("lighting.ramps.diffuse.power", 1.0f);
	myPool->Set("lighting.rim.sampler", myPool->Get<Texture1D*>("texture.1d.rainbow"));

	{
		
		/*
		auto effect = myPool->Set("particles", new particles::ParticleEffect());
		particles::ParticleEffectSettings efSettings = particles::ParticleEffectSettings();
		particles::LayerConfig config;
		config.SizeRange = glm::vec2(0.2f, 1.0f);
		config.InitColor = glm::vec4(0.1f, 0.1f, 1.0f, 1.0f);
		config.FinalColor = glm::vec4(0.5f, 0.5f, 1.0f, 0.0f);
		config.LifeRange = glm::vec2(0.1f, 1.0f);
		config.Gravity = glm::vec3(0.0f, 0.0f, -9.81f);
		config.EmissionRate = 20.0f;
		config.MaxParticles = 1000;
		config.BoundsType = particles::Circle;
		config.BoundsMeta = glm::vec3(1.0f);
		config.VelocityRange = glm::vec2(0.1f, 1.0f);
		config.Position = glm::vec3();
		config.TextureID = 1;
		memcpy(config.Name, "Default", 8);

		particles::ParticleLayerSettings settings = particles::ParticleLayerSettings();
		
		settings.Config = config;

		effect->AddLayer(settings);
		effect->Init();
		*/
		particles::ParticleEffect* effect = LoadEffect("effects/leaves.dat");
		myPool->Set("particles", effect);
	
		auto puff = myPool->Set("textures.2d.particles.puff", new Texture2D("textures/cloud.png"));
		auto flare = myPool->Set("textures.2d.particles.puff", new Texture2D("textures/flare.png"));

		//particles::Renderer::SetTexture(0, puff->GetHandle());
		//particles::Renderer::SetTexture(1, flare->GetHandle());
	}

	myPool->Set<uint8_t>("DisplayMode", Deferred);
	myPool->Set<glm::vec3>("lighting.rim.color", glm::vec3(0.078f, 0.270f, 0.140f));
	myPool->Set<float>("lighting.rim.alpha", 1.0f);
	myPool->Set<float>("lighting.rim.width", 0.6f);

	/*
	auto shadowBuff1 = myPool->Set("fbo.shadow.1", new graphics::RenderTarget(1024, 1024));
	shadowBuff1->AddDepthStencilBuffer();
	shadowBuff1->Finalize();

	auto shadowCam1 = myPool->Set("cam.shadow.1", new graphics::Camera());
	shadowCam1->SetOrtho(-10.0f, 10.0f, -10.0f, 10.0f);
	shadowCam1->SetPosition(glm::vec3(32.0f, 32.0f, -64.0f));
	shadowCam1->LookAt(glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	*/

	auto gBuffer = myPool->Set("fbo.gBuffer", new graphics::RenderTarget(WINDOW_WIDTH, WINDOW_HEIGHT));
	FILE_LOG(logINFO) << "Starting FBO";
	gBuffer->AddDepthStencilBuffer();
	gBuffer->AddBuffer(GL_COLOR_ATTACHMENT0);
	gBuffer->AddBuffer(GL_COLOR_ATTACHMENT1, GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE); // Normals
	gBuffer->AddBuffer(GL_COLOR_ATTACHMENT2, GL_RGB16F, GL_RGB, GL_FLOAT); // View Space
	gBuffer->Finalize();

	auto particleBuffer = myPool->Set("fbo.particles", new graphics::RenderTarget(WINDOW_WIDTH, WINDOW_HEIGHT));
	particleBuffer->UseSharedDepthWith(gBuffer);
	particleBuffer->AddBuffer(GL_COLOR_ATTACHMENT0);
	particleBuffer->Finalize();

	auto lightBuffer = myPool->Set("fbo.lightacc", new graphics::RenderTarget(WINDOW_WIDTH, WINDOW_HEIGHT));
	lightBuffer->AddBuffer(GL_COLOR_ATTACHMENT0, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
	lightBuffer->AddBuffer(GL_COLOR_ATTACHMENT1, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
	lightBuffer->Finalize();

	auto composited = myPool->Set("fbo.composite", new graphics::RenderTarget(WINDOW_WIDTH, WINDOW_HEIGHT));
	composited->AddBuffer(GL_COLOR_ATTACHMENT0, GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE);
	composited->Finalize();

	auto bloomBright = myPool->Set("fbo.bloom.highlights", new graphics::RenderTarget(WINDOW_WIDTH / 8, WINDOW_HEIGHT / 8));
	bloomBright->AddBuffer(GL_COLOR_ATTACHMENT0);
	bloomBright->Finalize();

	auto bloomBlur1 = myPool->Set("fbo.bloom.blurred.1", new graphics::RenderTarget(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2));
	bloomBlur1->AddBuffer(GL_COLOR_ATTACHMENT0);
	bloomBlur1->Finalize();

	auto bloomBlur2 = myPool->Set("fbo.bloom.blurred.2", new graphics::RenderTarget(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2));
	bloomBlur2->AddBuffer(GL_COLOR_ATTACHMENT0);
	bloomBlur2->Finalize();


	MeshData data = GeoUtils::CreateSphere(1.0f);
	myPool->Set("mesh.sphere", new Mesh(data));

	myPool->Set("texture.lut.jungle", new Texture3D("luts/jungle.cube", 8, 8, GL_LINEAR, GL_REPEAT));
	myPool->Set("texture.lut.warm", new Texture3D("luts/warm.cube", 8, 8, GL_LINEAR, GL_REPEAT));
	myPool->Set("texture.lut.cool", new Texture3D("luts/cool.cube", 8, 8, GL_LINEAR, GL_REPEAT));
	myPool->Set("texture.atlas", new Texture3D("textures/atlas.png", 16, 16, GL_NEAREST, GL_REPEAT));
	myPool->Set("texture.atlas.normals", new Texture3D("textures/atlas_norms.png", 16, 16, GL_NEAREST, GL_REPEAT));

	myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.warm"));

	fml_replace debugGeo;
	myPool->Set("debug.geometry", debugGeo);

	myPool->Set("composite.lutfactor", 0.5f);
	myPool->Set("lighting.specpower", 1.0f);
	myPool->Set("lighting.diffpower", 1.0f);

	myPool->Get<fml_replace>("debug.geometry")["sample"] = {
		myPool->Get<Mesh*>("mesh.sphere"),
		glm::vec3(),
		[](DELETE_ME_LATER* ctx, float dt) {
		static float counter = 0;
		counter += dt;
		ctx->Position = glm::vec3(16.0f, 16.0f, 18.0f) + glm::vec3(cos(counter) * 12.0f, sin(counter) * 12.0f, cos(counter) * 4.0f);
	}
	};

	myPool->Get<fml_replace>("debug.geometry")["sample2"] = {
		myPool->Get<Mesh*>("mesh.sphere"),
		glm::vec3(16.0f, 16.0f, 16.0f)
	};

	auto& list = myPool->Set("lights.shadow_casters", std::vector<ShadowCastLight*>());

	// Shadow light 1
	{
		auto light = new ShadowCastLight(1024, 1024);
		light->SetPosition(glm::vec3(32, 32, 32));
		light->LookAt(glm::vec3(0.0f));
		light->SetupPerpectiveProjection(60.0f, 16.0f / 9.0f);
		list.push_back(light);
	}

	// Shadow light 2
	{
		auto light = new ShadowCastLight(1024, 1024);
		light->SetPosition(glm::vec3(-32, -32, 32));
		light->LookAt(glm::vec3(0.0f));
		light->SetupPerpectiveProjection(60.0f, 16.0f / 9.0f);
		light->SetColor(glm::vec3(1.0f, 0.5f, 0.0f));
		light->SetDynamic(false);
		list.push_back(light);
	}

	// Load the basic fullscreen quad shader
	__LoadShaders();
}

void Game::Init() {

	// Register the thread pool
	ServiceProvider::Register<tools::ThreadPool>(new tools::ThreadPool());

	// Create the attribute pool
	myPool = new tools::AttribPool();

	// Seed random
	size_t seed = time(NULL);

	// Initialize the world generator
	tools::WorldGen::Init(seed);

	__InitAudio();

	__InitGraphics();

	myWorld = new gameplay::VoxelWorld();

	for (int ix = -2; ix <= 2; ix++) {
		for (int iy = -2; iy <= 2; iy++) {
			for (int iz = -2; iz <= 2; iz++)
				myWorld->ForceLoad(glm::ivec3(ix, iy, iz));
		}
	}

	myCamera = new graphics::Camera();

	myWorld->LoadLightsFromFile("data/lights.xml");

	//myWorld->ReserveLights(64);
	//for (uint32_t x = 0; x < 4; x++) {
	//	for (uint32_t y = 0; y < 4; y++) {
	//		for (uint32_t z = 0; z < 2; z++) {
	//			float att = randf(0.1f, 16.0f);
	//			myWorld->AddLight(NoShadowLight(
	//				glm::vec3(x * 8.0f, y * 8.0f, (z + 2) * 8.0f),
	//				glm::vec3(randf(), randf(), randf()),
	//				att));
	//		}
	//	}
	//}

	myCamera->SetPosition(glm::vec3(CHUNK_SIZE / 2.0f, CHUNK_SIZE / 2.0f, CHUNK_SIZE));

}

void Game::__HandleInput() {

	ImGuiIO& io = ImGui::GetIO();

	// Camera movement
	if (!io.WantCaptureKeyboard)
	{

		glm::vec3 camPrevPos = myCamera->GetPosition();

		static glm::vec3 ypr, move;
		ypr.x = ypr.y = ypr.z = 0.0f;
		move.x = move.y = move.z = 0.0f;


		if (!myPool->Get<bool>("config.animate_camera")) {
			if (myWindow->IsKeyPressed(GLFW_KEY_A)) {
				move.x -= 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_D)) {
				move.x += 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_W)) {
				move.y += 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_S)) {
				move.y -= 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_SPACE)) {
				move.z += 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_LEFT_CONTROL)) {
				move.z -= 1.0f;
			}

			move *= 6.0f;

			if (myWindow->IsKeyPressed(GLFW_KEY_LEFT_SHIFT)) {
				move *= 2.0f;
			}

			if (myWindow->IsKeyPressed(GLFW_KEY_LEFT)) {
				ypr.x += 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_RIGHT)) {
				ypr.x -= 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_UP)) {
				ypr.y += 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_DOWN)) {
				ypr.y -= 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_E)) {
				ypr.z += 1.0f;
			}
			if (myWindow->IsKeyPressed(GLFW_KEY_Q)) {
				ypr.z -= 1.0f;
			}

			myCamera->Rotate(ypr * 90.0f * (float)myWindow->GetFrameDelta());
			myCamera->MoveRelative(move * (float)myWindow->GetFrameDelta());
		}

		glm::vec3 velocity = myCamera->GetPosition() - camPrevPos;
		myAudioEngine->SetListenerVelocity(velocity);

		uint8_t& renderMode = myPool->Get<uint8_t>("DisplayMode", 0U);

		if (InputManager::GetKeyState(GLFW_KEY_1) == ButtonPressed) {
			renderMode = DisplayMode::Deferred;
		}
		if (InputManager::GetKeyState(GLFW_KEY_2) == ButtonPressed) {
			Toggle(myPool, "config.lights.preview_volumes");
		}
		if (InputManager::GetKeyState(GLFW_KEY_3) == ButtonPressed) {
			renderMode = DisplayMode::LinearDepth;
		}
		if (InputManager::GetKeyState(GLFW_KEY_4) == ButtonPressed) {
			renderMode = DisplayMode::Normals;
		}
		if (InputManager::GetKeyState(GLFW_KEY_5) == ButtonPressed) {
			renderMode = DisplayMode::Albedo;
		}
		if (InputManager::GetKeyState(GLFW_KEY_6) == ButtonPressed) {
			renderMode = DisplayMode::LightingDiffuse;
		}
		if (InputManager::GetKeyState(GLFW_KEY_7) == ButtonPressed) {
			for (int ix = 0; ix < myWorld->NumLights(); ix++) {
				myWorld->GetLight(ix)->Type = (LightVolumeType)wrap((int)myWorld->GetLight(ix)->Type + 1, 1, 4);
			}
		}


		if (InputManager::GetKeyState(GLFW_KEY_F) == ButtonPressed) {
			auto& list = myPool->Get<std::vector<ShadowCastLight*>>("lights.shadow_casters");
			list[0]->SetPosition(myCamera->GetPosition());
			list[0]->LookAt(myCamera->GetPosition() + myCamera->GetFacing(), myCamera->GetUpVector());
		}

		if (InputManager::GetKeyState(GLFW_KEY_EQUAL) == ButtonPressed) {
			float att = randf(1.0f, 16.0f);
			myWorld->AddLight(NoShadowLight(
				(glm::vec3)(CHUNK_EXTENTS / 2) +
				glm::vec3(randf(-8.0f, 8.0f), randf(-8.0f, 8.0f), randf(-8.0f, 8.0f)),
				glm::vec3(randf(), randf(), randf()),
				att));
		}

		if (InputManager::GetKeyState(GLFW_KEY_8) == ButtonPressed) {
			myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.warm"));
			myPool->Set("composite.lutfactor", 1.0f);
			myPool->Set("lighting.diffpower", 1.0f);
			myPool->Set("lighting.specpower", 1.0f);
			myPool->Set("lighting.rimfactor", 1.0f);
		}
		if (InputManager::GetKeyState(GLFW_KEY_9) == ButtonPressed) {
			myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.cool"));
			myPool->Set("composite.lutfactor", 1.0f);
			myPool->Set("lighting.diffpower", 1.0f);
			myPool->Set("lighting.specpower", 1.0f);
			myPool->Set("lighting.rimfactor", 1.0f);
		}
		if (InputManager::GetKeyState(GLFW_KEY_0) == ButtonPressed) {
			myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.jungle"));
			myPool->Set("composite.lutfactor", 1.0f);
			myPool->Set("lighting.diffpower", 1.0f);
			myPool->Set("lighting.specpower", 1.0f);
			myPool->Set("lighting.rimfactor", 1.0f);
		}

		if (InputManager::GetKeyState(GLFW_KEY_F5) == ButtonPressed) {

			Texture3D* newAtlas = new Texture3D("atlas.png", 16, 16, GL_NEAREST, GL_REPEAT);
			myPool->Remove("texture.atlas");
			myPool->Set("texture.atlas", newAtlas);

			Texture3D* newAtlas2 = new Texture3D("atlas_norms.png", 16, 16, GL_NEAREST, GL_REPEAT);
			myPool->Remove("texture.atlas.normals");
			myPool->Set("texture.atlas.normals", newAtlas2);
		}

		if (InputManager::GetKeyState(GLFW_KEY_F4) == ButtonPressed) {
			__LoadShaders();
		}

		if (InputManager::GetKeyState(GLFW_KEY_P) == ButtonPressed) {
			myPool->Get<SoundInstance*>("audio.theme")->TogglePlaying();
		}

		if (InputManager::GetKeyState(GLFW_KEY_F) == ButtonPressed) {
			myAudioEngine->FireSound(
				"pop",
				myCamera->GetPosition() + glm::vec3(sin(glfwGetTime()), cos(glfwGetTime()), 0.0f),
				glm::vec3(),
				0.1f * ((rand() / (float)RAND_MAX) - 0.5f));
		}
		
		if (InputManager::GetKeyState(GLFW_KEY_F1) == ButtonPressed) {
			myPool->Set<bool>("WirefameEnabled", !myPool->Get<bool>("WirefameEnabled"));
		}
		
		if (InputManager::GetKeyState(GLFW_KEY_F2) == ButtonPressed) {
			myPool->Set<uint8_t>("DisplayMode", wrap(++myPool->Get<uint8_t>("DisplayMode", 0U), 0U, (uint32_t)DISPM_MAX + 1U));
		}
	}


}

void RenderParticles(particles::ParticleEffect* effect, tools::AttribPool* pool, Camera* camera) {
	RenderTarget* particleFbo = pool->Get<RenderTarget*>("fbo.particles");
	
	particleFbo->Bind();
	glClear(GL_COLOR_BUFFER_BIT);
	
	particles::Renderer::ViewMatrix = camera->GetView();
	particles::Renderer::ProjectionMatrix = camera->GetProjection();
	particles::Renderer::WorldTransform = glm::mat4(1.0f);

	effect->Draw();

	particles::Renderer::Flush();

	particleFbo->UnBind();
}

void Game::Update() {

	__HandleInput();

	myAudioEngine->SetListenerPos(myCamera->GetPosition());
	myAudioEngine->SetListenerAxes(myCamera->GetFacing(), myCamera->GetUpVector());

	//std::cout << gameplay::FACE_NAMES[(int)gameplay::DirectionToFacing(myCamera->GetFacing())] << std::endl;

	myAudioEngine->Update();

	myWorld->Update();

	float dt = myWindow->GetFrameDelta();

	myPool->Get<particles::ParticleEffect*>("particles")->Update(dt);

	auto& vec = myPool->Get<fml_replace>("debug.geometry");
	for (auto& kvp : vec) {
		auto& inst = kvp.second;
		if (inst.Update)
			inst.Update(&inst, dt);
	}

	float t = myWindow->GetFrameTime();
	if (dynamicLights)
	{
		for (int ix = 0; ix < myWorld->NumLights(); ix++)
		{
			if (ix % 2 == 0)
				myWorld->GetLight(ix)->Position += glm::vec3(sin(t + ix) * 1.0f, 0.0f, 0.0f);
			else
				myWorld->GetLight(ix)->Position += glm::vec3(0.0f, cos(t - ix) * 1.0f, 0.0f);
		}
	}


	if (myPool->Get<bool>("config.animate_camera")) {
		static float cameraTime = 0.0f;
		cameraTime += dt;

		myCamera->SetPosition((glm::vec3)(CHUNK_EXTENTS / 2) + glm::vec3(cos(cameraTime) * 16.0f, sin(cameraTime) * 16.0f, 10.0f));
		myCamera->LookAt(CHUNK_EXTENTS / 2, glm::vec3(0.0f, 0.0f, 1.0f));
	}


	ImGuiIO& io = ImGui::GetIO();

	if (io.WantMoveMouse)
		InputManager::SetMousePos(io.MousePos.x, io.MousePos.y);

	for (uint32_t ix = 0; ix < 4 * 4 * 2; ix++) {
		NoShadowLight* light = myWorld->GetLight(ix);
		//light->Position += glm::vec3(randf(-1.0f, 1.0f), randf(-1.0f, 1.0f), randf(-1.0f, 1.0f)) * 0.1f;
	}
	glfwSetInputMode(myWindow->GetHandle(), GLFW_CURSOR, io.MouseDrawCursor ? GLFW_CURSOR_HIDDEN : GLFW_CURSOR_NORMAL);
}

void Game::Render() {

	if (myWindow->GetWidth() > 0 && myWindow->GetHeight() > 0) {

		RenderTarget* gBuff = myPool->Get<RenderTarget*>("fbo.gBuffer");
		RenderTarget* lBuff = myPool->Get<RenderTarget*>("fbo.lightacc");
		RenderTarget* cBuff = myPool->Get<RenderTarget*>("fbo.composite");
		RenderTarget* pBuff = myPool->Get<RenderTarget*>("fbo.particles");


		GLuint64 forwardNs{ 0 }, lightingNS{ 0 }, compositeNS{ 0 }, bloomNs{ 0 }, finalNs{ 0 };
		GLint available;

		double startTime = glfwGetTime();

		// Dynamic lightmap generation 
		if (true) {

			{
				while (glGetError() != GL_NO_ERROR) {}

				// Disable all alpha blending
				glBlendFunc(GL_ONE, GL_ZERO);
				glEnable(GL_DEPTH_TEST);

				Shader* forward = myPool->Get<Shader*>("shader.forward.depth");
				forward->Enable();

				auto& vec = myPool->Get<fml_replace>("debug.geometry");

				auto& list = myPool->Get<std::vector<ShadowCastLight*>>("lights.shadow_casters");

				for (int ix = 0; ix < list.size(); ix++) {
					ShadowCastLight* light = list[ix];

					if (light->NeedsRender()) {
						light->BindForRender(forward);

						myWorld->Render(*light->GetCameraPtr(), forward);

						for (auto& kvp : vec) {
							auto& inst = kvp.second;

							forward->SetUniform("xViewWorld", light->GetView() * glm::translate(inst.Position));
							forward->SetUniform("xWorld", glm::translate(inst.Position));

							inst.Model->Draw();

						}

						light->NotifyRenderComplete();
					}
				}

				int err = glGetError();
			}
		}

		// Forward rendering stage
		if (gBuff)
		{
			glBeginQuery(GL_TIME_ELAPSED​, myQueryId);

			// Bind the gBuffer for use
			gBuff->Bind();

			// Clear the buffer to transparent grey
			glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

			// Disable all alpha blending
			glBlendFunc(GL_ONE, GL_ZERO);
			glEnable(GL_DEPTH_TEST);

			// Bind the texture atlas
			myPool->Get<Texture3D*>("texture.atlas")->Bind(0);
			myPool->Get<Texture3D*>("texture.atlas.normals")->Bind(1);

			// Get the forward renderer and set it up for the frame

			Shader* forward = myPool->Get<bool>("config.normals_enabled", true) ?
				myPool->Get<Shader*>("shader.forward.normal") :
				myPool->Get<Shader*>("shader.forward");

			forward->Enable();
			forward->SetUniform("xView", myCamera->GetView());
			forward->SetUniform("xProjection", myCamera->GetProjection());

			// Render the scene
			myWorld->Render(*myCamera, forward);

			// Render debug geometry
			auto& vec = myPool->Get<fml_replace>("debug.geometry");

			forward = myPool->Get<Shader*>("shader.forward.flat");
			forward->Enable();
			forward->SetUniform("xView", myCamera->GetView());
			forward->SetUniform("xProjection", myCamera->GetProjection());

			for (auto& kvp : vec) {
				auto& inst = kvp.second;

				forward->SetUniform("xWorld", glm::translate(inst.Position));
				forward->SetUniform("xNormalMatrix", glm::mat3(glm::inverseTranspose(glm::translate(inst.Position) * myCamera->GetView())));

				inst.Model->Draw();

			}

			// Blit the gBuffer's depth to the backbuffer in case we want to do transparent rendering now
			gBuff->BlitDepthToBackBuffer(myWindow->GetWidth(), myWindow->GetHeight());

			glEndQuery(GL_TIME_ELAPSED​);
			available = 0;
			while (!available) {
				glGetQueryObjectiv(myQueryId, GL_QUERY_RESULT_AVAILABLE, &available);
			}
			glGetQueryObjectui64v(myQueryId, GL_QUERY_RESULT, &forwardNs);
		}

		RenderParticles(myPool->Get<particles::ParticleEffect*>("particles"), myPool, myCamera);

		// Lighting stage
		if (lBuff)
		{
			glBeginQuery(GL_TIME_ELAPSED, myQueryId);

			// Bind the lighting buffer
			lBuff->Bind();

			// Bind the G-Buffer to the correct slots
			gBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			gBuff->BindTexture(GL_COLOR_ATTACHMENT1, 1);
			gBuff->BindDepthTex(2);
			gBuff->BindTexture(GL_COLOR_ATTACHMENT2, 3);

			// Clear to transparent black
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			// Enable additive blending
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);

			// Draw a fullscreen quad to calculate ambient light
			auto compositeShader = myPool->Get<Shader*>("shader.lighting.ambient");
			compositeShader->Enable();
			DrawFullscreenQuad();

			// Point lights
			{
				// Switch to front-face culling
				glCullFace(GL_FRONT);
				glEnable(GL_CULL_FACE);

				// Get the shader for point lights
				auto pointLightComposite = myPool->Get<Shader*>("shader.lighting.point");
				pointLightComposite->Enable();
				pointLightComposite->SetUniform("xDebugLights", myPool->Get<bool>("debug.showlights"));


				pointLightComposite->SetUniform("xSpecularRampPower", myPool->Get<float>("lighting.ramps.specular.power"));
				pointLightComposite->SetUniform("xDiffuseRampPower", myPool->Get<float>("lighting.ramps.diffuse.power"));

				myPool->Get<Texture1D*>("lighting.ramps.diffuse")->Bind(4);
				myPool->Get<Texture1D*>("lighting.ramps.specular")->Bind(5);

				// Initialize the point light shader
				pointLightComposite->SetUniform("xWindowSize", glm::vec2(gBuff->GetWidth(), gBuff->GetHeight()));
				pointLightComposite->SetUniform("xView", myCamera->GetView());
				pointLightComposite->SetUniform("xProjection", myCamera->GetProjection());
								
				// Iterate over all lights using the fastest method
				NoShadowLight *light = myWorld->GetLight(0);
				NoShadowLight *last = light + myWorld->NumLights();
				for (; light < last; light++) {
					// Render each light to the lBuffer
					light->Render(pointLightComposite, *myCamera);
				}

				// Go back to backface culling
				glCullFace(GL_BACK);
				
			}
			// Rim lighting
			{

				myPool->Get<Texture1D*>("lighting.rim.sampler")->Bind(4);

				auto rimShader = myPool->Get<Shader*>("shader.lighting.rim");
				rimShader->Enable();
				rimShader->SetUniform("xRimRampFactor", myPool->Get<float>("lighting.ramps.rim.power"));
				rimShader->SetUniform("xRimColor", myPool->Get<glm::vec3>("lighting.rim.color"));
				rimShader->SetUniform("xRimAlpha", myPool->Get<float>("lighting.rim.alpha"));
				rimShader->SetUniform("xRimWidth", myPool->Get<float>("lighting.rim.width"));
				DrawFullscreenQuad();

			}

			// Shadows
			if (true)
			{
				auto& list = myPool->Get<std::vector<ShadowCastLight*>>("lights.shadow_casters");

				auto shader = myPool->Get<Shader*>("shader.lighting.shadow");
				shader->Enable();

				myPool->Get<Texture1D*>("lighting.ramps.diffuse")->Bind(5);
				myPool->Get<Texture1D*>("lighting.ramps.specular")->Bind(6);

				const glm::mat4 biasMatrix(
					0.5, 0.0, 0.0, 0.0,
					0.0, 0.5, 0.0, 0.0,
					0.0, 0.0, 0.5, 0.0,
					0.5, 0.5, 0.5, 1.0
				);

				for (int ix = 0; ix < list.size(); ix++) {

					ShadowCastLight* light = list[ix];

					light->BindForSampling(shader, 4);

					glm::mat4 viewToLight = biasMatrix * light->GetCameraPtr()->GetViewProjection() * glm::inverse(myCamera->GetView());

					glm::vec4 lightPos = myCamera->GetView() * glm::vec4(light->GetPosition(), 1.0f);
					shader->SetUniform("xViewToLightSpace", viewToLight);
					shader->SetUniform("xLight.Position", lightPos);

					DrawFullscreenQuad();
				}
			}

			glEndQuery(GL_TIME_ELAPSED​);
			available = 0;
			while (!available) {
				glGetQueryObjectiv(myQueryId, GL_QUERY_RESULT_AVAILABLE, &available);
			}
			glGetQueryObjectui64v(myQueryId, GL_QUERY_RESULT, &lightingNS);
		}
		
		// Lighting composition
		if (cBuff) {
			glBeginQuery(GL_TIME_ELAPSED, myQueryId);

			// Disable the depth buffer completely
			glDisable(GL_DEPTH_TEST);
			glDepthMask(GL_FALSE);

			cBuff->Bind();

			// Clear with the default clear color (a greyish-blue)
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			// Grab the final compistion shader
			auto shader = myPool->Get<Shader*>("shader.lighting.combine");
			shader->Enable();

			shader->SetUniform("xSpecularMult", myPool->Get<float>("lighting.specpower"));
			shader->SetUniform("xDiffuseMult", myPool->Get<float>("lighting.diffpower"));

			// Bind the albedo buffer and the lighting buffers
			gBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			lBuff->BindTexture(GL_COLOR_ATTACHMENT0, 1);
			lBuff->BindTexture(GL_COLOR_ATTACHMENT1, 2);

			// Composite the scene
			DrawFullscreenQuad();

			glEndQuery(GL_TIME_ELAPSED​);
			available = 0;
			while (!available) {
				glGetQueryObjectiv(myQueryId, GL_QUERY_RESULT_AVAILABLE, &available);
			}
			glGetQueryObjectui64v(myQueryId, GL_QUERY_RESULT, &compositeNS);
		}

		// Bloom pass
		DisplayMode mode = (DisplayMode)myPool->Get<uint8_t>("DisplayMode", 0U);
		if (mode == DeferredBloom | mode == DeferredHighlight | mode == DeferredBlurred) {
			glBeginQuery(GL_TIME_ELAPSED, myQueryId);

			auto highlights = myPool->Get<RenderTarget*>("fbo.bloom.highlights");
			highlights->Bind();
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT);


			auto highlightShader = myPool->Get<Shader*>("shader.bloom.highlights");
			if (highlightShader) {
				highlightShader->Enable();
				cBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
				DrawFullscreenQuad();
			}


			auto blurShader = myPool->Get<Shader*>("shader.blur.gaussian.7");
			blurShader->Enable();
			RenderTarget*& blur1 = myPool->Get<RenderTarget*>("fbo.bloom.blurred.1");
			RenderTarget*& blur2 = myPool->Get<RenderTarget*>("fbo.bloom.blurred.2");
			blurShader->SetUniform("xResolution", glm::vec2(blur1->GetWidth(), blur1->GetHeight()));
			blurShader->SetUniform("xDirection", glm::vec2(0, 1));

			blur1->Bind();
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT);
			highlights->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();
			blurShader->SetUniform("xDirection", glm::vec2(1, 0));
			blur2->Bind();
			glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
			glClear(GL_COLOR_BUFFER_BIT);
			blur1->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();

			for (int ix = 0; ix < 4; ix++) {
				blurShader->SetUniform("xDirection", glm::vec2(0, 1));
				__ApplySwap(blur1, blur2);
				blurShader->SetUniform("xDirection", glm::vec2(1, 0));
				__ApplySwap(blur1, blur2);
			}

			myPool->Set("fbo.bloom.blurred.active", blur1);

			glEndQuery(GL_TIME_ELAPSED​);
			available = 0;
			while (!available) {
				glGetQueryObjectiv(myQueryId, GL_QUERY_RESULT_AVAILABLE, &available);
			}
			glGetQueryObjectui64v(myQueryId, GL_QUERY_RESULT, &bloomNs);
		}

		glBeginQuery(GL_TIME_ELAPSED, myQueryId);

		// Bind the backbuffer
		RenderTarget::UnBind();
		glViewport(0, 0, myWindow->GetWidth(), myWindow->GetHeight());
		glClearColor(0.5f, 0.5f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// Enable alpha blending
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Composite frame depending on options
		switch ((DisplayMode)myPool->Get<uint8_t>("DisplayMode", 0U)) {
		case DeferredBloom: {

			//auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			//quadShader->Enable();
			//blurT->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			//DrawFullscreenQuad();
			lBuff->Bind();
			glClearColor(0.5f, 0.5f, 1.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			RenderTarget* blur = myPool->Get<RenderTarget*>("fbo.bloom.blurred.active");



			auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			quadShader->Enable();

			// Enable alpha blending
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			cBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();

			// Enable additive blending
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);

			blur->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();

			RenderTarget::UnBind();
			// Enable alpha blending
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glViewport(0, 0, myWindow->GetWidth(), myWindow->GetHeight());

			auto lutShader = myPool->Get<Shader*>("shader.lut");
			lutShader->Enable();
			lutShader->SetUniform("xLutFactor", myPool->Get<float>("composite.lutfactor"));
			lBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			myPool->Get<Texture3D*>("texture.lut.active")->Bind(1);
			DrawFullscreenQuad();

			break;
		}
		case DeferredHighlight: {
			auto highlights = myPool->Get<RenderTarget*>("fbo.bloom.highlights");

			auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			quadShader->Enable();
			highlights->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();
			break;

			break;
		}
		case DeferredBlurred: {
			RenderTarget* blur = myPool->Get<RenderTarget*>("fbo.bloom.blurred.active");

			auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			quadShader->Enable();
			blur->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();

			break;
		}
		case Deferred: {
			auto quadShader = myPool->Get<Shader*>("shader.lut");
			quadShader->Enable();
			quadShader->SetUniform("xLutFactor", myPool->Get<float>("composite.lutfactor"));
			cBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			myPool->Get<Texture3D*>("texture.lut.active")->Bind(1);
			DrawFullscreenQuad();
			pBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();
			break;
		}
		case Albedo: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			quadShader->Enable();
			gBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();
			break;
		}
		case ViewPos: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad.renorm");
			quadShader->Enable();
			quadShader->SetUniform("xMin", glm::vec3(0.1f));
			quadShader->SetUniform("xRange", glm::vec3(100.0f));
			gBuff->BindTexture(GL_COLOR_ATTACHMENT2, 0);
			DrawFullscreenQuad();
			break;
		}
		case Normals: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad.renorm");
			quadShader->Enable();
			quadShader->SetUniform("xMin", glm::vec3(-1.0f));
			quadShader->SetUniform("xRange", glm::vec3(2.0f));
			gBuff->BindTexture(GL_COLOR_ATTACHMENT1, 0);
			DrawFullscreenQuad();
			break;
		}
		case LightingDiffuse: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			quadShader->Enable();
			lBuff->BindTexture(GL_COLOR_ATTACHMENT0, 0);
			DrawFullscreenQuad();
			break;
		}
		case LightingSpecular: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad");
			quadShader->Enable();
			lBuff->BindTexture(GL_COLOR_ATTACHMENT1, 0);
			DrawFullscreenQuad();
			break;
		}
		case LinearDepth: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad.linearize");
			quadShader->Enable();
			quadShader->SetUniform("xNearPlane", myCamera->GetNearPlane());
			quadShader->SetUniform("xFarPlane", myCamera->GetFarPlane());
			gBuff->BindDepthTex(0);
			DrawFullscreenQuad();
			break;
		}
		case LightDepth: {
			auto quadShader = myPool->Get<Shader*>("shader.fsQuad.linearize");
			quadShader->Enable();
			
			auto& list = myPool->Get<std::vector<ShadowCastLight*>>("lights.shadow_casters");
			list[0]->GetRenderTargetPtr()->BindDepthTex(0);

			quadShader->SetUniform("xNearPlane", list[0]->GetCameraPtr()->GetNearPlane());
			quadShader->SetUniform("xFarPlane", list[0]->GetCameraPtr()->GetFarPlane());

			DrawFullscreenQuad();
			break;
		}
		default: {
			glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			break;
		}
		}

		glEndQuery(GL_TIME_ELAPSED​);
		available = 0;
		while (!available) {
			glGetQueryObjectiv(myQueryId, GL_QUERY_RESULT_AVAILABLE, &available);
		}
		glGetQueryObjectui64v(myQueryId, GL_QUERY_RESULT, &finalNs);

		if (myPool->Get<bool>("config.lights.preview_volumes", false)) {
			bool cullingEnabled = glIsEnabled(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			Shader* shader = myPool->Get<Shader*>("shader.forward.flat");
			shader->Enable();
			shader->SetUniform("xView", myCamera->GetView());
			shader->SetUniform("xProjection", myCamera->GetProjection());
			for (int ix = 0; ix < myWorld->NumLights(); ix++)
				myWorld->GetLight(ix)->Render(shader, *myCamera);
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glCullFace(GL_BACK);
			if (cullingEnabled)
				glEnable(GL_CULL_FACE);
		}

		// TODO: Render UI elements
		{
			/*
			DeferredBloom     = 0x0,
			DeferredHighlight = 0x1,
			DeferredBlurred   = 0x2,
			Deferred          = 0x3,
			Albedo            = 0x4,
			Normals           = 0x5,
			ViewPos           = 0x6,
			LightingDiffuse   = 0x7,
			LightingSpecular  = 0x8,
			LinearDepth       = 0x9
			LightDepth        = 0xA
			*/
			const char* RENDER_NAMES = "Magic Mushroom Mode\0DeferredHighlight\0DeferredBlurred\0Deferred\0Albedo\0Normals\0ViewPos\0LightingDiffuse\0LightingSpecular\0LinearDepth\0LightDepth\0";

			// Update the ImGui delta time
			auto& ctx = ImGui::GetIO();
			ctx.DeltaTime = myWindow->GetFrameDelta();

			// Start a new ImGui frame
			ImGui::NewFrame();

			const char* RAMP_NAMES = "Regular\0Inverse\0Toon\0Rainbow\0Smoothstep\0";
			const char* RAMP_REALNAMES[5] = {
				"texture.1d.black_to_white",
				"texture.1d.white_to_black",
				"texture.1d.toon",
				"texture.1d.rainbow",
				"texture.1d.smoothstep"
			};

			const char* LIGHT_TYPE_NAMES = "--\0Sphere\0Cone\0Cube\0";

			// Start the debug menu
			bool& debugOpen = myPool->Get<bool>("debug.isopen", true);
			if (ImGui::Begin("Debug Menu", &debugOpen, ImGuiWindowFlags_AlwaysAutoResize)) {

				ImGui::Text("Controls:");
				ImGui::Text("Arrow keys to rotate camera");
				ImGui::Text("WASD to move");
				ImGui::Text("Q and E to roll");
				ImGui::Text("F2 to change render display mode");
				ImGui::Text("F4 to reload shaders");
				ImGui::Text("F5 to reload textures");
				ImGui::NewLine();

				glm::vec3 pos = myCamera->GetPosition();

				// Write the facing of the camera
				ImGui::Text(FACE_NAMES[(int)gameplay::DirectionToFacing(myCamera->GetFacing())]);
				ImGui::Text("%05.2f, %05.2f, %05.2f", pos.x, pos.y, pos.z);

				{
					ImGui::Checkbox("Show Light Timing", &myPool->Get<bool>("config.debug.timing"));
					ImGui::Checkbox("Toggle Dynamic Lights", &dynamicLights);
					ImGui::Checkbox("Animate Camera", &myPool->Get<bool>("config.animate_camera"));
				}

				uint8_t& renderMode = myPool->Get<uint8_t>("DisplayMode", 0U);
				int renderModeInt = renderMode;
				ImGui::Combo("Render Mode", &renderModeInt, RENDER_NAMES);
				renderMode = renderModeInt;

				ImGui::NewLine();

				if (ImGui::CollapsingHeader("Lights                    ")) {
					int toRemove = -1;
					if (ImGui::Button("Add Light")) {
						float att = randf(1.0f, 16.0f);
						myWorld->AddLight(NoShadowLight(
							(glm::vec3)(CHUNK_EXTENTS / 2) + 
								glm::vec3(randf(-8.0f, 8.0f), randf(-8.0f, 8.0f), randf(-8.0f, 8.0f)),
							glm::vec3(randf(), randf(), randf()),
							att));
					}
					ImGui::SameLine();
					if (ImGui::Button("Clear Lights")) {
						myWorld->ClearLights();
					}
					ImGui::SameLine();
					if (ImGui::Button("Load Lights")) {
						myWorld->ClearLights();
						myWorld->LoadLightsFromFile("lights.xml");
					}
					std::string text = "Light ";
					for (int ix = 0; ix < myWorld->NumLights(); ix++) {
						NoShadowLight* light = myWorld->GetLight(ix);
						//ImGui::PushID(ix);
						{
							if (ImGui::TreeNode(std::to_string(ix).c_str())) {
								ImGui::Combo("Type", (int*)&light->Type, LIGHT_TYPE_NAMES);
								ImGui::DragFloat3("Position", &light->Position.r, 0.1f);
								ImGui::ColorEdit3("Color", &light->Color.r);
								ImGui::SliderFloat("Radius", &light->Radius,0.1f, 100.0f);
								float rad = glm::radians(light->Angle);
								ImGui::SliderAngle("Angle", &rad, 0.1f, 79.9f);
								light->Angle = glm::degrees(rad);
								ImGui::SliderAngle("RotX", &light->EulerRotation.x);
								ImGui::SliderAngle("RotY", &light->EulerRotation.y);
								ImGui::SliderAngle("RotZ", &light->EulerRotation.z);
								if (ImGui::Button("Remove")) {
									toRemove = ix;
								}
								ImGui::TreePop();
							}
						}
						//ImGui::PopID();
					}
					if (toRemove != -1)
						myWorld->RemoveLight(toRemove);
				}

				if (ImGui::CollapsingHeader("Lighting Options          "))
				{
					ImGui::Checkbox("Preview Light Volumes", &myPool->Get<bool>("config.lights.preview_volumes", false));

					ImGui::Text("Compositing:");

					{
						auto shader = myPool->Get<Shader*>("shader.bloom.highlights");
						if (shader) {
							static float threshold = 1 - 0.2f;
							ImGui::SliderFloat("Bloom", &threshold, 0, 1);
							shader->Enable();
							shader->SetUniform("xBloomThreshold", 1 - threshold);
						}
					}

					{
						ImGui::Checkbox("Normal Mapping", &myPool->Get<bool>("config.normals_enabled"));
					}

					{
						float& spec = myPool->Get<float>("lighting.specpower");
						ImGui::SliderFloat("Specular Power", &spec, 0, 1);
						float& diff = myPool->Get<float>("lighting.diffpower");
						ImGui::SliderFloat("Diffuse Power", &diff, 0, 1);
					}

					{
						static int selectedSpec = 0;
						ImGui::Combo("Spec. Ramp", &selectedSpec, RAMP_NAMES);
						myPool->Set("lighting.ramps.specular", myPool->Get<Texture1D*>(RAMP_REALNAMES[selectedSpec]));
						float& specRampPower = myPool->Get<float>("lighting.ramps.specular.power");
						ImGui::SliderFloat("Specular Ramp Factor", &specRampPower, 0, 1);

						static int selectedDiff = 0;
						ImGui::Combo("Diff. Ramp", &selectedDiff, RAMP_NAMES);
						myPool->Set("lighting.ramps.diffuse", myPool->Get<Texture1D*>(RAMP_REALNAMES[selectedDiff]));
						float& diffRampPower = myPool->Get<float>("lighting.ramps.diffuse.power");
						ImGui::SliderFloat("Diffuse Ramp Factor", &diffRampPower, 0, 1);
					}

					ImGui::Text("Color Correction:");

					{
						if (ImGui::Button("Cool")) {
							myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.cool"));
						}
						ImGui::SameLine();
						if (ImGui::Button("Warm")) {
							myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.warm"));
						}
						ImGui::SameLine();
						if (ImGui::Button("Jungle")) {
							myPool->Set("texture.lut.active", myPool->Get<Texture3D*>("texture.lut.jungle"));
						}
					}

					{
						float& lutFactor = myPool->Get<float>("composite.lutfactor");
						ImGui::SliderFloat("LUT Factor", &lutFactor, 0, 1);
					}

					ImGui::Text("Rim Lighting:");

					{
						glm::vec3& col = myPool->Get<glm::vec3>("lighting.rim.color", glm::vec3(0.5f, 0.0f, 0.5f));
						ImGui::ColorEdit3("Rim Color", &col.x);
						float& alpha = myPool->Get<float>("lighting.rim.alpha", 1.0f);
						ImGui::SliderFloat("Rim Alpha", &alpha, 0, 1);
						float& width = myPool->Get<float>("lighting.rim.width", 0.6f);
						ImGui::SliderFloat("Rim Width", &width, 0, 1);

						static int selectedSpec = 0;
						ImGui::Combo("Rim Ramp", &selectedSpec, RAMP_NAMES);
						myPool->Set("lighting.rim.sampler", myPool->Get<Texture1D*>(RAMP_REALNAMES[selectedSpec]));
						float& rampPower = myPool->Get<float>("lighting.ramps.rim.power");
						ImGui::SliderFloat("Rim Ramp Factor", &rampPower, 0, 1);

					}
				}

				// Write the audio header
				if (ImGui::CollapsingHeader("Audio"))
				{
					// Draw the music volume slider and bind to the group volume
					{
						float vol = myAudioEngine->GetGroupVolume(SoundMusic);
						ImGui::SliderFloat("Music Volume", &vol, 0, 1);
						myAudioEngine->SetGroupVolume(SoundMusic, vol);
					}
					// Draw the stings volume slider and bind to the group volume
					{
						float vol = myAudioEngine->GetGroupVolume(SoundStings);
						ImGui::SliderFloat("Effect Volume", &vol, 0, 1);
						myAudioEngine->SetGroupVolume(SoundStings, vol);
					}

					// Make some space above the track list
					ImGui::NewLine();
					ImGui::TextColored(ImVec4(1, 0, 0, 1), "Tracklist");

					{
						SoundInstance* theme = myPool->Get<SoundInstance*>("audio.theme");
						// Provide buttons for each track in the list
						if (ImGui::Button(" Off to Another Castle ")) {
							myAudioEngine->SetInstanceSource(theme, "theme1");
							theme->Play();
						}
						if (ImGui::Button("   Ode of Adventure    ")) {
							myAudioEngine->SetInstanceSource(theme, "theme2");
							theme->Play();
						}
						if (ImGui::Button("    Escape Da Snow     ")) {
							myAudioEngine->SetInstanceSource(theme, "theme3");
							theme->Play();
						}

						if (ImGui::Button("Stop")) {
							theme->Stop();
						}
					}

					// Add some space
					ImGui::NewLine();

					// Draw the audio sample area
					{
						// Prompt for filename
						ImGui::Text("Enter a filename to load");
						static char buffer[32] = "audio/music/theme2.ogg";
						ImGui::InputText("", buffer, 32);

						// Provide a button for loading the file
						if (ImGui::Button("Load File")) {
							// Unload the sound effect if it exists and load in new sound
							myAudioEngine->UnloadNamedSource("user_sound");
							myPool->Remove("audio.user_sound");
							myAudioEngine->CreateNamedSource(buffer, "user_sound");

							// Make a new sound instance for the sound, make it loop, then add to pool
							SoundInstance* user_sound = myAudioEngine->CreateInstance("user_sound");
							user_sound->SetLoopingEnabled();
							myPool->Set("audio.user_sound", user_sound);

							// Only perform once (Add geometry)
							static bool hasRenderable = false;
							if (!hasRenderable) {
								hasRenderable = true;
								// Make a sphere mesh
								Mesh* mesh = myPool->Get<Mesh*>("mesh.sphere");

								// Add the geometry to the debug geo for both origin and speaker
								myPool->Get<fml_replace>("debug.geometry")["sound"] = {
									mesh,
									glm::vec3()
								};
								myPool->Get<fml_replace>("debug.geometry")["origin"] = {
									mesh,
									glm::vec3(0.0f, 0.0f, 32.0f)
								};

								// Force camera to look at origin
								myCamera->LookAt(glm::vec3(0, 0, 32.0f));

								// Add a light over the origin to differentiate it
								myWorld->AddLight(NoShadowLight(glm::vec3(0.0f, 0.0f, 33.0f), glm::vec3(1.0f, 0.0f, 1.0f)));
							}

						}

						// Grab the sound instance and show the controls if it exists
						SoundInstance* user_sound = myPool->Get<SoundInstance*>("audio.user_sound");
						if (user_sound) {
							// Grab refs to the parameters from the pool
							glm::vec3&   pos = myPool->Get<glm::vec3>("audio.user_sound.position");
							float&      dist = myPool->Get<float>("audio.user_sound.distance", 10.0f);
							bool& isRotating = myPool->Get<bool>("audio.user_sound.rotate");
							float&     angle = myPool->Get<float>("audio.user_sound.angle", 0.0f);

							// Playing checkbox
							bool playing = user_sound->IsPlaying();
							if (ImGui::Checkbox("Play", &playing)) {
								if (playing)
									user_sound->Play();
								else
									user_sound->Pause();
							}

							// Rolloff checkbox
							static bool linear = true;
							if (ImGui::Checkbox("Linear Rolloff", &linear)) {
								user_sound->SetRolloff(linear ? Linear : Inverse);
							}

							// Position manipulation and display
							ImGui::Checkbox("Rotate", &isRotating);
							ImGui::SliderFloat("Distance ", &dist, 0, 500.0f);
							ImGui::Text("Pos: %03.2f %03.2f %03.2f", pos.x, pos.y, pos.z);

							// Rolloff tweaking
							glm::vec2 fadeOff = user_sound->GetMinMaxDistance();
							ImGui::SliderFloat2("Rolloff Min/Max", &fadeOff.x, 0.0f, 1000.0f);
							user_sound->SetMinMaxDistance(fadeOff);

							// Should really be in update :/
							pos = glm::vec3(cos(angle) * dist, sin(angle) * dist, 32.0f);
							glm::vec3 velocity = glm::vec3(0);
							if (isRotating) {
								float angleDelta = myWindow->GetFrameDelta() * 3.14f / 2.0f;
								angle += angleDelta;
								velocity = glm::normalize(glm::cross(glm::vec3(0, 0, 1), pos)) * angleDelta * 3.14f * dist * dist * (float)myWindow->GetFrameDelta();
							}
							// Update the sound's 3D settings as well as the geometries' position
							user_sound->Set3D(pos, velocity);
							myPool->Get<fml_replace>("debug.geometry")["sound"].Position = pos;
						}
					}
				}
			}
			// Finish the debug window
			ImGui::End();

			// Render the ImGui things
			ImGui::Render();
		}

		if (myPool->Get<bool>("config.debug.timing", false)) {
			FILE_LOG(logINFO) << format("%05.2f | %05.2f | %05.2f | %05.2f | %05.2f",
				forwardNs / 1000000.0,
				lightingNS / 1000000.0,
				compositeNS / 1000000.0,
				bloomNs / 1000000.0,
				compositeNS / 1000000.0);
		}
		
		double endTime = glfwGetTime();

		if (myPool->Get<bool>("config.debug.timing", false)) {
			FILE_LOG(logINFO) << format("Render Time: %05.6f", endTime - startTime);
		}

		// Re-enable the depth buffer
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);

	}

}

void __ResizeFBO(tools::AttribPool* pool, const std::string& name, uint32_t w, uint32_t h) {
	RenderTarget* buff = pool->Get<RenderTarget*>(name);
	if (buff) {
		buff->Resize(w, h);
	}
}

void Game::__Resized(void * userParam, uint32_t newWidth, uint32_t newHeight) {
	if (newWidth > 0 && newHeight > 0) {
		auto& ctx = ImGui::GetIO();
		ctx.DisplaySize.x = newWidth;
		ctx.DisplaySize.y = newHeight;

		__ResizeFBO(myPool, "fbo.particles", newWidth, newHeight);

		__ResizeFBO(myPool, "fbo.gBuffer", newWidth, newHeight);
		__ResizeFBO(myPool, "fbo.lightacc", newWidth, newHeight);
		__ResizeFBO(myPool, "fbo.composite", newWidth, newHeight);

		__ResizeFBO(myPool, "fbo.bloom.highlights", newWidth / 8, newHeight / 8);
		__ResizeFBO(myPool, "fbo.bloom.blurred.1", newWidth / 2, newHeight / 2);
		__ResizeFBO(myPool, "fbo.bloom.blurred.2", newWidth / 2, newHeight / 2);

		myCamera->SetAspectRatio(glm::vec2((float)newWidth, (float)newHeight));
	}
}
