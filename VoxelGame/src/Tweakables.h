/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Defines compiler level tweakables that let us control various parameters throughout the engine
*/
#pragma once

#define THREAD_POOL_DEFAULT_THREADS 2
#define THREAD_POOL_RESERVED_THREADS 2 // main thread & audio thread

#define TERRAIN_MIN_HEIGHT -32.0f
#define TERRAIN_MAX_HEIGHT 32.0f

// World generation tweakables
#define WORLDGEN_DIRT_DEPTH 4

// Graphics tweakables
#define DEFAULT_LIGHT_EPSILON 5.0f/256.0f
#define BLOOM_BLUR_PASSES 3

// Audio tweakables
#define AUDIO_MAX_CHANNELS 128
#define AUDIO_DOPPLER_SCALE 0.1f
#define AUDIO_DISTANCE_FACTOR 1.0f
#define AUDIO_ROLLOFF_FACTOR 1.0f
#define AUDIO_DEFAULT_3D_ROLLOFF 0x00200000 //FMOD_3D_LINEARROLLOFF