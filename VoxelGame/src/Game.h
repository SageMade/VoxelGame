/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Manages the core gameplay loop and initialization
*/
#pragma once

#include "tools/AttribPool.h"

#include "gameplay/VoxelChunk.h"
#include "graphics/Camera.h"
#include "gameplay/VoxelWorld.h"

#include "graphics/Window.h"
#include "graphics/Mesh.h"
#include "graphics/Shader.h"

#include "audio/AudioEngine.h"

class Game {
public:
	Game();
	~Game();

	void Run();

	void Init();

	void Update();
	void Render();

private:
	Window * myWindow;

	void __LoadShaders();
	void __HandleInput();
	void __InitAudio();
	void __InitGraphics();

	void __Resized(void* userParam, uint32_t newWidth, uint32_t newHeight);

	GLuint                myQueryId;
	graphics::Camera     *myCamera;
	gameplay::VoxelWorld *myWorld;
	audio::AudioEngine   *myAudioEngine;
	tools::AttribPool    *myPool;
};