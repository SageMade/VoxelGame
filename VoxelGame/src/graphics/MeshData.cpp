/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#include "MeshData.h"

void graphics::MeshData::Delete() {

	delete[] Vertices;  
	delete[] Indices;

	VertexCount = 0;
	IndexCount = 0;
	Vertices = nullptr;
	Indices = nullptr;
	VDecl = nullptr;
}

