#include "CommonTools.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Mesh.h"
#include "VertexLayouts.h"

#include "tools/GeoUtils.h"

void DrawFullscreenQuad() {
	static float verts[] = {
		-1.0f, -1.0f, 0.0f,
		-1.0f,  1.0f, 0.0f,
		 1.0f,  1.0f, 0.0f,
		 1.0f, -1.0f, 0.0f
	};
	static uint16_t indices[] = {
		0, 2, 1,
		2, 0, 3
	};
	static graphics::Mesh *mesh;
	static bool isInit = false;
	if (!isInit) {
		graphics::MeshData data;
		data.Initialize(verts, 4, indices, 6, &graphics::VertPosDecl);
		mesh = new graphics::Mesh(data);
		isInit = true;
	}
	mesh->Draw();
}

void DrawSphere(graphics::Shader* shader, const glm::vec3& pos, const float radius) {
	static graphics::Mesh *mesh;
	static bool isInit = false;
	if (!isInit) {
		graphics::MeshData data = GeoUtils::CreateSphere(1.0f);
		mesh = new graphics::Mesh(data);
		isInit = true;
	}
	shader->SetUniform("xWorld", glm::translate(pos) * glm::scale(glm::vec3(radius)));
	mesh->Draw();
}

void DrawCube(graphics::Shader* shader, const glm::vec3& pos, const glm::quat& rotation, const float halfSize) {
	DrawCube(shader, pos, rotation, glm::vec3(halfSize));
}

void DrawCube(graphics::Shader * shader, const glm::vec3 & pos, const glm::quat& rotation, const glm::vec3 & halfExtents) {
	static graphics::Mesh *mesh;
	static bool isInit = false;
	if (!isInit) {
		graphics::MeshData data = GeoUtils::CreateCube(glm::vec3(), glm::vec3(0.5f), glm::vec4(1.0f));
		mesh = new graphics::Mesh(data);
		isInit = true;
	}
	shader->SetUniform("xWorld", glm::translate(pos) * glm::mat4_cast(rotation) * glm::scale(halfExtents));
	mesh->Draw();
}

void DrawCone(graphics::Shader* shader, const glm::vec3& pos, const glm::quat& rotation, const float height, const float angle) {
	static graphics::Mesh *mesh;
	static bool isInit = false;
	if (!isInit) {
		graphics::MeshData data = GeoUtils::CreateCone(1.0f, 45, 10);
		mesh = new graphics::Mesh(data);
		isInit = true;
	}
	float scaleYZ = height * tan(glm::radians(angle));
	shader->SetUniform("xWorld", glm::translate(pos) * glm::mat4_cast(rotation) * glm::scale(glm::vec3(height, scaleYZ, scaleYZ)));
	mesh->Draw();
}

