/*
	Author: Shawn Matthews
	Date:   Feb 7 2018
*/
#pragma once
#ifndef TEXTURE3D_H
#define TEXTURE3D_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "logger.h"

namespace graphics {

	struct Texture3DDefinition {
		uint32_t  Width;
		uint32_t  Height;
		uint32_t  Depth;
		uint8_t  *Data;
		GLenum    FilterMode;
		GLenum    InternalPixelType;
		GLenum    PixelFormat;
		GLenum    PixelType;
		GLenum    WrapMode;
		bool      EnableAnisotropy;

		Texture3DDefinition() : Width(0), Height(0), Depth(0), Data(nullptr), FilterMode(GL_NEAREST), InternalPixelType(GL_RGBA),
			PixelFormat(GL_RGBA), PixelType(GL_UNSIGNED_BYTE), WrapMode(GL_CLAMP), EnableAnisotropy(true) {}
		Texture3DDefinition(
			const uint32_t width,
			const uint32_t height,
			const uint32_t depth,
			uint8_t *data,
			const GLenum filterMode = GL_NEAREST,
			const GLenum internalPixelType = GL_RGBA,
			const GLenum pixelFormat = GL_RGBA,
			const GLenum pixelType = GL_UNSIGNED_BYTE,
			const GLenum wrapMode = GL_CLAMP,
			const bool   enableAnisotropy = true);

		Texture3DDefinition(const std::string& filename, int xSlices, int ySlices);
		~Texture3DDefinition();
	};

    /**
        Manages creating and loading textures
    */
    class Texture3D
    {
        public:
            //destructor
            ~Texture3D();
            //constructors
			Texture3D() {} 
			Texture3D(const std::string& fileName, int xSlices, int ySlices, GLenum filterMode = GL_NEAREST, GLenum wrapMode = GL_CLAMP, bool enableAnisotropy = true);
			Texture3D(const uint32_t width, const uint32_t height, const uint32_t depth, uint8_t *data,
                                  const GLenum filterMode = GL_NEAREST,
                                  const GLenum internalPixelType = GL_RGBA,
                                  const GLenum pixelFormat = GL_RGBA,
                                  const GLenum pixelType = GL_UNSIGNED_BYTE,
                                  const GLenum wrapMode = GL_CLAMP,
                                  const bool   enableAnisotropy = true);
			Texture3D(const Texture3DDefinition& definition);
			Texture3D(const Texture3D& other) {
				FILE_LOG(logDEBUG) << "Texture copy detected. Should be removed!\n" << LOG_CALLSTACK();
			}

			/*
				Unloads this resource
			*/
			void Unload();

            /**
                Binds this texture to the given texture slot
                @param textureSlot The texture slot to bind to (EX: GL_TEXTURE0)
            */
            void Bind(uint8_t textureSlot);  
            /**
                Gets the GL handle to the underlying texture
            */
            inline GLuint GetHandle() const { return myTextureHandle; }

			static void Unbind(const GLenum location);

            inline uint32_t GetWidth() const { return myWidth; }
            inline uint32_t GetHeight() const { return myHeight; }
			inline uint32_t GetDepth() const { return myDepth; }

        private:
            // Stores the GL handle to the underlying texture
            GLuint myTextureHandle;

            // Stores the dimensions of the texture
			uint32_t   myWidth, myHeight, myDepth;

            void __Create(const uint32_t width, const uint32_t height, const uint32_t depth, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy = true);
    };

}

#endif // TEXTURE_H
