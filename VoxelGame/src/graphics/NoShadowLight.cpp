#include "NoShadowLight.h"

#include "graphics/CommonTools.h"
#include "Logger.h"

#include <chrono>

namespace graphics {

	void NoShadowLight::Render(Shader * shader, const Camera& camera) const {

		// Setting uniforms takes almost as long as rendering the mesh... weird.
		shader->SetUniform("xLight.Position",  camera.GetView() * glm::vec4(Position, 1.0f));
		shader->SetUniform("xLight.Color",     Color);
		shader->SetUniform("xLight.Radius",    Radius);

		if (Volume) {
			shader->SetUniform("xWorld", glm::translate(Position) * glm::scale(glm::vec3(Radius)));
			Volume->Draw();
		}
		else {
			switch (Type)
			{
			case SphereVolume:
				DrawSphere(shader, Position, Radius);
				break;
			case ConeVolume:
				DrawCone(shader, Position, glm::quat(EulerRotation), Radius, Angle);
				break;
			case CubeVolume:
				DrawCube(shader, Position, glm::quat(EulerRotation), Radius * 2.0f);
				break;
			default:
				break;
			}
		}


	}
}
