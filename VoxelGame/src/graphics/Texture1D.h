/*
Author: Shawn Matthews
Date:   Feb 7 2018
*/
#pragma once
#ifndef TEXTURE1D_H
#define TEXTURE1D_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "logger.h"

namespace graphics {

	struct Texture1DDefinition {
		uint32_t  Width;
		uint8_t  *Data;
		GLenum    FilterMode;
		GLenum    InternalPixelType;
		GLenum    PixelFormat;
		GLenum    PixelType;
		GLenum    WrapMode;
		bool      EnableAnisotropy;

		Texture1DDefinition() : Width(0), Data(nullptr), FilterMode(GL_NEAREST), InternalPixelType(GL_RGBA),
			PixelFormat(GL_RGBA), PixelType(GL_UNSIGNED_BYTE), WrapMode(GL_CLAMP), EnableAnisotropy(true) {}
		Texture1DDefinition(
			const uint32_t width,
			uint8_t *data,
			const GLenum filterMode = GL_NEAREST,
			const GLenum internalPixelType = GL_RGBA,
			const GLenum pixelFormat = GL_RGBA,
			const GLenum pixelType = GL_UNSIGNED_BYTE,
			const GLenum wrapMode = GL_CLAMP,
			const bool   enableAnisotropy = true);

		Texture1DDefinition(const std::string& filename);
		~Texture1DDefinition();
	};

    /**
        Manages creating and loading textures
    */
    class Texture1D
    {
        public:
            //destructor
            ~Texture1D();
            //constructors
            Texture1D() {}
            Texture1D(const char* fileName, GLenum filterMode = GL_NEAREST, GLenum wrapMode = GL_CLAMP, bool enableAnisotropy = true);
            Texture1D(const uint32_t width, uint8_t *data,
                                  const GLenum filterMode = GL_NEAREST,
                                  const GLenum internalPixelType = GL_RGBA,
                                  const GLenum pixelFormat = GL_RGBA,
                                  const GLenum pixelType = GL_UNSIGNED_BYTE,
                                  const GLenum wrapMode = GL_CLAMP,
                                  const bool   enableAnisotropy = true);
			Texture1D(const Texture1DDefinition& definition);
			Texture1D(const Texture1D& other) {
				FILE_LOG(logDEBUG) << "Texture copy detected. Should be removed!\n" << LOG_CALLSTACK();
			}

			/*
				Unloads this resource
			*/
			void Unload();

            /**
                Binds this texture to the given texture slot
                @param textureSlot The texture slot to bind to (EX: GL_TEXTURE0)
            */
            void Bind(uint8_t textureSlot);  
            /**
                Gets the GL handle to the underlying texture
            */
            inline GLuint GetHandle() const { return myTextureHandle; }

			static void Unbind(const GLenum location);

            inline uint32_t GetWidth() const { return myWidth; }

        private:
            // Stores the GL handle to the underlying texture
            GLuint myTextureHandle;

            // Stores the dimensions of the texture
			uint32_t   myWidth;

            void __Create(const uint32_t width, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy = true);
    };

}

#endif // TEXTURE1D_H
