/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Provides an entry point for the compiler to compile the STB implementations exactly once
*/
#pragma once

#ifndef STB_PROXY
#define STB_PROXY
#endif