/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#include "Camera.h"

#include <stdexcept>

namespace graphics {

	Camera::Camera() :
		myPerspectiveData(glm::vec4(60.0f, 16.0f / 9.0f, 0.0f, 0.0f)),
		myNearPlane(0.1f),
		myFarPlane(1000.0f),
		myPosition({ 0.0f, 0.0f, 0.0f }),
		myNormal({ 0.0f, 1.0f, 0.0f }),
		myUp({ 0.0f, 0.0f, 1.0f }),
		myRight({ 1.0f, 0.0f, 0.0f }),
		isOrthoEnabled(false),
		myViewMatrix(glm::mat4()),
		myProjectionMatrix(glm::mat4()),
		myViewProjection(glm::mat4()) { 
		__UpdateProjection();
		__UpdateView();
	}

	void Camera::MoveRelative(const glm::vec3 & val) {
		myPosition += myRight  * val.x;
		myPosition += myNormal * val.y;
		myPosition += myUp     * val.z;

		__UpdateView();
	}

	void Camera::LookAt(const glm::vec3 & pos, const glm::vec3 & up) {
		myNormal = glm::normalize(pos - myPosition);
		myRight = glm::cross(myNormal, up);
		myUp = glm::cross(myRight, myNormal);

		__UpdateView();
	}

	void Camera::Rotate(float yaw, float pitch, float roll) {
		glm::quat rotation = glm::rotate(glm::radians(yaw), myUp);
		rotation = rotation * (glm::quat)glm::rotate(glm::radians(pitch), rotation * myRight);
		rotation = rotation * (glm::quat)glm::rotate(glm::radians(roll), rotation * myNormal);
		myNormal = rotation * myNormal;
		myRight  = rotation * myRight;
		myUp     = rotation * myUp;

		__UpdateView();
	}

	void Camera::SetFieldOfView(const float fov) {
		if (fov <= 0 || fov >= 180)
			throw new std::invalid_argument("FOV must be between 0 and 180 degrees (exclusive)");

		myPerspectiveData.x = fov;
		isOrthoEnabled = false;
		__UpdateProjection();
	}

	void Camera::SetAspectRatio(const float aspectRatio) {
		if (aspectRatio <= 0)
			throw new std::invalid_argument("Aspect ratio must be greater than 0 (width / height)");
		
		myPerspectiveData.y = aspectRatio;
		isOrthoEnabled = false;
		__UpdateProjection();
	}

	void Camera::SetAspectRatio(glm::vec2 viewport) {
		if (viewport.x <= 0 || viewport.y <= 0)
			throw new std::invalid_argument("Aspect ratio must be greater than 0 (width / height)");
		myPerspectiveData.y = viewport.x / viewport.y;
		isOrthoEnabled = false;
		__UpdateProjection();
	}

	void Camera::SetOrtho(float left, float right, float bottom, float top) {
		myPerspectiveData = glm::vec4(left, right, bottom, top);
		isOrthoEnabled = true;
		__UpdateProjection();
	}

	void Camera::SetNearPlane(float nearField) {
		if (nearField >= myFarPlane)
			throw new std::invalid_argument("Near plane must be < far plane");

		myNearPlane = nearField;
		__UpdateProjection();
	}

	void Camera::SetFarPlane(float farField) {
		if (farField <= myNearPlane)
			throw new std::invalid_argument("Far plane must be > near plane");

		myFarPlane = farField;
		__UpdateProjection();
	}

	void Camera::SetClipPlanes(float near, float far) /* WHEREVER YOU AREE!!! */ {
		if (near >= far)
			throw new std::invalid_argument("Far plane must be > near plane");

		myNearPlane = near;
		myFarPlane = far;
		__UpdateProjection();
	}

	void Camera::__UpdateView() {
		myViewMatrix = glm::lookAt(myPosition, myPosition + myNormal, myUp);
		myViewProjection = myProjectionMatrix * myViewMatrix;
	}

	void Camera::__UpdateProjection() {
		// Lil trick here, we're using 1 for height so we can direct substitute the pre-calculated aspect ratio here
		if (isOrthoEnabled) {
			myProjectionMatrix = glm::ortho(myPerspectiveData.x, myPerspectiveData.y, myPerspectiveData.z, myPerspectiveData.w, myNearPlane, myFarPlane);
		}
		else {
			myProjectionMatrix = glm::perspectiveFov(glm::radians(myPerspectiveData.x), myPerspectiveData.y, 1.0f, myNearPlane, myFarPlane);
		}
		myViewProjection = myProjectionMatrix * myViewMatrix;
	}
}
