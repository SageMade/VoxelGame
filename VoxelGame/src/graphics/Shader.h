/**
    Defines the Shader class
    @date   Aug 2, 2017
    @author Shawn Matthews - 100412327
*/
#pragma once
#ifndef SHADER_H
#define SHADER_H

#include <map>

#include "glm_math.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace graphics { 

	class Shader
	{
	public:
		//constructor
		Shader();
		Shader(const Shader& other) = delete;
		//destructor
		virtual ~Shader();

		//functions
		void LoadShaderPart(GLenum type, const char* fileName);
		void LoadShaderPartFromSource(GLenum type, const char* source);
		void Link();
		void Enable() const; 
		void Unload();

		/*
			Binds a new vertex shader input to an attribute location from the CPU vertex data.
			Basically lets us bypass layout location=n in the GLSL shader.
			MUST be called before Link
			@param index The slot to bind the attribute to
			@param name  The name of the attribute in the shader to bind
		*/
		void AddAttribute(uint32_t index, const std::string& name); 
		/*
		Gets the location of an attribute within the vertex shader
		@param name The name of the vertex attribute to get the location for
		@returns The 0-based index of the attribute (basically layout= in GLSL)
		*/
		int GetAttribLocation(const std::string& name) const;

		/*
		Looks up the location of a shader uniform with the given name, returns -1 if
		not present
		@param name The name of the uniform to search for
		@returns The location of the uniform or -1 if non-existant
		*/
		int GetUniformLoc(const std::string& name);

		/*
			Gets the size of a uniform block in this shader. Basically how many bytes the uniform block uses
			@param name The name of the uniform block to inspect
			@returns The size of the uniform block in bytes
		*/
		int GetUniformArraySize(const std::string& name);

		/*
			Gets the total number of elements of type T that can fit in the uniform block with the given name
			@param name The name of the uniform block to inspect
			@returns The number of elements that can fit in the uniform block
		*/
		template <typename T>
		int GetBlockElementCount(const std::string& name) {
			return GetUniformArraySize(name) / sizeof(T);
		}

		/*
			Sets the binding location for a uniform buffer. (Maps a buffer to a buffer binding location)
			@param name The name of the uniform buffer to bind
			@param bindPoint The index to bind the buffer to
		*/
		void SetBufferBindingPoint(const std::string& name, GLuint bindPoint);

		void SetUniform(const  std::string& name, const glm::mat4 value, bool transpose = GL_FALSE);
		void SetUniform(const  std::string& name, const glm::mat3 value, bool transpose = GL_FALSE);
		template <typename T>
		void SetUniform(const  std::string& name, const T value) {
			SetUniform(myUniformLocs[name], value);
		}

		void SetUniform(const int location, const glm::mat4 value, bool transpose = GL_FALSE);
		void SetUniform(const int location, const glm::mat3 value, bool transpose = GL_FALSE);
		void SetUniform(const int location, const glm::vec4 value);
		void SetUniform(const int location, const glm::vec3 value);
		void SetUniform(const int location, const glm::vec2 value);
		void SetUniform(const int location, const float value);
		void SetUniform(const int location, const int value);
		void SetUniform(const int location, const bool value);
		void SetUniform(const int location, const uint64_t value);
		
		static void Unbind() { glUseProgram(GL_NONE); }

		static void InvalidateCurrent() { __BoundShaderId = -1; }

		//get
		inline const GLuint GetHandle() const { return myProgramHandle; }

	private:
		GLuint myProgramHandle;

		bool isLoaded = false;

		std::map<GLenum, GLuint> myShaderParts;
		struct DefaultNeg {
			int Value = -1;
			DefaultNeg() : Value(-1) {}
			DefaultNeg(int value) : Value(value) {}
			operator int() {
				return Value;
			}
		};
		std::map<std::string, DefaultNeg> myUniformLocs;

		void __PerformIntrospection();

		static GLuint __CreateShader(const GLenum type, const char* source);
		static GLuint __BoundShaderId;

	};
}
#endif // SHADER_H
