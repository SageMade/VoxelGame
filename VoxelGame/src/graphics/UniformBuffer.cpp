/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#include "graphics/UniformBuffer.h"

namespace graphics {

	UniformBuffer::UniformBuffer() :
		myData(nullptr), myHandle(0) {
	}

	UniformBuffer::~UniformBuffer() {
		if (myHandle)
			Unload();
	}

	void UniformBuffer::Init() {
		glGenBuffers(1, &myHandle);
	}

	void UniformBuffer::Unload() {
		glDeleteBuffers(1, &myHandle);
		myHandle = 0;
		if (myData)
			free(myData);
		myDataSize = 0;
	}

	void UniformBuffer::Bind(GLuint bindingPoint) {
		glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, myHandle);
	}

	void UniformBuffer::Update() {
		glBindBuffer(GL_UNIFORM_BUFFER, myHandle);
		glBufferData(GL_UNIFORM_BUFFER, myDataSize, myData, GL_DYNAMIC_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}

} 
