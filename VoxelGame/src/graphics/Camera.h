/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include "glm_math.h"

namespace graphics {

	class Camera {
	public:
		Camera();
		virtual ~Camera() {}

	// Accessors
	public:
		const glm::mat4& GetView() const { return myViewMatrix; }
		const glm::mat4& GetProjection() const { return myProjectionMatrix; }
		const glm::mat4& GetViewProjection() const { return myViewProjection; }

		glm::vec3 GetPosition() const { return myPosition; }
		glm::vec3 GetFacing() const { return myNormal; }
		glm::vec3 GetUpVector() const { return myUp; }
		glm::vec3 GetRightVector() const { return myRight; }

		float GetFieldOfView() const { return myPerspectiveData.x; }
		float GetAspectRatio() const { return myPerspectiveData.y; }
		float GetNearPlane() const { return myNearPlane; }
		float GetFarPlane() const { return myFarPlane; }

	// Mutators
	public:

		void SetPosition(const glm::vec3& pos) { myPosition = pos; __UpdateView(); }
		void MoveRelative(const glm::vec3& val);

		void LookAt(const glm::vec3& pos) { LookAt(pos, myUp); }
		void LookAt(const glm::vec3& pos, const glm::vec3& up);

		void Rotate(const glm::vec3& ypr) { Rotate(ypr.x, ypr.y, ypr.z); }
		void Rotate(float yaw, float pitch, float roll);

		void SetFieldOfView(const float fov);

		void SetAspectRatio(const float aspectRatio);
		void SetAspectRatio(glm::vec2 viewport);

		void SetOrtho(float left, float right, float bottom, float top);
		
		void SetNearPlane(float nearField);
		void SetFarPlane(float farField);
		void SetClipPlanes(float near, float far);
		void SetClipPlanes(glm::vec2 nearFar) { SetClipPlanes(nearFar.x, nearFar.y); }

	protected:
		glm::vec3 myPosition;

		glm::vec3 myNormal;
		glm::vec3 myUp;
		glm::vec3 myRight;

		glm::vec4 myPerspectiveData;
		bool      isOrthoEnabled;

		// float myOrthoLeft;    x
		// float myOrthoRight;   y
		// float myOrthoTop;     z
		// float myOrthoBottom;  w

		//float myFieldOfView;   x
		//float myAspectRatio;   y
		float myNearPlane;     
		float myFarPlane;      

		glm::mat4 myViewMatrix;
		glm::mat4 myProjectionMatrix;
		glm::mat4 myViewProjection;
		
		void __UpdateView();
		void __UpdateProjection();
	};

}