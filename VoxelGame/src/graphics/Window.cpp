/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#include <stdexcept>

#include "Window.h"
#include "Logger.h"

#include "gameplay/input/InputManager.h"

// Implementation of constructor
Window::Window(const uint32_t width, const uint32_t height, const char* title, const bool fullscreen, const bool vsyncEnabled) {

    // Set all the callbacks to null
    /*
    myResizeCallback             = NULL;
    myClosingCallback            = NULL;
    myMouseMoveCallback          = NULL;
    myKeyPressEventback          = NULL;
    myTextEventCallback          = NULL;
    myMousePressCallback         = NULL;
    myMouseScrollCallback        = NULL;
    myJoystickConnectionCallback = NULL;
    myPathDropCallback           = NULL;
    */

    // Copy our parameters to our data members
    myWidth        = width;
    myHeight       = height;
    myTitle        = title;
    isFullscreen   = fullscreen;
	isVsyncEnabled = vsyncEnabled;

	myResizeCallbacks = std::vector<std::function<void(void*, uint32_t, uint32_t)>>();

	myGameTime = WindowTime();
	myGameTime.currentFrame = 1.0 / 60.0;
	myGameTime.lastFrame = 0.0;

    Init();

	_handleGlfwResize(width, height);
}

// Implementation of window destructor
Window::~Window()
{
	// If our window was created
    if (myWindowHandle) {
        // Mark it for closure and clean up glfw
        glfwDestroyWindow(myWindowHandle);
        glfwTerminate();
    }
	gameplay::input::InputManager::Cleanup();
}

// Implementation of window init
void Window::Init() {
    static bool isGladInit = false;

	hasFocus = false;

    // Set the error callback to the static member function
    glfwSetErrorCallback(Window::__handleGlfwError);
	
    // Try to initialize the GLFW library
    if (!glfwInit()) {
        // Throw an exception if glfw failed to load
        throw new std::runtime_error("Failed to initialize GLFW");
    }

    /*
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    */

    // Use the 24-8 depth stencil setup
    glfwWindowHint(GLFW_DEPTH_BITS,   24);
    glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_SAMPLES,      4);

    // Create the window
    myWindowHandle = glfwCreateWindow(myWidth, myHeight, myTitle, NULL, NULL);


    // If we failed to create our window
    if (!myWindowHandle) {
        // Kill the GLFW framework and throw an exception
        glfwTerminate();
        throw new std::runtime_error("Failed to create window");
    }

    // Make our window our current context, and tie our window class to the window's user pointer
    glfwMakeContextCurrent(myWindowHandle);
    glfwSetWindowUserPointer(myWindowHandle, this);

    // Set the swap interval (enable/disable vsync)
    glfwSwapInterval((isVsyncEnabled ? 1 : 0));
			 
	// Enable sticky keys to prevent missing key presses
	glfwSetInputMode(myWindowHandle, GLFW_STICKY_KEYS, 1);

    // Set our window callbacks
	glfwSetWindowFocusCallback(myWindowHandle, Window::__handleGlfwFocus);
    glfwSetWindowSizeCallback(myWindowHandle, Window::__handleGlfwResize);
    glfwSetKeyCallback(myWindowHandle, Window::__handleGlfwKeyEvent);
    glfwSetMouseButtonCallback(myWindowHandle, Window::__handleGlfwMouseButton);
    glfwSetCursorPosCallback(myWindowHandle, Window::__handleGlfwMouseMove);
    glfwSetScrollCallback(myWindowHandle, Window::__handleGlfwMouseScroll);
    glfwSetWindowCloseCallback(myWindowHandle, Window::__handleGlfwWindowClose);
    glfwSetCharModsCallback(myWindowHandle, Window::__handleGlfwTextEvent);

    if (!isGladInit) {
        // Try to initialize glad, and throw an exception if it fails
        if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
            throw new std::runtime_error("Failed to initialize GLAD");
        }
        isGladInit = true;
    }

    // Tie the GL message callback, and pass our window as the user param
	glEnable(GL_DEBUG_OUTPUT);
	// Disable notifications
	glDebugMessageControl(GL_DEBUG_SOURCE_API, GL_DEBUG_TYPE_OTHER, GL_DEBUG_SEVERITY_NOTIFICATION, 0, nullptr, false);
    glDebugMessageCallback((GLDEBUGPROC)Window::__handleGlDebug, this);

	glEnable(GL_MULTISAMPLE);

    // Get our render and version strings, and log them
    const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte* version = glGetString(GL_VERSION); // version as a string

    // Log the renderer and opengl
    FILE_LOG(logINFO) << "Renderer: " << renderer;
    FILE_LOG(logINFO) << "OpenGL version: " << version;

	gameplay::input::InputManager::SetWindow(this);
	gameplay::input::InputManager::Init();
}

// Enable or disable vsync
void Window::SetVSyncEnabled(const bool value) {
    // Update our private member and update the glfw interval
    isVsyncEnabled = value;
    glfwSwapInterval((isVsyncEnabled ? 1.0 / 60 : 0.0));
}

// Handles resizing for this window instance
void Window::Resize(const uint newWidth, const uint newHeight) {
    // Make sure we don't try to set the sizes to be invalid
    if (newWidth <= 0 || newHeight <= 0)
        throw std::runtime_error("Attempted to resize to <= 0");

    // Pass the resize request to glfw
    glfwSetWindowSize(myWindowHandle, newWidth, newHeight);
}

glm::vec2 Window::GetPosition() const{
	int xPos, yPos;
	glfwGetWindowPos(myWindowHandle, &xPos, &yPos);
	return glm::vec2((float)xPos, (float)yPos);
}

// Handle the update
void Window::Update() {
    // Update our timing values
    myGameTime.currentFrame = glfwGetTime();
    myGameTime.delta = myGameTime.currentFrame - myGameTime.lastFrame;
    myGameTime.lastFrame = myGameTime.currentFrame;

    // Poll for input events
    glfwPollEvents();
	gameplay::input::InputManager::Poll();
}

// Implementation of swapbuffers, just call the GLFW command
void Window::SwapBuffers() const {
    glfwSwapBuffers(myWindowHandle);
}

// Implementation of init render loop
void Window::InitRenderLoop() {
    // We want to ignore any loading time since initializing glfw, so reset timer to 0
    glfwSetTime(0.0f);

    if (myResizeCallback != NULL)
        myResizeCallback(myUserParam, myWidth, myHeight);
}

// Implementation of Close
void Window::Close() {
    glfwSetWindowShouldClose(myWindowHandle, GLFW_TRUE);
}

// Implementation of Closing function, will check to see if window should close
bool Window::Closing() const {
    return glfwWindowShouldClose(myWindowHandle);
}

bool Window::HasFocus() const {
	return hasFocus;
}

// Implementation of handle glfw window closing
void Window::_handleGlfwClose() {
    if(myClosingCallback != NULL) {
        bool cancelClose = false;
        myClosingCallback(myUserParam, cancelClose);

        if (cancelClose)
            glfwSetWindowShouldClose(myWindowHandle, GLFW_FALSE);
    }
}

// Implementation of handle glfw window resize
void Window::_handleGlfwResize(const uint32_t width, const uint32_t height) {
    myWidth = width;
    myHeight = height;

    glViewport(0, 0, width, height);
			
	for (int ix = 0; ix < myResizeCallbacks.size(); ix++)
		myResizeCallbacks[ix](myUserParam, width, height);

    if (myResizeCallback != NULL)
        myResizeCallback(myUserParam, width, height);
}

// Implementation of handle glfw mouse button press input
void Window::_handleGlfwMouseButton(const int button, const int action, const int mods) {
    if (myMousePressCallback != NULL)
        myMousePressCallback(myUserParam, button, action, mods);
}

// Implementation of handle glfw keyboard press input
void Window::_handleGlfwKeyEvent(const int key, const int scanCode, const int action, const int mods) {
    if (myKeyPressEventback != NULL)
        myKeyPressEventback(myUserParam, key, scanCode, action, mods);
}

// Implementation of handle glfw mouse move input
void Window::_handleGlfwMouseMove(const double xPos, const double yPos) {
    if (myMouseMoveCallback != NULL)
        myMouseMoveCallback(myUserParam, xPos, yPos);
}

// Implementation of handle glfw scroll input
void Window::_handleGlfwScroll(const double xScroll, const double yScroll) {
    if (myMouseScrollCallback != NULL)
        myMouseScrollCallback(myUserParam, xScroll, yScroll);
}

// Implementation of handle glfw text input
void Window::_handleGlfwTextInput(uint codepoint, const int mods) {
    if (myTextEventCallback != NULL)
        myTextEventCallback(myUserParam, codepoint, mods);
}

// Implementation of path dropping
void Window::_handleGlfwDrop(int numPaths, char** paths) {
    if (myPathDropCallback != NULL)
        myPathDropCallback(myUserParam, numPaths, paths);
}

// Gets the clipboard text
const char* Window::GetClipboardString() const {
    return glfwGetClipboardString(myWindowHandle);
}

// Sets the clipboard text
void Window::SetClipboardString(const char* text) {
    glfwSetClipboardString(myWindowHandle, text);
}

bool Window::IsMousePressed(const int button) const {
    return glfwGetMouseButton(myWindowHandle, button);
}

bool Window::IsKeyPressed(const int button) const {
    return glfwGetKey(myWindowHandle, button);
}

/*
    The static callbacks work in a similar fashion:

    Get the callback from the window and cast to our window type
    Try to call the corresponding function in the window instance
    Otherwise log the callback
*/

void Window::__handleGlfwResize(GLFWwindow* window, int newWidth, int newHeight) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwResize(newWidth, newHeight);
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

void Window::__handleGlfwFocus(GLFWwindow * window, int focus) {
	Window* ptr = (Window*)glfwGetWindowUserPointer(window);
	if (ptr)
		ptr->hasFocus = focus;
}

void Window::__handleGlfwKeyEvent(GLFWwindow* window, int key, int scancode, int action, int mods) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwKeyEvent(key, scancode, action, mods);
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

void Window::__handleGlfwMouseButton(GLFWwindow* window, int button, int action, int mods) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwMouseButton(button, action, mods);
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

void Window::__handleGlfwMouseMove(GLFWwindow* window, double xPos, double yPos) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwMouseMove(xPos, yPos);
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

// Handles the glfw window mouse scroll
void Window::__handleGlfwMouseScroll(GLFWwindow* window, double xScroll, double yScroll) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwMouseMove(xScroll, yScroll);
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

// Handles the glfw window close event, invoked
void Window::__handleGlfwWindowClose(GLFWwindow* window) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwClose();
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

void Window::__handleGlfwTextEvent(GLFWwindow* window, uint codepoint, int mods) {
    // Get the custom window instance from the glfw window
    Window* ptr = (Window*)glfwGetWindowUserPointer(window);

    // Make sure we got our class before calling the member
    if (ptr)
        ptr->_handleGlfwTextInput(codepoint, mods);
    else
        FILE_LOG(logWARNING) << "Received callback for unregistered window";
}

// Handles and GLFW errors, this will simply log it
void Window::__handleGlfwError(int error, const char* message) {
    FILE_LOG(logERROR) << "GLFW Exception - " << error << std::endl << message;
}

// Handles GL debug info
void CALLBACK Window::__handleGlDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
            // Pick what utils/Logger to use based on severity
            if (severity > DEBUG_SEVERITY_MEDIUM)
                FILE_LOG(logERROR)  << "GL ERROR " << source << " - " << message;
            else if (severity > DEBUG_SEVERITY_NOTIFICATION)
                FILE_LOG(logWARNING)  << "GL WARN "  << source << " - " << message;
            else
                FILE_LOG(logINFO)  << "GL INFO "  << source << " - " << message;
        }

