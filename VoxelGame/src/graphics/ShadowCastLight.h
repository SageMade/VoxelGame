#pragma once

#include "RenderTarget.h"
#include "Camera.h"
#include "Shader.h"

namespace graphics {

	class ShadowCastLight {
	public:
		ShadowCastLight(const glm::ivec2& bounds);
		ShadowCastLight(const int boundsX, const int boundsY);
		~ShadowCastLight();

		void SetupOrthoProjection(const float extentsX, const float extentsY);
		void SetupPerpectiveProjection(const float fov, const float aspectRatio);

		void SetPosition(const glm::vec3& pos);
		void SetNormal(const glm::vec3& normal);
		void LookAt(const glm::vec3& target, const glm::vec3& up = glm::vec3(1.0f, 0.0f, 0.0f));

		glm::vec3 GetPosition() const;
		glm::vec3 GetNormal() const;

		glm::mat4 GetView() const { return myCamera->GetView(); }
		glm::mat4 GetProjection() const { return myCamera->GetProjection(); }

		void SetDepthRange(const glm::vec2& range);
		void SetDepthRange(const float rangeMin, const float rangeMax);

		void BindForRender(Shader* shader);
		void BindForSampling(Shader* shader, const int slot);

		void SetColor(const glm::vec3& color) { myColor = color; }
		void SetColor(const glm::vec4& color) { myColor = color.rgb * color.a; };

		void SetAttenuationFactor(const float value);
		float GetAttenuationFactor() const;

		void SetAttenuationRange(const float value);
		float GetAttenuationRange() const;

		bool NeedsRender() const;

		void SetDynamic(const bool value) { isDynamic = value; }
		bool IsDynamic() const { return isDynamic; }

		void NotifyRenderComplete();

		glm::vec3 GetColor() const { return myColor; }

		Camera* GetCameraPtr() const { return myCamera; }
		RenderTarget* GetRenderTargetPtr() const { return myDepthTarget; }
	
	private:
		RenderTarget *myDepthTarget;
		Camera       *myCamera;
		bool          isTargetOwner;
		bool          isCameraOwner;

		bool          isDynamic;
		bool          hasRendered;

		// Light parameters
		float         myAttenuationFactor;
		float         myAttenuationRange;
		glm::vec3     myColor;
		
	};

}