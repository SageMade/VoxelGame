/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#pragma once
#ifndef MESHDATA_H_INCLUDED
#define MESHDATA_H_INCLUDED

#include "VertexDeclaration.h"

#include <functional>

namespace graphics {
	


    /**
        Represents a set of geometry with vertices and indices
    */
    struct MeshData {
        /**  Stores a pointer to the vertex data for the mesh  */
        void   *Vertices;
        /**  Stores the number of vertices in the mesh part  */
		uint16_t  VertexCount;
        /**  Stores a pointer to the index data for the mesh  */
		uint16_t *Indices;
        /**  Stores the number of indices in this mesh  */
		uint16_t  IndexCount;
		/** Stores the vertex declaration used by this mesh data */
		const VertexDeclaration *VDecl;

        /** Default constructor  */
		MeshData() {};

		template <typename T>
		void Initialize(T* vertexData, uint16_t vertexCount, uint16_t* indices, uint16_t indexCount, const VertexDeclaration * vDecl) {
			IndexCount = indexCount;
			VertexCount = vertexCount;
			VDecl = vDecl;

			Vertices = vertexData;
			Indices = indices;
		}

		void Delete();

		void* GetVertex(const int index) {
			return (char*)Vertices + (index * VDecl->ElementSize);
		}

		template <typename T>
		T* ResolveVertexAttrib(const int index, const int attribOffset) {
			return reinterpret_cast<T*>((char*)GetVertex(index) + attribOffset);
		}

	private:
		std::function<void()> __myDestructor;  
    };
}

#endif // MESHDATA_H_INCLUDED
