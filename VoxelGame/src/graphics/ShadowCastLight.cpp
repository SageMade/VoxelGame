#include "ShadowCastLight.h"

namespace graphics {
	ShadowCastLight::ShadowCastLight(const glm::ivec2 & bounds) 
		:ShadowCastLight(bounds.x, bounds.y)
	{ }

	ShadowCastLight::ShadowCastLight(const int boundsX, const int boundsY)
	{
		myColor = glm::vec3(1.0f);
		myAttenuationFactor = 0.0f;
		myAttenuationRange = 1000.0f;
		
		isDynamic = true;
		hasRendered = false;

		myCamera = new Camera();
		myCamera->SetPosition(glm::vec3(0.0f));
		myCamera->LookAt(glm::vec3(1.0f, 0.0f, 0.0f));

		myCamera->SetOrtho(-1.0f, 1.0f, -1.0f, 1.0f);

		myDepthTarget = new RenderTarget(boundsX, boundsY);
		myDepthTarget->AddDepthBuffer();
		myDepthTarget->Finalize();

		isCameraOwner = true;
		isTargetOwner = true;
	}

	ShadowCastLight::~ShadowCastLight() {
		if (isTargetOwner)
			delete myDepthTarget;
		if (isCameraOwner)
			delete myCamera;

		myCamera = nullptr;
		myDepthTarget = nullptr;
		isTargetOwner = false;
		isCameraOwner = false;
	}

	void ShadowCastLight::SetupOrthoProjection(const float extentsX, const float extentsY) {
		myCamera->SetOrtho(-extentsX / 2.0f, extentsX / 2.0f, -extentsY / 2.0f, extentsY / 2.0f);
	}

	void ShadowCastLight::SetupPerpectiveProjection(const float fov, const float aspectRatio) {
		myCamera->SetFieldOfView(fov);
		myCamera->SetAspectRatio(aspectRatio);
	}

	void ShadowCastLight::SetPosition(const glm::vec3& pos) {
		myCamera->SetPosition(pos);
	}

	void ShadowCastLight::SetNormal(const glm::vec3& normal) {
		myCamera->LookAt(myCamera->GetPosition() + normal);
	}

	void ShadowCastLight::LookAt(const glm::vec3& target, const glm::vec3& up) {
		myCamera->LookAt(target, up);
	}

	glm::vec3 ShadowCastLight::GetPosition() const {
		return myCamera->GetPosition();
	}

	glm::vec3 ShadowCastLight::GetNormal() const {
		return myCamera->GetFacing();
	}

	void ShadowCastLight::SetDepthRange(const glm::vec2& range) {
		if (range.x < range.y && range.x > 0)
			myCamera->SetClipPlanes(range);
	}

	void ShadowCastLight::SetDepthRange(const float rangeMin, const float rangeMax) {
		if (rangeMin < rangeMax && rangeMin > 0)
			myCamera->SetClipPlanes(rangeMin, rangeMax);
	}

	void ShadowCastLight::BindForRender(Shader * shader) {
		myDepthTarget->Bind();
		
		// Clear the buffer to transparent grey
		glClear(GL_DEPTH_BUFFER_BIT);
		
		shader->SetUniform("xView", myCamera->GetView());
		shader->SetUniform("xProjection", myCamera->GetProjection());
	}

	void ShadowCastLight::BindForSampling(Shader* shader, const int slot) {
		myDepthTarget->BindDepthTex(slot);

		shader->SetUniform("xLight.Color", myColor);
		shader->SetUniform("xLight.Radius", myAttenuationRange);
		shader->SetUniform("xAttenuationFactor", myAttenuationFactor);
	}

	void ShadowCastLight::SetAttenuationFactor(const float value) {
		myAttenuationFactor = glm::clamp(value, 0.0f, 1.0f);
	}

	float ShadowCastLight::GetAttenuationFactor() const {
		return myAttenuationFactor;
	}

	void ShadowCastLight::SetAttenuationRange(const float value) {
		if (value > 0)
			myAttenuationRange = value;
	}

	float ShadowCastLight::GetAttenuationRange() const {
		return myAttenuationRange;
	}

	bool ShadowCastLight::NeedsRender() const {
		return isDynamic || !hasRendered;
	}

	void ShadowCastLight::NotifyRenderComplete() {
		hasRendered = true;
	}

}
