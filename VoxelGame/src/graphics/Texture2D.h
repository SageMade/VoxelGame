/**
    Defines the Texture Class
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#pragma once
#ifndef TEXTURE2D_H
#define TEXTURE2D_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "logger.h"

namespace graphics {

	struct Texture2DDefinition {
		uint32_t   Width;
		uint32_t   Height;
		uint8_t   *Data;
		GLenum     FilterMode;
		GLenum     InternalPixelType;
		GLenum     PixelFormat;
		GLenum     PixelType;
		GLenum     WrapMode;
		bool       EnableAnisotropy;
		char       Name[16];
		char       FileName[256];

		Texture2DDefinition() : Width(0), Height(0), Data(nullptr), FilterMode(GL_NEAREST), InternalPixelType(GL_RGBA),
			PixelFormat(GL_RGBA), PixelType(GL_UNSIGNED_BYTE), WrapMode(GL_CLAMP), EnableAnisotropy(true) {
			memset(Name, 0, 16);
			memset(FileName, 0, 256);
		}
		Texture2DDefinition(
			const uint32_t width,
			const uint32_t height,
			uint8_t *data,
			const GLenum filterMode = GL_NEAREST,
			const GLenum internalPixelType = GL_RGBA,
			const GLenum pixelFormat = GL_RGBA,
			const GLenum pixelType = GL_UNSIGNED_BYTE,
			const GLenum wrapMode = GL_CLAMP,
			const bool   enableAnisotropy = true);

		Texture2DDefinition(const std::string& filename);
		~Texture2DDefinition();
	};

    /**
        Manages creating and loading textures
    */
    class Texture2D
    {
        public:
            //destructor
            ~Texture2D();
            //constructors
            Texture2D() {}
            Texture2D(const char* fileName, GLenum filterMode = GL_NEAREST, GLenum wrapMode = GL_CLAMP, bool enableAnisotropy = true);
            Texture2D(const uint32_t width, const uint32_t height, uint8_t *data,
                                  const GLenum filterMode = GL_NEAREST,
                                  const GLenum internalPixelType = GL_RGBA,
                                  const GLenum pixelFormat = GL_RGBA,
                                  const GLenum pixelType = GL_UNSIGNED_BYTE,
                                  const GLenum wrapMode = GL_CLAMP,
                                  const bool   enableAnisotropy = true);
			Texture2D(const Texture2DDefinition& definition);
			Texture2D(const Texture2D& other) {
				FILE_LOG(logDEBUG) << "Texture copy detected. Should be removed!\n" << LOG_CALLSTACK();
			}

			/*
				Unloads this resource
			*/
			void Unload();

            /**
                Binds this texture to the given texture slot
                @param textureSlot The texture slot to bind to (EX: GL_TEXTURE0)
            */
            void Bind(uint8_t textureSlot);  
            /**
                Gets the GL handle to the underlying texture
            */
            inline GLuint GetHandle() const { return myTextureHandle; }

			static void Unbind(const GLenum location);

            inline uint32_t GetWidth() const { return myWidth; }
            inline uint32_t GetHeight() const { return myHeight; }

        private:
            // Stores the GL handle to the underlying texture
            GLuint myTextureHandle;

            // Stores the dimensions of the texture
			uint32_t   myWidth, myHeight;

            void __Create(const uint32_t width, const uint32_t height, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy = true);
    };

}

#endif // TEXTURE_H
