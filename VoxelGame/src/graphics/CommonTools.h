#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "glm_math.h"
#include "graphics/Shader.h"

void  DrawFullscreenQuad();
void  DrawSphere(graphics::Shader* shader, const glm::vec3& pos, const float radius);
void  DrawCube(graphics::Shader* shader, const glm::vec3& pos, const glm::quat& rotation, const float halfSize);
void  DrawCube(graphics::Shader* shader, const glm::vec3& pos, const glm::quat& rotation, const glm::vec3& halfExtents);
void  DrawCone(graphics::Shader* shader, const glm::vec3& pos, const glm::quat& rotation, const float height, const float angle);

constexpr bool IsPixelType(GLenum value) {
	switch (value) {
		case GL_UNSIGNED_BYTE:
		case GL_BYTE:
		case GL_UNSIGNED_SHORT:
		case GL_SHORT:
		case GL_UNSIGNED_INT:
		case GL_INT:
		case GL_FLOAT:
		case GL_UNSIGNED_BYTE_3_3_2:
		case GL_UNSIGNED_BYTE_2_3_3_REV:
		case GL_UNSIGNED_SHORT_5_6_5:
		case GL_UNSIGNED_SHORT_5_6_5_REV:
		case GL_UNSIGNED_SHORT_4_4_4_4:
		case GL_UNSIGNED_SHORT_4_4_4_4_REV:
		case GL_UNSIGNED_SHORT_5_5_5_1:
		case GL_UNSIGNED_SHORT_1_5_5_5_REV:
		case GL_UNSIGNED_INT_8_8_8_8:
		case GL_UNSIGNED_INT_8_8_8_8_REV:
		case GL_UNSIGNED_INT_10_10_10_2:
		case GL_UNSIGNED_INT_2_10_10_10_REV:
		case GL_UNSIGNED_INT_24_8:
		case GL_UNSIGNED_INT_10F_11F_11F_REV:
		case GL_UNSIGNED_INT_5_9_9_9_REV:
		case GL_FLOAT_32_UNSIGNED_INT_24_8_REV:
			return true;
		default:
			return false;
	}
}

constexpr bool IsPixelFormat(GLenum value) {
	switch (value) {
		case GL_RED:
		case GL_RG: 
		case GL_RGB: 
		case GL_BGR: 
		case GL_RGBA: 
		case GL_BGRA: 
		case GL_RED_INTEGER: 
		case GL_RG_INTEGER: 
		case GL_RGB_INTEGER: 
		case GL_BGR_INTEGER: 
		case GL_RGBA_INTEGER: 
		case GL_BGRA_INTEGER: 
		case GL_STENCIL_INDEX: 
		case GL_DEPTH_COMPONENT: 
		case GL_DEPTH_STENCIL:
			return true;
		default:
			return false;
	}
}