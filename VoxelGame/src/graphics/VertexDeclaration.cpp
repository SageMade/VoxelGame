/**
    Implementation file for our VertexDeclaration, which lays out how mesh data is interpreted by our shaders
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/

#include <cstdarg>
#include <vector>
#include <cstdlib>

#include "graphics/VertexDeclaration.h"

namespace graphics {

	const VertexElement VertexElement::Null = VertexElement();
	
    // Implementation of VertexDeclaration constructor
    VertexDeclaration::VertexDeclaration(const uint32_t count, ...)
    {
        // Set our total element size and individual element sizes to zero
        ElementSize = 0;
        int eSize   = 0;

        /* PositionColorNormalTexture

            vec3 - position
                float - 4
                float - 4
                float - 4

            vec4 - color
                float - 4
                float - 4
                float - 4
                float - 4

            vec3 - normal
                float - 4
                float - 4
                float - 4

            vec2 - texture uv
                float - 4
                float - 4

            total size = 12 * 4 = 48 bytes
        */
		
        // Make a virtual argument list to iterate over our arguments
        va_list args;
        // Start getting our args
        va_start(args, count);

		myElements = (VertexElement*)malloc(count * sizeof(VertexElement));

		int realCount = 0;

        // Loop over all arguments
        for (int i = 0; i < count; i ++) {
            // Get the next argument as a Vertex Elemement
            VertexElement element = va_arg(args, VertexElement);

			if (element.Usage != VType_Unknown) {

				// We need to determine the element size, so we use a simple switch
				switch (element.Type) {
				case GL_BYTE:
					eSize = 1 * element.Size;
					break;
				case GL_UNSIGNED_BYTE:
					eSize = 1 * element.Size;
					break;
				case GL_SHORT:
					eSize = 2 * element.Size;
					break;
				case GL_UNSIGNED_SHORT:
					eSize = 2 * element.Size;
					break;
				case GL_FIXED:
					eSize = 4 * element.Size;
					break;
				case GL_FLOAT:
					eSize = 4 * element.Size;
					break;
				case GL_INT:
					eSize = 4 * element.Size;
					break;
				}

				// Our offset is whatever our last calculated element size is
				element.Offset = ElementSize;
				// Add the size of the element to the total size
				ElementSize += eSize;

				realCount++;
				
				// Add the element to the vector
				myElements[i] = element;
			}
			else {
				i--;
			}
        }
		
		myElementCount = realCount;

		if (realCount != count) {
			VertexElement* newCopy = (VertexElement*)malloc(realCount * sizeof(VertexElement));
			memcpy(newCopy, myElements, realCount * sizeof(VertexElement));
			delete[] myElements;
			myElements = newCopy;
		}

        // Stop getting variable args
        va_end(args);

    }

    VertexDeclaration::~VertexDeclaration() {

    }

    // Implementation of the applies function
    void VertexDeclaration::Apply() const {

        // Iterate over all our elements
        for(uint32_t i = 0; i < myElementCount; i ++) {

            // Get the element from the list
            VertexElement element = myElements[i];

            // Apply it to the VAO
            glEnableVertexAttribArray(element.Location);
            glVertexAttribPointer(element.Location, element.Size, element.Type, element.Normalized, ElementSize, (void*)element.Offset);
        }
    }
	const VertexElement& VertexDeclaration::Introspect(VType usage) const {
		for (int ix = 0; ix < myElementCount; ix++) {
			if (myElements[ix].Usage == usage) {
				return myElements[ix];
			}
		}
		return VertexElement::Null;
	}
}



