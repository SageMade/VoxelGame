/*
	Authors:
	Shawn M.          100412327
	Paul Puig         100656910
	Stephen Richards  100458273
*/

#include "TextureCollection.h"

#include <sstream>

#include "tools/FileUtils.h"
#include "Renderer.h"

namespace graphics {
	namespace particles {
		using namespace graphics;
		using namespace tools;

		Texture2D* TextureCollection::myTextures[255];

		void TextureCollection::LoadTexture(uint8_t id, const char * filename) {
			if (myTextures[id] != nullptr && myTextures[id]->GetHandle() != 0) {
				delete myTextures[id];
				myTextures[id] = nullptr;
			}

			myTextures[id] = new Texture2D(filename);
		}

		Texture2D* TextureCollection::Get(uint8_t id) {
			return myTextures[id];
		}

		void TextureCollection::WriteToFile(std::fstream& stream) {
			throw std::exception("Not Implemented");
			/*
			uint8_t count = 0;
			std::streampos pos = stream.tellg();
			Write(stream, count);
			for (int ix = 0; ix < 255; ix++) {
				if (myTextures[ix] != nullptr && myTextures[ix]->GetHandle() != 0) {
					count++;
					Write(stream, (uint8_t)ix);
					myTextures[ix].writeToFile(stream);
				}
			}
			std::streampos endPos = stream.tellg();
			stream.seekg(pos);
			Write(stream, count);
			stream.seekg(endPos);
			endPos = stream.tellg();
			*/
		}

		void TextureCollection::ReadFromFile(std::fstream & stream) {
			uint8_t count = 0;
			Read(stream, count);
			uint8_t location = 0;
			GLenum garbage;
			for (int ix = 0; ix < count; ix++) {
				Read(stream, location);
				Texture2DDefinition definition;
				Read(stream, definition.Name, 16);
				Read(stream, definition.FilterMode);
				Read(stream, definition.WrapMode);
				Read(stream, definition.InternalPixelType);
				Read(stream, definition.PixelFormat);
				Read(stream, definition.PixelType);
				Read(stream, garbage);
				Read(stream, definition.Width);
				Read(stream, definition.Height);
				bool readData = false;
				Read(stream, readData);
				if (readData) {
					int pixelSize = 1;
					switch (definition.PixelType) {
					case GL_UNSIGNED_BYTE:
						pixelSize = 1;
						break;
					case GL_FLOAT:
						pixelSize = sizeof(float);
						break;
					}
					switch (definition.PixelFormat) {
					case GL_RGBA:
						pixelSize *= 4;
						break;
					case GL_RGB:
						pixelSize *= 3;
						break;
					}
					definition.Data = (uint8_t*)malloc(definition.Width * definition.Height * pixelSize);
					Read(stream, definition.Data, definition.Width * definition.Height * pixelSize);
					myTextures[location] = new Texture2D(definition);// readFromFile(stream);
				}
				else {
					size_t stringSize = 0;
					Read(stream, stringSize);
					char* data = new char[stringSize];
					Read(stream, data, stringSize);
					memcpy(definition.FileName, data, stringSize);
					myTextures[location] = new Texture2D(definition.FileName);// readFromFile(stream);
				}
				Renderer::SetTexture(location, myTextures[location]->GetHandle());
			}
		}
	}
}
