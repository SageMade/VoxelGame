/*
	Authors:
	Shawn M.          100412327
	Paul Puig         100656910
	Stephen Richards  100458273
*/
#pragma once

#include "graphics/Texture2D.h"

#include <fstream>

namespace graphics {
	namespace particles {
		class TextureCollection {
		public:
			static void LoadTexture(uint8_t id, const char* filename);
			static graphics::Texture2D* Get(uint8_t id);

			static void WriteToFile(std::fstream& stream);
			static void ReadFromFile(std::fstream& stream);

		private:
			static graphics::Texture2D* myTextures[255];
		};
	}
}