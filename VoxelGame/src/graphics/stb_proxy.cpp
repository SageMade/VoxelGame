/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>
#define STB_PERLIN_IMPLEMENTATION
#include <stb_perlin.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>