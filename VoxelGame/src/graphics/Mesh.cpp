/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/

#include "Mesh.h"

#include "VertexDeclaration.h"
#include "Logger.h"

namespace graphics {

    // Implementation of mesh constructor
    Mesh::Mesh(const MeshData& data, GLenum primitiveMode, GLenum usage) {
        // Update our local fields
        myVertexCount   = data.VertexCount;
        myIndexCount    = data.IndexCount;
        myVertexSize    = data.VDecl->ElementSize;

		myUsage         = usage;
		myPrimitiveMode = primitiveMode;

		Transform = glm::mat4(1.0f);
		
        // Generate our vertex array binding
        glGenVertexArrays(1, &myVao);

        // Generate some buffers for our vertices and indices
        glGenBuffers(1, &myVbo);
        glGenBuffers(1, &myIbo);

        // Make sure our creation was successful
        if (myVao == 0 || myVbo == 0 || myIbo == 0) {
            // Otherwise delete any objects we DID make
            glDeleteBuffers(1, &myVbo);
            glDeleteBuffers(1, &myIbo);
            glDeleteVertexArrays(1, &myVao);

            // and throw an exception
            throw std::runtime_error("Failed to generate VAO, VBO, or IBO");
        }

        // Bind our VAO, let's us tie commands to it
        glBindVertexArray(myVao);

        // Bind and buffer our vertex data
        glBindBuffer(GL_ARRAY_BUFFER, myVbo);
        glBufferData(GL_ARRAY_BUFFER, myVertexCount * myVertexSize, data.Vertices, myUsage);

        // Apply our vertex layout
		data.VDecl->Apply();

        // Bind and buffer our index data
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myIbo); 
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, myIndexCount * sizeof(USHORT), data.Indices, myUsage);

        // Bind the default vertex array so ours isn't accidentally modified
        glBindVertexArray(0);
		
        FILE_LOG(logINFO)  << "Creating mesh " << myVao << " (VBO: " << myVbo << ", IBO: " << myIbo  << ")";  
    }

    // Implementation of destructor
    Mesh::~Mesh() {
		//Unload();
    }

	void Mesh::Unload() {
		// Delete our VAO
		glDeleteVertexArrays(1, &myVao);

		// Delete our buffers
		glDeleteBuffers(1, &myVbo);
		glDeleteBuffers(1, &myIbo);

		FILE_LOG(logINFO) << "Deleting mesh " << myVao << " (VBO: " << myVbo << ", IBO: " << myIbo << ")";
	}
	
	void Mesh::Update(void* vertexData, int numVertices, USHORT* indexData, int numIndices) {
        glBindVertexArray(myVao);

		// Warn on update if not a streaming mesh
		if (myUsage == GL_STATIC_DRAW && !(myWarnFlags & USAGE)) {
			FILE_LOG(logWARNING) << "Mesh usage warning: updating a mesh with STATIC_DRAW is a slow operation";
			myWarnFlags |= USAGE;
		}

		// If we are not updating the whole thing, we just update a subsection from 0
		if (numVertices < myVertexCount) {
			// If we haven't raised the warning, raise it now and update the flags
			if (!(myWarnFlags & SLOW_STREAMING)) {
				FILE_LOG(logWARNING) << "Slow buffer update detected, for maximum speed keep mesh updates to same size";
				myWarnFlags |= SLOW_STREAMING;
			}

			// Bind the VBO and update a sub-buffer
			glBindBuffer(GL_ARRAY_BUFFER, myVbo);
			glBufferSubData(GL_ARRAY_BUFFER, 0, numVertices * myVertexSize, vertexData);
		}
		else if (numVertices == myVertexCount) {
			// Bind the VBO, orphan, then re-buffer
			glBindBuffer(GL_ARRAY_BUFFER, myVbo);
			glBufferData(GL_ARRAY_BUFFER, myVertexCount * myVertexSize, NULL, myUsage);
			glBufferData(GL_ARRAY_BUFFER, myVertexCount * myVertexSize, vertexData, myUsage);
		}
		else {
			// Show warning if required
			if (!(myWarnFlags & SLOW_GROW)) {
				FILE_LOG(logWARNING) << "Slow buffer update detected (required to grow buffer) Warning will not repeat for this buffer";
				myWarnFlags |= SLOW_GROW;
			}
			// Bind the VBO, orphan, then re-buffer
			glBindBuffer(GL_ARRAY_BUFFER, myVbo);
			glBufferData(GL_ARRAY_BUFFER, myVertexCount * myVertexSize, NULL, myUsage);
			glBufferData(GL_ARRAY_BUFFER, numVertices * myVertexSize, vertexData, myUsage);
			myVertexCount = numVertices;
		}

		// Same idea with indices, if we are not updating the entire buffer, we use SubBuffer
		if (numIndices < myIndexCount) {
			// Show warning if required
			if (!(myWarnFlags & SLOW_STREAMING)) {
				FILE_LOG(logWARNING) << "Slow buffer update detected, for maximum speed keep mesh updates to same size";
				myWarnFlags |= SLOW_STREAMING; 
			}

			// Bind buffer and update range
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myIbo);
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, numIndices * sizeof(short), indexData);
		}
		else if (numIndices == myIndexCount) {
			// Bind, orphan, update
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myIbo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, myIndexCount * sizeof(short), NULL, myUsage);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, myIndexCount * sizeof(short), indexData, myUsage);
		}
		else {
			// Show warning if required
			if (!(myWarnFlags & SLOW_GROW)) {
				FILE_LOG(logWARNING) << "Slow buffer update detected (required to grow buffer) Warning will not repeat for this buffer";
				myWarnFlags |= SLOW_GROW;
			}
			// Bind, orphan, update
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, myIbo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, myIndexCount * sizeof(short), NULL, myUsage);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(short), indexData, myUsage);
			myIndexCount = numIndices;
		}
		
		// Unbind the VAO
        glBindVertexArray(0);
    }
	
    // Implementation of mesh draw
    void Mesh::Draw() const {

        // Bind our VAO for use, we want to render from it
        glBindVertexArray(myVao);
        // Draw our elements in indexed mode
        glDrawElements(myPrimitiveMode, myIndexCount, GL_UNSIGNED_SHORT, nullptr);
    }

    // Implementation of mesh draw
    void Mesh::Draw(const int indexCount) const {

        // Bind our VAO for use, we want to render from it
        glBindVertexArray(myVao);
        // Draw our elements in indexed mode
        glDrawElements(myPrimitiveMode, indexCount, GL_UNSIGNED_SHORT, nullptr);
    }

}
