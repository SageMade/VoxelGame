/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/

#pragma once
#ifndef MESH_H
#define MESH_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <vector>

#include "glm_math.h"

#include "graphics/VertexDeclaration.h"
#include "MeshData.h"

namespace graphics { 
	
	class Model;

    /**
        Represents an indexed mesh that can be rendered. This is meant to be a single-material mesh part
    */
    class Mesh
    {
		friend class Model;
        public:
			// Stores any mesh offset for this mesh part
			glm::mat4 Transform;

            /**
                Create a new mesh
                @param data The vertex and index data to create the mesh from
                @param vDecl The layout of the vertex data in this mesh
            */
            Mesh(const MeshData& data, GLenum primitiveMode = GL_TRIANGLES, GLenum usage = GL_STATIC_DRAW); 
			//Mesh(const Mesh& other) = delete;
            /**
                Cleans up this mesh and it's buffers
            */
            virtual ~Mesh();
						
			void Update(const MeshData& mesh) { Update(mesh.Vertices, mesh.VertexCount, mesh.Indices, mesh.IndexCount); }
            void Update(void* vertexData, int numVertices, USHORT* indexDara, int numIndices);

			void Unload();
			
            /**
                Renders this mesh part
                @see https://www.khronos.org/opengl/wiki/Primitive#Provoking_vertex
            */
            void Draw() const;
            /**
                Renders this mesh part
                @see https://www.khronos.org/opengl/wiki/Primitive#Provoking_vertex
            */
            void Draw(const int indexCount) const;

        protected:
            // Stores the handles to the buffers
            GLuint myVbo, myIbo, myVao;
            // Store the vertex and index counts
            uint   myVertexCount, myIndexCount;

			// Stores the size of a single vertex
            uint myVertexSize;

			// Stores the usage type (ex: static_draw, stream_draw, etc...)
			GLenum myUsage;
			// Stores the primitive mode
			GLenum myPrimitiveMode;

			// Stores which warnings have been raised for this mesh
			uint8_t myWarnFlags;

			enum WarnFlags {
				/* Warning for slow streaming when using glBufferSubData */
				SLOW_STREAMING = 1,
				/* Warning for updating a buffer without STREAM_DRAW usage */
				USAGE          = 2,
				/* Warning for slow streaming when growing a buffer */
				SLOW_GROW      = 4,
			};

    };

} 

#endif // MESH_H
