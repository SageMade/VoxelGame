/*
Author: Shawn Matthews
Date:   Feb 7 2018
*/
#include <graphics/Texture1D.h>

#include <iostream>

#include <stb_image.h>

#include <Logger.h>

namespace graphics {

	Texture1DDefinition::Texture1DDefinition(
		const uint32_t width, 
		uint8_t * data, const GLenum filterMode,
		const GLenum internalPixelType,
		const GLenum pixelFormat,
		const GLenum pixelType,
		const GLenum wrapMode,
		const bool enableAnisotropy) :
		Width(width), Data(data), FilterMode(filterMode), InternalPixelType(internalPixelType),
		PixelFormat(pixelFormat), PixelType(pixelType), WrapMode(wrapMode), EnableAnisotropy(enableAnisotropy) {}

	Texture1DDefinition::Texture1DDefinition(const std::string & filename) : Texture1DDefinition() {
		int width{ 0 }, numChannels{ 0 };
		// Simple enough, just use the stbi function, the important notes are that we are forcing 4 bytes of pixel data
		int height;
		Data = stbi_load(filename.c_str(), &width, &height, &numChannels, 4);
		if (height != 1) {
			stbi_image_free(Data);
			throw new std::runtime_error("1D texture must be exactly 1 pixel tall");
		}
		Width = (uint32_t)width;
	}

	Texture1DDefinition::~Texture1DDefinition() {
		free(Data);
	}


    // Texture destructor
    Texture1D::~Texture1D()
    {
        // We want to delete our texture from the GPU
        glDeleteTextures(1, &myTextureHandle);
        FILE_LOG(logINFO)  << "Destroying texture: " << myTextureHandle;
    }

    // Implementation of bind
    void Texture1D::Bind(uint8_t textureSlot) {
        // Set the active slot to the given slot
        glActiveTexture(GL_TEXTURE0 + textureSlot);
        // Binds our texture
        glBindTexture(GL_TEXTURE_1D, myTextureHandle);  
    }


    // Implementation of load
    Texture1D::Texture1D(const char *fileName, GLenum filterMode, GLenum wrapMode, bool enableAnisotropy) {

        FILE_LOG(logINFO) << "Loading texture from " << fileName;

		double startTime = glfwGetTime();

        // Declare outputs for the stbi function
        int width{0}, height{0}, numChannels{0};
        // Simple enough, just use the stbi function, the important notes are that we are forcing 4 bytes of pixel data
        unsigned char *data = stbi_load(fileName, &width, &height, &numChannels, 4);

		if (height != 1) {
			stbi_image_free(data);
			throw new std::runtime_error("1D texture must be exactly 1 pixel tall");
		}

		double endTime = glfwGetTime();
		FILE_LOG(logDEBUG) << "Took " << (endTime - startTime) << "s to load data" << fileName;

		startTime = glfwGetTime();

        // Check to see if we got any data
        if (data) {
            // If we did, make a texture with it, using GL_RGBA and unsigned bytes
            __Create((uint32_t)width, data, filterMode, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, wrapMode, enableAnisotropy);
			stbi_image_free(data);
        }
        // Otherwise throw an exception
        else {
            throw std::runtime_error("Failed to load texture");
        }

		endTime = glfwGetTime();
		FILE_LOG(logDEBUG) << "Took " << (endTime - startTime) << "s to upload data" << fileName;

    }
	
    // Implement the create function
    Texture1D::Texture1D(const uint32_t width, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy)
    {

        __Create(width, data, filterMode, internalPixelType, pixelFormat, pixelType, wrapMode, enableAnisotropy);
    }

	// Implement the create function
	Texture1D::Texture1D(const Texture1DDefinition& definition)
	{
		__Create(definition.Width, definition.Data, definition.FilterMode, 
			definition.InternalPixelType, definition.PixelFormat, definition.PixelType, definition.WrapMode, definition.EnableAnisotropy);
	}

	void Texture1D::Unload() {
		glDeleteTextures(1, &myTextureHandle);
		myTextureHandle = 0;
		myWidth = 0;
	}

    // Actually creates the texture
    void Texture1D::__Create(const uint32_t width, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy) {

        myWidth = width;

        // Generate a texture for the reuslt
        glGenTextures(1, &myTextureHandle);

        // If we could not generate a texture, throw an exception
        if (myTextureHandle == 0)
            throw new std::runtime_error("Cannot create texture instance");

		glActiveTexture(GL_TEXTURE0);
        // Bind the texture for creation
        glBindTexture(GL_TEXTURE_1D, myTextureHandle);
		
        // We want to clamp our texture coords to within the texture
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, wrapMode);
		glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_T, wrapMode);
		

		GLenum magFilter = filterMode;

		switch (filterMode) {
			case GL_LINEAR_MIPMAP_LINEAR:
			case GL_LINEAR_MIPMAP_NEAREST:
				magFilter = GL_LINEAR;
				break;
			case GL_NEAREST_MIPMAP_LINEAR:
			case GL_NEAREST_MIPMAP_NEAREST:
				magFilter = GL_NEAREST;
				break;
		}

        // Set the filter mode
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, filterMode);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, magFilter);

        // Buffer our texture data
        glTexImage1D(GL_TEXTURE_1D, 0, internalPixelType, width, 0, pixelFormat, pixelType, data);

        FILE_LOG(logINFO)  << width << " @ " << myTextureHandle;
    }

	void Texture1D::Unbind(const GLenum location) {
		glActiveTexture(GL_TEXTURE0 + location);
		glBindTexture(GL_TEXTURE_1D, 0);
	}

}