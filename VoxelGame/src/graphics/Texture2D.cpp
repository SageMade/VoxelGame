/**
    Implementation file four our texture class, handles texturing loading and creation
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#include <graphics/Texture2D.h>

#include <iostream>

#include <stb_image.h>

#include <Logger.h>

namespace graphics {

	Texture2DDefinition::Texture2DDefinition(
		const uint32_t width, const uint32_t height,
		uint8_t * data, const GLenum filterMode,
		const GLenum internalPixelType,
		const GLenum pixelFormat,
		const GLenum pixelType,
		const GLenum wrapMode,
		const bool enableAnisotropy) :
		Width(width), Height(height), Data(data), FilterMode(filterMode), InternalPixelType(internalPixelType),
		PixelFormat(pixelFormat), PixelType(pixelType), WrapMode(wrapMode), EnableAnisotropy(enableAnisotropy) {
		memset(Name, 0, 16);
		memset(FileName, 0, 256);
	}

	Texture2DDefinition::Texture2DDefinition(const std::string & filename) : Texture2DDefinition() {
		int width{ 0 }, height{ 0 }, numChannels{ 0 };
		// Simple enough, just use the stbi function, the important notes are that we are forcing 4 bytes of pixel data
		Data = stbi_load(filename.c_str(), &width, &height, &numChannels, 4);
		Width = (uint32_t)width;
		Height = (uint32_t)height;
	}

	Texture2DDefinition::~Texture2DDefinition() {
		delete[] Data;
	}


    // Texture destructor
    Texture2D::~Texture2D()
    {
        // We want to delete our texture from the GPU
        glDeleteTextures(1, &myTextureHandle);
        FILE_LOG(logINFO)  << "Destroying texture: " << myTextureHandle;
    }

    // Implementation of bind
    void Texture2D::Bind(uint8_t textureSlot) {
        // Set the active slot to the given slot
        glActiveTexture(GL_TEXTURE0 + textureSlot);
        // Binds our texture
        glBindTexture(GL_TEXTURE_2D, myTextureHandle);  
    }


    // Implementation of load
    Texture2D::Texture2D(const char *fileName, GLenum filterMode, GLenum wrapMode, bool enableAnisotropy) {

        FILE_LOG(logINFO) << "Loading texture from " << fileName;

		double startTime = glfwGetTime();

        // Declare outputs for the stbi function
        int width{0}, height{0}, numChannels{0};
        // Simple enough, just use the stbi function, the important notes are that we are forcing 4 bytes of pixel data
        unsigned char *data = stbi_load(fileName, &width, &height, &numChannels, 4);

		double endTime = glfwGetTime();
		FILE_LOG(logDEBUG) << "Took " << (endTime - startTime) << "s to load data" << fileName;

		startTime = glfwGetTime();

        // Check to see if we got any data
        if (data) {
            // If we did, make a texture with it, using GL_RGBA and unsigned bytes
            __Create((uint32_t)width, (uint32_t)height, data, filterMode, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, wrapMode, enableAnisotropy);
			delete data;
        }
        // Otherwise throw an exception
        else {
            throw std::runtime_error("Failed to load texture");
        }

		endTime = glfwGetTime();
		FILE_LOG(logDEBUG) << "Took " << (endTime - startTime) << "s to upload data" << fileName;

    }
	
    // Implement the create function
    Texture2D::Texture2D(const uint32_t width, const uint32_t height, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy)
    {

        __Create(width, height, data, filterMode, internalPixelType, pixelFormat, pixelType, wrapMode, enableAnisotropy);
    }

	// Implement the create function
	Texture2D::Texture2D(const Texture2DDefinition& definition)
	{
		__Create(definition.Width, definition.Height, definition.Data, definition.FilterMode, 
			definition.InternalPixelType, definition.PixelFormat, definition.PixelType, definition.WrapMode, definition.EnableAnisotropy);
	}

	void Texture2D::Unload() {
		glDeleteTextures(1, &myTextureHandle);
		myTextureHandle = 0;
		myWidth = 0;
		myHeight = 0;
	}

    // Actually creates the texture
    void Texture2D::__Create(const uint32_t width, const uint32_t height, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy) {

        myWidth = width;
        myHeight = height;

        // Generate a texture for the reuslt
        glGenTextures(1, &myTextureHandle);

        // If we could not generate a texture, throw an exception
        if (myTextureHandle == 0)
            throw new std::runtime_error("Cannot create texture instance");

		glActiveTexture(GL_TEXTURE0);
        // Bind the texture for creation
        glBindTexture(GL_TEXTURE_2D, myTextureHandle);

        // Buffer our texture data
        glTexImage2D(GL_TEXTURE_2D, 0, internalPixelType, width, height, 0, pixelFormat, pixelType, data);

        // Generate mipmaps for LOD stuff and whatnots
        glGenerateMipmap(GL_TEXTURE_2D);

        if (enableAnisotropy) {
            float maxAnisotropy;
            glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
        }

        // We want to clamp our texture coords to within the texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

        // Set a border color to magenta
        float borderColor[] = { 1.0f, 0.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

		GLenum magFilter = filterMode;

		switch (filterMode) {
			case GL_LINEAR_MIPMAP_LINEAR:
			case GL_LINEAR_MIPMAP_NEAREST:
				magFilter = GL_LINEAR;
				break;
			case GL_NEAREST_MIPMAP_LINEAR:
			case GL_NEAREST_MIPMAP_NEAREST:
				magFilter = GL_NEAREST;
				break;
		}

        // Set the filter mode
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMode);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);

        FILE_LOG(logINFO)  << width << " x " << height << " @ " << myTextureHandle;
    }

	void Texture2D::Unbind(const GLenum location) {
		glActiveTexture(GL_TEXTURE0 + location);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
}