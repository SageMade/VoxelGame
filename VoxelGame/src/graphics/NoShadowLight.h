#pragma once

#include "Tweakables.h"

#include "glm_math.h"

#include "graphics/Shader.h"
#include "graphics/Camera.h"
#include "graphics/Mesh.h"

namespace graphics {

	enum LightVolumeType : int {
		Unknown       = 0x00,
	    SphereVolume  = 0x01,
		ConeVolume    = 0x02,
		CubeVolume    = 0x03
	};

	class NoShadowLight {
	public:
		NoShadowLight() : 
			Position(glm::vec3()), 
			Color(glm::vec3(1.0f)), 
			Radius(1.0f),
			Angle(45.0f),
			Type(SphereVolume),
			Volume(nullptr),
			EulerRotation(glm::vec3(0.0f))
		{
		};
		NoShadowLight(
			glm::vec3 position,
			glm::vec3 color,
			float radius  = 1.0f
		) : 
			Position(position), 
			Color(color),
			Radius(radius),
			Angle(45.0f),
			Type(SphereVolume),
			Volume(nullptr),
			EulerRotation(glm::vec3(0.0f)) {
		};
		NoShadowLight(

			LightVolumeType type,

			glm::vec3 position,

			glm::vec3 color,

			float radius,

			float angle,

			glm::vec3 euler_rot

		) :

			Position(position),

			Color(color),

			Radius(radius),

			Angle(angle),

			Type(type),

			Volume(nullptr),

			EulerRotation(euler_rot) {

		};

		glm::vec3  Position;
		glm::vec3  Color;
		float      Radius;

		glm::vec3  EulerRotation;
		float      Angle;

		LightVolumeType Type;

		Mesh*      Volume;
		
		void Render(Shader * shader, const Camera& camera) const;
		
	};

}