/*
	Author: Shawn Matthews
	Date:   Feb 7 2018
*/

#include "RenderTarget.h"

#include "Logger.h"
#include "CommonTools.h"

#include <stdexcept>

namespace graphics {

	RenderTarget::RenderTarget(uint32_t width, uint32_t height) : myDepthBuffer(GL_DEPTH_STENCIL_ATTACHMENT) {
		myWidth = width;
		myHeight = height;
		glGenFramebuffers(1, &myHandle);
		myBufferCount = 0;
		myBuffers = nullptr;
		hasDepthBuffer = false;
		isFinalized = false;
		ownsDepth = true;
	}

	RenderTarget::~RenderTarget(){
		glDeleteRenderbuffers(1, &myHandle);
		myHandle = 0;
	}

	void RenderTarget::Resize(uint32_t width, uint32_t height) {
		myWidth = width;
		myHeight = height;

		Bind();

		GLint prevID;
		glGetIntegerv(GL_TEXTURE_BINDING_2D, &prevID);

		if (hasDepthBuffer) {
			glBindTexture(GL_TEXTURE_2D, myDepthBuffer.TextureHandle);
			glTexImage2D(GL_TEXTURE_2D, 0, myDepthBuffer.InternalFormat, width, height, 0, myDepthBuffer.PixelFormat, myDepthBuffer.PixelType, nullptr);
			glFramebufferTexture(GL_FRAMEBUFFER, myDepthBuffer.BindingTarget, myDepthBuffer.TextureHandle, myDepthBuffer.MipLevel);
		}

		for (uint32_t ix = 0; ix < myBufferCount; ix++) {
			const BufferInfo& info = myBuffers[ix];
			glBindTexture(GL_TEXTURE_2D, info.TextureHandle);
			glTexImage2D(GL_TEXTURE_2D, 0, info.InternalFormat, width, height, 0, info.PixelFormat, info.PixelType, nullptr);
			glFramebufferTexture(GL_FRAMEBUFFER, info.BindingTarget, info.TextureHandle, info.MipLevel);
		}

		glBindTexture(GL_TEXTURE_2D, prevID);

		UnBind();
	}

	void RenderTarget::Bind() const {
		if (!isFinalized)
			throw std::runtime_error("Rendertarget must be finalized before binding");

		__Bind();
		glViewport(0, 0, myWidth, myHeight);
	}

	void RenderTarget::UnBind() {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void RenderTarget::Clear() const {
		Bind();
		for (uint32_t ix = 0; ix < myBufferCount; ix++) {
			if (myBuffers[ix].ClearValue) {
				switch (myBuffers[ix].PixelType) {
					case GL_BYTE:
					case GL_SHORT:
					case GL_INT:
						glClearBufferiv(GL_COLOR, ix, (int32_t*)myBuffers[ix].ClearValue);
						break;
					case GL_FLOAT:
						glClearBufferfv(GL_COLOR, ix, (float*)myBuffers[ix].ClearValue);
						break;
					case GL_UNSIGNED_BYTE:
					case GL_UNSIGNED_BYTE_3_3_2:
					case GL_UNSIGNED_BYTE_2_3_3_REV:
					case GL_UNSIGNED_SHORT:
					case GL_UNSIGNED_SHORT_5_6_5:
					case GL_UNSIGNED_SHORT_5_6_5_REV:
					case GL_UNSIGNED_SHORT_4_4_4_4:
					case GL_UNSIGNED_SHORT_4_4_4_4_REV:
					case GL_UNSIGNED_SHORT_5_5_5_1:
					case GL_UNSIGNED_SHORT_1_5_5_5_REV:
					case GL_UNSIGNED_INT:
					case GL_UNSIGNED_INT_8_8_8_8:
					case GL_UNSIGNED_INT_8_8_8_8_REV:
					case GL_UNSIGNED_INT_10_10_10_2:
					case GL_UNSIGNED_INT_2_10_10_10_REV:
					case GL_UNSIGNED_INT_24_8:
					case GL_UNSIGNED_INT_10F_11F_11F_REV:
					case GL_UNSIGNED_INT_5_9_9_9_REV:
						glClearBufferuiv(GL_COLOR, ix, (uint32_t*)myBuffers[ix].ClearValue);
						break;
					default:
						throw std::invalid_argument("Cannot determine correct clear mode for type");
						break;
				}
			}
			else {
				switch (myBuffers[ix].PixelType) {
				case GL_BYTE:
				case GL_SHORT:
				case GL_INT: {
					static int clearInts[] = { 0, 0, 0, 0 };
					glClearBufferiv(GL_COLOR, ix, clearInts);
					break;
				}
				case GL_FLOAT: {
					static float clearFloats[] = { 0.0f, 0.0f, 0.0f, 0.0f };
					glClearBufferfv(GL_COLOR, ix, clearFloats);
					break;
				}
				case GL_UNSIGNED_BYTE:
				case GL_UNSIGNED_BYTE_3_3_2:
				case GL_UNSIGNED_BYTE_2_3_3_REV:
				case GL_UNSIGNED_SHORT:
				case GL_UNSIGNED_SHORT_5_6_5:
				case GL_UNSIGNED_SHORT_5_6_5_REV:
				case GL_UNSIGNED_SHORT_4_4_4_4:
				case GL_UNSIGNED_SHORT_4_4_4_4_REV:
				case GL_UNSIGNED_SHORT_5_5_5_1:
				case GL_UNSIGNED_SHORT_1_5_5_5_REV:
				case GL_UNSIGNED_INT:
				case GL_UNSIGNED_INT_8_8_8_8:
				case GL_UNSIGNED_INT_8_8_8_8_REV:
				case GL_UNSIGNED_INT_10_10_10_2:
				case GL_UNSIGNED_INT_2_10_10_10_REV:
				case GL_UNSIGNED_INT_24_8:
				case GL_UNSIGNED_INT_10F_11F_11F_REV:
				case GL_UNSIGNED_INT_5_9_9_9_REV: {
					static uint32_t clearUints[] = { 0U, 0U, 0U, 0U };
					glClearBufferuiv(GL_COLOR, ix, clearUints);
					break;
				}
				default:
					throw std::invalid_argument("Cannot determine correct clear mode for type");
					break;
				}
			}
		}

		if (hasDepthBuffer) {
			if (myDepthBuffer.ClearValue)
				glClearBufferfv(GL_DEPTH, 0, (float*)myDepthBuffer.ClearValue);
			else {
				static float clearDepth[] = { 0.0f };
				glClearBufferfv(GL_DEPTH, 0, clearDepth);
			}
		}
	}

	void RenderTarget::BlitDepthToBackBuffer(const uint32_t width, const uint32_t height) const {
		if (hasDepthBuffer) {
			glBindFramebuffer(GL_READ_FRAMEBUFFER, myHandle);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glBlitFramebuffer(0, 0, myWidth, myHeight, 0, 0, width, height, GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		}
	}

	void RenderTarget::BindTexture(GLenum bindLoc, uint8_t location) const {
		if (!isFinalized)
			throw std::runtime_error("Rendertarget must be finalized before binding");

		for (uint32_t ix = 0; ix < myBufferCount; ix++) {
			if (myBuffers[ix].BindingTarget == bindLoc) {
				glActiveTexture(GL_TEXTURE0 + location);
				glBindTexture(GL_TEXTURE_2D, myBuffers[ix].TextureHandle);
			}
		}
	}

	void RenderTarget::BindTextureByID(uint8_t id, uint8_t location) const {
		if (!isFinalized)
			throw std::runtime_error("RenderTarget must be finalized before binding");

		if (id < myBufferCount) {
			glActiveTexture(GL_TEXTURE0 + location);
			glBindTexture(GL_TEXTURE_2D, myBuffers[id].TextureHandle);
		}
			
	}

	void RenderTarget::BindDepthTex(uint8_t location) const {
		if (!isFinalized)
			throw std::runtime_error("RenderTarget must be finalized before binding");

		if (hasDepthBuffer) {
			glActiveTexture(GL_TEXTURE0 + location);
			glBindTexture(GL_TEXTURE_2D, myDepthBuffer.TextureHandle);
		}
	}

	void RenderTarget::AddDepthStencilBuffer(bool isPCFEnabled) {
		if (isFinalized)
			throw std::runtime_error("Cannot modify RenderTarget after finalizing");

		AddBuffer(BufferInfo(GL_DEPTH_STENCIL_ATTACHMENT, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, 0, nullptr, isPCFEnabled));
	}

	void RenderTarget::AddDepthBuffer(bool isPCFEnabled) {
		if (isFinalized)
			throw std::runtime_error("Cannot modify RenderTarget after finalizing");

		AddBuffer(BufferInfo(GL_DEPTH_ATTACHMENT, GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, 0, nullptr, isPCFEnabled));
	}

	void RenderTarget::AddBuffer(const BufferInfo & buffer)
	{
		if (isFinalized)
			throw std::runtime_error("Cannot modify RenderTarget after finalizing");

		if (buffer.BindingTarget == GL_DEPTH_STENCIL_ATTACHMENT || buffer.BindingTarget == GL_DEPTH_ATTACHMENT) {
			if (hasDepthBuffer)
				throw std::runtime_error("Cannot bind 2 depth buffers!");
		}

		for(uint32_t ix = 0; ix < myBufferCount; ix++)
			if (myBuffers[ix].BindingTarget == buffer.BindingTarget)
				throw std::runtime_error("Attempting to bind 2 buffers to the same target");

		if (!IsPixelType(buffer.PixelType))
			throw std::runtime_error("Buffer does not have a valid pixel type");
		if (!IsPixelFormat(buffer.PixelFormat))
			throw std::runtime_error("Buffer does not have a valid pixel format");

		__Bind();

		GLuint texID{ 0 };
		glGenTextures(1, &texID);
		glBindTexture(GL_TEXTURE_2D, texID);
		glTexImage2D(GL_TEXTURE_2D, 0, buffer.InternalFormat, myWidth, myHeight, 0, buffer.PixelFormat, buffer.PixelType, nullptr);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, buffer.MagFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, buffer.MinFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, buffer.WrapMode);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, buffer.WrapMode);

		glFramebufferTexture(GL_FRAMEBUFFER, buffer.BindingTarget, texID, buffer.MipLevel);

		if (buffer.BindingTarget == GL_DEPTH_STENCIL_ATTACHMENT || buffer.BindingTarget == GL_DEPTH_ATTACHMENT) {
			myDepthBuffer = BufferInfo(buffer);
			myDepthBuffer.TextureHandle = texID;
			hasDepthBuffer = true;
			ownsDepth = true;
			
			if (buffer.PCFEnabled) {
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
				glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
			}
		}
		else {
			BufferInfo *newList = (BufferInfo*)malloc(sizeof(BufferInfo) * (myBufferCount + 1));
			if (myBufferCount > 0)
				memcpy(newList, myBuffers, myBufferCount * sizeof(BufferInfo));
			newList[myBufferCount] = BufferInfo(buffer);
			newList[myBufferCount].TextureHandle = texID;
			if (myBuffers)
				free(myBuffers);

			myBuffers = newList;
			myBufferCount++;
		}

		UnBind();

		FILE_LOG(logINFO) << "Added RT to FBO";
	}

	void RenderTarget::Finalize() {
		if (!isFinalized) {
			GLenum *drawBuffers = new GLenum[myBufferCount];

			__Bind();
			for (uint32_t ix = 0, i = 0; ix < myBufferCount; ix++) {
				drawBuffers[ix] = myBuffers[ix].BindingTarget;
			}

			glDrawBuffers(myBufferCount, drawBuffers);
			delete[] drawBuffers;

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
				FILE_LOG(logERROR) << "Failed to build rendertarget: " << glCheckFramebufferStatus(GL_FRAMEBUFFER);
				throw std::runtime_error("Failed to build render target");
			}
			else
				FILE_LOG(logINFO) << "Built framebuffer!";

			UnBind();

			isFinalized = true;
		}
	}

	void RenderTarget::UseSharedDepthWith(RenderTarget* source) {
		if (source == nullptr)
			throw std::exception("Source buffer is null");

		myDepthBuffer = source->myDepthBuffer;

		__Bind();

		glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, myDepthBuffer.TextureHandle, myDepthBuffer.MipLevel);

		UnBind();

		hasDepthBuffer = true;
		ownsDepth = false;
	}

	void RenderTarget::__Bind() const {
		glBindFramebuffer(GL_FRAMEBUFFER, myHandle);
	}

}
