/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#include "glm_math.h"
glm::vec3 mult(const glm::vec3 & left, const glm::mat4 & right)
{
	return glm::vec3(
		left.x * right[0][0] + left.y * right[1][0] + left.z * right[2][0] + right[3][0],
	    left.x * right[0][1] + left.y * right[1][1] + left.z * right[2][1] + right[3][1],
		left.x * right[0][2] + left.y * right[1][2] + left.z * right[2][2] + right[3][2]);
}

glm::vec2 mult(const glm::vec2 & left, const glm::mat3 & right)
{
	return glm::vec3(
		left.x * right[0][0] + left.y * right[1][0] + right[2][0],
		left.x * right[0][1] + left.y * right[1][1] + right[2][1],
		left.x * right[0][2] + left.y * right[1][2] + right[2][2]);
}

float randf(float min, float max)
{
	return min + (rand() / (float)RAND_MAX) * (max - min);
}
