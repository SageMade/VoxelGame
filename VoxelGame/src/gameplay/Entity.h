/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include "glm_math.h"

namespace gameplay {

	class Entity {
	public:
		Entity();
		virtual ~Entity() {}

		virtual void Update();
		virtual void Render();

	protected:
		glm::vec3 myPosition;
		glm::vec3 myYawPitchRoll;
	};

}