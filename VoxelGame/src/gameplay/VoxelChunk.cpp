/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#include "VoxelChunk.h"
#include "ServiceProvider.h"

namespace gameplay {

	VoxelChunk::VoxelChunk() : myOpaqueMesh(nullptr) {
		memset(myData, 0, sizeof(Voxel) * CHUNK_VOLUME);
	}

	VoxelChunk::~VoxelChunk() {

	}
	
	VoxelFaceData VoxelChunk::GetFaceData(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal, VoxelFace face) const {
		VoxelFaceData result;
		result.Face = face;
		result.Type = __GetVoxel(xLocal, yLocal, zLocal).BlockID;
		glm::ivec3 normal = FACE_NORMALS[face];
		result.IsVisible = true;// result.Type != 0 && __GetVoxel(xLocal + normal.x, yLocal + normal.y, zLocal + normal.z).BlockID == 0;
		return result;
	}

	void VoxelChunk::SetLocal(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal, const Voxel value) {
		__GetVoxel(xLocal, yLocal, zLocal) = value;
		isDirty = true;
	}

	void VoxelChunk::Update() {
		// TODO: update the blocks in this chunk
	}

	void VoxelChunk::Render() {
		if (myOpaqueMesh) {
			myOpaqueMesh->Draw();   
		}
	}

	void VoxelChunk::UpdateMesh(const graphics::MeshData & mesh) {
		if (myOpaqueMesh == nullptr)
			myOpaqueMesh = new graphics::Mesh(mesh);
		else
			myOpaqueMesh->Update(mesh.Vertices, mesh.VertexCount, mesh.Indices, mesh.IndexCount);
	}
	
}
