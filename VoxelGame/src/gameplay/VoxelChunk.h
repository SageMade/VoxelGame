/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include "BlockRegistry.h"
#include "Voxel.h"
#include "glm_math.h"
#include "graphics/Mesh.h"

#include "graphics/VertexLayouts.h";

#define CHUNK_SIZE 32
#define CHUNK_LAYERSIZE CHUNK_SIZE * CHUNK_SIZE
#define CHUNK_VOLUME CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE

#define CHUNK_EXTENTS glm::ivec3(CHUNK_SIZE, CHUNK_SIZE, CHUNK_SIZE)

#define BLOCK_SIZE 1.0f

namespace gameplay {

	class VoxelChunk {
	public:
		VoxelChunk();
		~VoxelChunk();

		Voxel GetLocal(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal) const {
			return __GetVoxel(xLocal, yLocal, zLocal);
		}
		Voxel GetLocal(glm::ivec3 pos) const {
			return __GetVoxel(pos.x, pos.y, pos.z);
		}

		VoxelFaceData GetFaceData(glm::ivec3 pos, VoxelFace face) const {
			return GetFaceData(pos.x, pos.y, pos.z, face);
		}
		VoxelFaceData GetFaceData(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal, VoxelFace face) const;

		void SetLocal(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal, const Voxel value);

		inline uint8_t GetFace(const glm::ivec3& pos, VoxelFace face) const {
			return BlockRegistry::GetBlockFaceId(GetLocalID(pos.x, pos.y, pos.z), face);
		}

		inline bool IsVisible(glm::ivec3 pos, VoxelFace face) const {
			glm::ivec3 other = pos - FACE_NORMALS[face];
			uint8_t type = !IsInChunk(other) ? 0 : GetLocalID(other.x, other.y, other.z);
			return type == 0 || false;
			/*
			return (other.x < 0 | other.x >= CHUNK_SIZE | other.y < 0 | other.y >= CHUNK_SIZE | other.z < 0 | other.z >= CHUNK_SIZE) ?
				GetLocalID(pos.x, pos.y, pos.z) : (GetLocalID(pos.x, pos.y, pos.z) != GetLocalID(other.x, other.y, other.z)) & (GetLocalID(pos.x, pos.y, pos.z) != 0);
			*/
		}

		inline bool IsInChunk(const glm::ivec3& localPos) const {
			return !(localPos.x < 0 | localPos.x >= CHUNK_SIZE | localPos.y < 0 | localPos.y >= CHUNK_SIZE | localPos.z < 0 | localPos.z >= CHUNK_SIZE);
		}

		inline uint8_t GetLocalID(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal) const {
			return  myData[(xLocal % CHUNK_SIZE) * CHUNK_LAYERSIZE + (yLocal % CHUNK_SIZE) * CHUNK_SIZE + zLocal].BlockID;
		}
		inline uint8_t GetLocalID(const glm::ivec3& local) const {
			return  myData[(local.x % CHUNK_SIZE) * CHUNK_LAYERSIZE + (local.y % CHUNK_SIZE) * CHUNK_SIZE + local.z].BlockID;
		}
		inline uint8_t GetLocalMeta(uint32_t xLocal, uint32_t yLocal, uint32_t zLocal) {
			return  myData[(xLocal % CHUNK_SIZE) * CHUNK_LAYERSIZE + (yLocal % CHUNK_SIZE) * CHUNK_SIZE + zLocal].MetaData;
		}

		void Update();
		void Render();

		void UpdateMesh(const graphics::MeshData& mesh);

	private:
		bool isDirty;
		Voxel myData[CHUNK_VOLUME];

		graphics::Mesh *myOpaqueMesh;

		inline Voxel& __GetVoxel(uint32_t xLoc, uint32_t yLoc, uint32_t zLoc) {
			return myData[(xLoc % CHUNK_SIZE) * CHUNK_LAYERSIZE + (yLoc % CHUNK_SIZE) * CHUNK_SIZE + zLoc];
		}
		inline Voxel __GetVoxel(uint32_t xLoc, uint32_t yLoc, uint32_t zLoc) const {
			return myData[(xLoc % CHUNK_SIZE) * CHUNK_LAYERSIZE + (yLoc % CHUNK_SIZE) * CHUNK_SIZE + zLoc];
		}
	};

}