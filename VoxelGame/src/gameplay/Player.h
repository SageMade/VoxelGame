/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include "Entity.h"
#include "Camera.h"

namespace gameplay {

	class Player : public Entity {
	public:
		Player();
		~Player();

		void Update();
		void Render();

	private:
		Camera * myCamera;
	};

}