#include "BlockRegistry.h"

uint8_t gameplay::BlockRegistry::GetBlockFaceId(uint8_t blockID, VoxelFace face) {
	face = Invert(face);
	switch (blockID)
	{
	case 1: // grass
		if (face == TOP)
			return 0;
		else if (face == BOTTOM)
			return 2;
		else
			return 3;
	case 2: // dirt
			return 2;
	case 3: // stone
		return 1;
	default:
		return 0;
		break;
	}
}
