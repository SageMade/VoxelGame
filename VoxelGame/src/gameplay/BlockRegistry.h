/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/
#pragma once

#include <cstdint>
#include "Voxel.h"

namespace gameplay {

	class BlockRegistry {
	public:
		static uint8_t GetBlockFaceId(uint8_t blockID, VoxelFace face);
	private:
	};

}