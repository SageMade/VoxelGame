/*
	Author: Shawn Matthews
	Date: July 20th, 2017
	Description:
		Core low-level input and input state management
*/
#include "InputManager.h"

#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Logger.h"

#if DEBUG
#include "iostream"
#endif

namespace gameplay { namespace input {

	// Declare static data members
	const   Window* InputManager::myWindow;
	void*   InputManager::myUserParam;
	int     InputManager::myMouseMode;
	bool    InputManager::isGamePadEnabled = true;

	glm::dvec2 InputManager::myPrevMousePos, InputManager::myMousePos, InputManager::myMouseDelta;

	InputManager::JoystickData** InputManager::myJoystickData;
	ButtonState*                 InputManager::myKeyboardState;
	ButtonState*                 InputManager::myMouseState;


    // Initialize all the static event handler vectors
    std::vector<std::function<void(void*, glm::vec2, glm::vec2)>>       InputManager::myMouseMoveCallbacks;
    std::vector<std::function<void(void*, int, int, ButtonState, int)>> InputManager::myKeyEventCallbacks;
    std::vector<std::function<void(void*, uint, int)>>                  InputManager::myTextEventCallbacks;
    std::vector<std::function<void(void*, int, ButtonState, int)>>      InputManager::myMouseEventCallbacks;
    std::vector<std::function<void(void*, double, double)>>             InputManager::myMouseScrollCallbacks;
    std::vector<std::function<void(void*, int, int)>>                   InputManager::myJoystickConnectionCallbacks;
    std::vector<std::function<void(void*, int, int, ButtonState)>>      InputManager::myJoystickButtonCallbacks;

	// Raw callbacks
	std::vector<GLFWmousebuttonfun> InputManager::myMouseEventRawCallbacks;

	// Handles initializing the input manager
    void InputManager::Init() {
		// Logging is great!
        FILE_LOG(logINFO) << "Initializing input subsystem...";

		// Allocate some memory
        InputManager::myJoystickData  = new JoystickData*[GLFW_JOYSTICK_LAST + 1];
		InputManager::myKeyboardState = new ButtonState[GLFW_KEY_LAST + 1 - GLFW_KEY_FIRST];
		InputManager::myMouseState    = new ButtonState[GLFW_MOUSE_BUTTON_LAST + 1];
		
		// Iterate over all 
        for(uint ix = 0; ix < GLFW_JOYSTICK_LAST; ix ++) {
			// See if the joystick is present before grabbing or nullifying
            if (glfwJoystickPresent(ix)) {
				// Create the new joystick data and grab the data from glfw
                InputManager::myJoystickData[ix] = new JoystickData();
                InputManager::myJoystickData[ix]->JoystickId   = ix;
                InputManager::myJoystickData[ix]->AxisData     = glfwGetJoystickAxes(ix, &InputManager::myJoystickData[ix]->AxisCount);
                InputManager::myJoystickData[ix]->ButtonData   = glfwGetJoystickButtons(ix, &InputManager::myJoystickData[ix]->ButtonCount);
                InputManager::myJoystickData[ix]->ButtonStates = new ButtonState[InputManager::myJoystickData[ix]->ButtonCount]();
                InputManager::myJoystickData[ix]->Name         = glfwGetJoystickName(ix);
                InputManager::myJoystickData[ix]->IsConnected  = true;

				// Yay! More logging!
                FILE_LOG(logINFO) << "Detected controller @ " << ix << ": " << InputManager::myJoystickData[ix]->Name ;
            } 
			// Not connected, just nullify the joystick data
			else {
				// Create the data
                myJoystickData[ix] = new JoystickData();

				// Nullify the data
                InputManager::myJoystickData[ix]->JoystickId  = ix;
                InputManager::myJoystickData[ix]->AxisData    = NULL;
                InputManager::myJoystickData[ix]->ButtonData  = NULL;
                InputManager::myJoystickData[ix]->Name        = NULL;
                InputManager::myJoystickData[ix]->AxisCount   = 0;
                InputManager::myJoystickData[ix]->ButtonCount = 0;
                InputManager::myJoystickData[ix]->IsConnected = false;
            }
        }

		// Log that we're done
        FILE_LOG(logINFO) << "Done";
    }

	// Set the window handle
    void InputManager::SetWindow(const Window* window) {
		// Make sure we don't try to re-bind
        if (InputManager::myWindow != NULL)
            throw std::runtime_error("Cannot change window for input after setting initially");

		// Store the pointer
        InputManager::myWindow = window;

		// Initialize some stuff
		glfwGetCursorPos(myWindow->GetHandle(), &myMousePos.x, &myMousePos.y);
		myPrevMousePos = myMousePos;
		myMouseDelta = glm::vec2(0.0);

		// Tie the all our glfw callbacks
        glfwSetKeyCallback(window->GetHandle(), InputManager::__handleGlfwKeyEvent);
        glfwSetMouseButtonCallback(window->GetHandle(), InputManager::__handleGlfwMouseButton);
        glfwSetCursorPosCallback(window->GetHandle(), InputManager::__handleGlfwMouseMove);
        glfwSetScrollCallback(window->GetHandle(), InputManager::__handleGlfwMouseScroll);
        glfwSetCharModsCallback(window->GetHandle(), InputManager::__handleGlfwTextEvent);
        glfwSetJoystickCallback(InputManager::__handleGlfwJoystickChanged);
    }

	// Gets the joystick axis data
    const float InputManager::GetJoystickAxis(const int joystick, const int axis) {
		// Only return if connected
        if (myJoystickData[joystick]->IsConnected)
            return myJoystickData[joystick]->AxisData[axis];
        else
            return 0;
    }

	// Gets the state of a joystick button
    const ButtonState InputManager::GetJoystickButton(const int joystick, const int button) {
		// Only return from state if connected
        if (myJoystickData[joystick]->IsConnected)
            return myJoystickData[joystick]->ButtonStates[button];
        else
            return ButtonReleased;
    }

	// Gets the name of a joystick
    const char* InputManager::GetJoystickName(const int joystick) {
		// Only call if connected
        if (myJoystickData[joystick]->IsConnected)
            return myJoystickData[joystick]->Name;
        else
            return nullptr;
    }

	// Pass-though for getting the joystick connection state
    const bool  InputManager::GetJoystickConnected(const int joystick) {
        return myJoystickData[joystick]->IsConnected;
    }

	// Pass-though for getting the name associated with the keyboard key
	const char * InputManager::GetKeyboardKeyName(const int key) {
		return glfwGetKeyName(key, GLFW_KEY_UNKNOWN);
	}

	// Pass-though for getting the keyboard state
	const ButtonState InputManager::GetKeyState(const int key) {
		return InputManager::myKeyboardState[key - GLFW_KEY_FIRST];
	}

	// Pass-through for the cursor position setting
	void InputManager::SetMousePos(double xPos, double yPos) {
		glfwSetCursorPos(myWindow->GetHandle(), xPos, yPos);
	}

	// Sets the cursor mode
	void InputManager::SetCursorMode(int value) {
		// Basically just call the GLFW method
		myMouseMode = value;
		glfwSetInputMode(myWindow->GetHandle(), GLFW_CURSOR, value); 
	}

	void InputManager::NotifyFrame() {
		myMouseDelta = glm::vec2(0.0f);//myMousePos - myPrevMousePos;
	}

	// Polls for input events from the window
    void InputManager::Poll() {
		// Every time we poll we reset the mouse delta
		
		// We update the button state for every key
		for (int ix = GLFW_KEY_FIRST; ix < GLFW_KEY_LAST; ix++) {
			// Grab the glfw state
			int state = glfwGetKey(myWindow->GetHandle(), ix);
			// Determine the previous button state
			ButtonState prevState = myKeyboardState[ix - GLFW_KEY_FIRST];
			// Determine the new button state based off the previous state and the new glfw state
			InputManager::myKeyboardState[ix - GLFW_KEY_FIRST] = (state == GLFW_PRESS ? (prevState == ButtonPressed || prevState == ButtonHeld ? ButtonHeld : ButtonPressed) : ButtonReleased);
		}

		// We also update every mouse button state
		for (int ix = 0; ix <= GLFW_MOUSE_BUTTON_LAST; ix++) {
			// Get the glfw state
			int state = glfwGetMouseButton(myWindow->GetHandle(), ix);
			// Determine the new button state based off the previous state and the new glfw state
			InputManager::myMouseState[ix] = (state == GLFW_PRESS ? (myMouseState[ix] == ButtonPressed || myMouseState[ix] == ButtonHeld ? ButtonHeld : ButtonPressed) : ButtonReleased);
		}

		// Only update joysticks if gamepads are enabled
		if (isGamePadEnabled) {
			// We also need to update any connected controllers / joysticks
			for (int ix = 0; ix < GLFW_JOYSTICK_LAST; ix++) {
				// Only update if it is connected
				if (InputManager::myJoystickData[ix]->IsConnected) {
					int garbo;
					// Grab the joystick instance
					JoystickData* joystick = InputManager::myJoystickData[ix];

					// Refresh the axis and button data
					joystick->AxisData = glfwGetJoystickAxes(ix, &garbo);
					joystick->ButtonData = glfwGetJoystickButtons(ix, &garbo);

					// Fucking button states...
					for (int bx = 0; bx < joystick->ButtonCount; bx++) {
						/*
							This one is a bit tight, basically evaluates to:
								Is the button pressed?
									Was the last state released?
										Then it has been pressed
									Else
										Then it is being held
								Else
									Then it is released
						*/
						ButtonState newState = joystick->ButtonData[bx] == 1 ?
							((joystick->ButtonStates[bx] == ButtonReleased) ? ButtonPressed : ButtonHeld) :
							ButtonReleased;
						// We only need to update state and invoke events on a change
						if (newState != joystick->ButtonStates[bx]) {
							// Update the field
							joystick->ButtonStates[bx] = newState;

							// Invoke the event handlers. We COULD have these per-button, but maybe not since controllers have have different numbers of buttons
							// and can be connected/disconnected
							for (uint ix = 0; ix < InputManager::myJoystickButtonCallbacks.size(); ix++) {
								InputManager::myJoystickButtonCallbacks[ix](InputManager::myUserParam, ix, bx, newState);
							}
						}
					}
				}
			}
        }
    }

	// Clean up any memory we had to allocate
    void InputManager::Cleanup() {
        delete InputManager::myJoystickData;
		delete InputManager::myKeyboardState;
		delete InputManager::myMouseState;
    }

    #ifdef DEBUG
	// Dump the joystick data to the console
    void InputManager::DumpJoystick(const int joystick) {
		// Write the name
        std::cout << InputManager::myJoystickData[joystick]->Name << " ";

		// Write each axis data
        for(uint ix = 0; ix < InputManager::myJoystickData[joystick]->AxisCount; ix++) {
            std::cout << InputManager::myJoystickData[joystick]->AxisData[ix] << " ";
        }
        std::cout << " | ";
		// Write all the button states
        for(uint ix = 0; ix < InputManager::myJoystickData[joystick]->ButtonCount; ix++) {
            std::cout << (int)InputManager::myJoystickData[joystick]->ButtonData[ix] << " ";
        }
		// Throw an end-line on there
        std::cout << std::endl;
    }
    #endif // DEBUG

	// Handles the GLFW keyboard events
    void InputManager::__handleGlfwKeyEvent(GLFWwindow* window, int key, int scancode, int action, int mods) {
		// Calculate the state based on the last polled state (Note that we do NOT update the internal state)
		ButtonState state = (action == GLFW_PRESS ? (InputManager::myKeyboardState[key - GLFW_KEY_FIRST] == ButtonPressed ? ButtonHeld : ButtonPressed) : ButtonReleased);

		// Invoke all tied callbacks
        for(uint ix = 0; ix < InputManager::myKeyEventCallbacks.size(); ix ++)
            InputManager::myKeyEventCallbacks[ix](InputManager::myUserParam, key, scancode, state, mods);
    }

	// Handles the GLFW mouse button event
    void InputManager::__handleGlfwMouseButton(GLFWwindow* window, int button, int action, int mods) {
		// Calculate the state based on the last polled state (Note that we do NOT update the internal state)
		ButtonState state = (action == GLFW_PRESS ? (InputManager::myMouseState[button] == ButtonPressed ? ButtonHeld : ButtonPressed) : ButtonReleased);
		
		// Invoke all tied callbacks
		for (uint ix = 0; ix < InputManager::myMouseEventCallbacks.size(); ix++) {
			InputManager::myMouseEventCallbacks[ix](InputManager::myUserParam, button, state, mods);
		}

		for (uint ix = 0; ix < myMouseEventRawCallbacks.size(); ix++)
			myMouseEventRawCallbacks[ix](window, button, action, mods);
    }

	// Handle the GLFW mouse move event
    void InputManager::__handleGlfwMouseMove(GLFWwindow* window, double xPos, double yPos) {
		// Update our states in the callback (seems to work better)
		myPrevMousePos = myMousePos;
		myMousePos = glm::dvec2(xPos, yPos);
		myMouseDelta += myMousePos - myPrevMousePos;

		// Invoke the tied callbacks
		for (uint ix = 0; ix < InputManager::myMouseMoveCallbacks.size(); ix++) {
			InputManager::myMouseMoveCallbacks[ix](InputManager::myUserParam, myMousePos, myMouseDelta);
		}
    }

	// Handle the GLFW mouse scroll event
    void InputManager::__handleGlfwMouseScroll(GLFWwindow* window, double xPos, double yPos) {
		// Invoke the callbacks
		for (uint ix = 0; ix < InputManager::myMouseScrollCallbacks.size(); ix++) {
			myMouseScrollCallbacks[ix](myUserParam, xPos, yPos);
		}
    }

	// Handle the GLFW joystick event
    void InputManager::__handleGlfwJoystickChanged(int joystick, int action) {
        // Handle connection
		if (action == GLFW_CONNECTED) {
            // Handle gamepad added
            InputManager::myJoystickData[joystick]->AxisData     = glfwGetJoystickAxes(joystick, &InputManager::myJoystickData[joystick]->AxisCount);
            InputManager::myJoystickData[joystick]->ButtonData   = glfwGetJoystickButtons(joystick, &InputManager::myJoystickData[joystick]->ButtonCount);
            InputManager::myJoystickData[joystick]->ButtonStates = new ButtonState[InputManager::myJoystickData[joystick]->ButtonCount]();
            InputManager::myJoystickData[joystick]->Name         = glfwGetJoystickName(joystick);
            InputManager::myJoystickData[joystick]->IsConnected  = true;

            printf("Connecting controller @ %d\n", joystick);

        }
		// Handle disconnection
		else if (action == GLFW_DISCONNECTED) {
            // Handle gamepad disconnected
            InputManager::myJoystickData[joystick]->AxisData     = NULL;
            InputManager::myJoystickData[joystick]->ButtonData   = NULL;
            InputManager::myJoystickData[joystick]->ButtonStates = NULL;
            InputManager::myJoystickData[joystick]->AxisCount    = 0;
            InputManager::myJoystickData[joystick]->ButtonCount  = 0;
            InputManager::myJoystickData[joystick]->IsConnected  = false;

            printf("Disconnecting controller @ %d\n", joystick);

        }

		// Invoke any tied events
        for(uint ix = 0; ix < InputManager::myJoystickConnectionCallbacks.size(); ix ++)
            InputManager::myJoystickConnectionCallbacks[ix](InputManager::myUserParam, joystick, action);
    }

	// Handle GLFW text events
    void InputManager::__handleGlfwTextEvent(GLFWwindow* window, uint codepoint, int mods) {
		// Invoke any callbacks that were tied
        for(uint ix = 0; ix < InputManager::myTextEventCallbacks.size(); ix ++)
            InputManager::myTextEventCallbacks[ix](InputManager::myUserParam, codepoint, mods);
    }

} }
