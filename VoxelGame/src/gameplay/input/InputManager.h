/*
	Author: Shawn Matthews
	Date: July 20th, 2017 
	Description:
		Core low-level input and input state management
*/
#pragma once
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <vector>
#include <functional>

#include "graphics/Window.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "glm_math.h"

#define GLFW_KEY_FIRST 32

namespace gameplay {
	namespace input {

	/*
		Represents the state of a button, such as a keyboard key or joystick button. The flow of
		a button press looks like so:
		Released -> Pressed -> Held -> Released
	*/
    enum ButtonState {
		/* The key or button is released */
        ButtonReleased = 0,
		/* The key or button has been pressed since the last poll */
        ButtonPressed  = 1,
		/* The button has been held since the last state */
        ButtonHeld     = 2
    };

	/*
		Handles input management from controllers, keyboards, and mice
	*/
    class InputManager
    {
		friend class Window;

		/*
			Represents the data about a joystick or controller
		*/
        struct JoystickData {
			/* Stores the axis input data array */
            const float         *AxisData;
			/* Stores the number of axial inputs on the joystick or controller */
            int                  AxisCount;
			/* Stores the raw button states array */
            const unsigned char *ButtonData;
			/* Stores the calculated button states array */
            ButtonState         *ButtonStates;
			/* Stores the number of buttons on the controller or joystick */
            int                  ButtonCount;
			/* Stores whether the device is connected */
            bool                 IsConnected;
			/* Stores the name of the controller (NULL Terminated) */
            const char*          Name;
			/* The ID of this controller or joystick */
            int                  JoystickId;
        };

        public:
			/*
				Sets whether or not gamepad and joystick support is enabled
				@para value The new value for whether or gamepads are enabled
			*/
            static void SetGamePadsEnabled(const bool value) { InputManager::isGamePadEnabled = value; }
			
			/*
				Set's a generic usage pointer to be passed to ALL callbacks, this is shared between all callbacks.
				You can retreive the current user parameter with GetUserParam.
				@param value The new value to store in the user parameter
			*/
            static void  SetUserParam(void* value) { InputManager::myUserParam = value; }
			/*
				Gets the user parameter for the input manager. This can be set via SetUserParam.
				@returns The user parameter
			*/
            static void* GetUserParam() { return InputManager::myUserParam; }

			/*
				Gets the raw joystick data for a single joystick and axis. For more advanced input, see
				The InputSources.h file. This will return 0.0 if the input source is disconnected.
				@param joystick The joystick or gamepad ID to get the input from
				@param axis     The ID of the axis to get
				@returns        The raw axis output for the joystick and axis, or 0.0 if it's disconnected
			*/
            static const float        GetJoystickAxis(const int joystick, const int axis);

			/*
				Gets the calculated button state for the given input source and button ID. Will return
				ButtonReleased if the input source is disconnected.
				@param joystick The joystick or gamepad ID to get the input from
				@param button   The ID of the button to get
				@returns        The calculated button state
			*/
            static const ButtonState  GetJoystickButton(const int joystick, const int button);
			/*
				Gets name of the joystick connected to a given input location (null terminated). This pointer will
				be freed by GLFW.
				@param joystick The joystick or gamepad ID to get the name of
				@returns        The null-terminated name of the joystick
			*/
            static const char*        GetJoystickName(const int joystick);
			/*
				Gets whether or not a joystick or gamepad is connected to the given input location.
				@param joystick The ID of the joystick to check for
				@returns        True if the input source is connected, false if otherwise;
			*/
            static const bool         GetJoystickConnected(const int joystick);

			/*
				Gets a human-readable name for the given GLFW key code. The pointer will be managed
				by GLFW.
				@param key The key code to get the name for
				@returns   The human-readable name for the key
			*/
			static const char*        GetKeyboardKeyName(const int key);
			/*
				Gets the calculated state of a single keyboard key
				@param key The key to sample
				@returns   The calculated state of the button
			*/
			static const ButtonState  GetKeyState(const int key);

			/*
				Gets the mouse delta since the previous polled frame
				@returns The change in position since the last polling event in window coordinates
			*/
			static const glm::dvec2   GetMouseDelta() { return InputManager::myMouseDelta; }
			/*
				Gets the position of the mouse on the screen in window coordinates
				@returns The mouse position in screen coordinates
			*/
			static const glm::dvec2   GetMousePos() { return InputManager::myMousePos; }
			/*
				Sets the mouse position in window coordinates
				@param pos The new position of the mouse pointer in window coordinates
			*/
			static void               SetMousePos(glm::dvec2 pos) { SetMousePos(pos.x, pos.y); }
			/*
				Sets the mouse position in window coordinates
				@param xPos The x-position of the mouse pointer in window coordinates
				@param yPos The y-position of the mouse pointer in window coordinates
			*/
			static void               SetMousePos(double xPos, double yPos);
			/*
				Gets the calculated state of a mouse button
				@param button The ID of the button to get (see http://www.glfw.org/docs/latest/group__buttons.html)
				@returns      The calculated state of the button
			*/
			static const ButtonState  GetMouseButton(int button) { return myMouseState[button]; }

			/*
				Sets the mode of the cursor display. See http://www.glfw.org/docs/latest/group__input.html#gaa92336e173da9c8834558b54ee80563b
				@param value The new value for the cursor mode. (GLFW_CURSOR_NORMAL, GLFW_CURSOR_HIDDEN, GLFW_CURSOR_DISABLED)
			*/
			static void SetCursorMode(int value);
									

            #ifdef DEBUG
			/*
				Dumps the data about a joystick to the console
				@param joystick The ID of the joystick to display depug info for
			*/
            static void DumpJoystick(const int joystick);
            #endif // DEBUG

			/*
				Adds a new callback for when a keyboard key has raised an event
				@param callback The callback to invoke when a key event has occured
			*/
            static void AddKeyEventCallback(std::function<void(void* userParam, int key, int scancode, ButtonState state, int mods)> callback) { InputManager::myKeyEventCallbacks.push_back(callback); }
			/*
				Adds a new callback for when the user has typed text, can be used for text input controls
				@param callback The callback to invoke when a key even has occured
			*/
			static void AddTextEventCallback(std::function<void(void* userParam, uint codepoint, int mods)> callback) { InputManager::myTextEventCallbacks.push_back(callback); }
			
			/*
				Adds a new callback for when the user has moved their mouse
				@param callback The callback to invoke when the mouse has moved
			*/
            static void AddMouseMoveCallback(std::function<void(void* userParam, glm::vec2 pos, glm::vec2 delta)> callback) { InputManager::myMouseMoveCallbacks.push_back(callback); }
			/*
				Adds a new callback for when the user has clicked a mouse button
				@param callback The callback to invoke when a button state has changed
			*/
			static void AddMouseButtonEventCallback(std::function<void(void* userParam, int button, ButtonState state, int mods)> callback) { InputManager::myMouseEventCallbacks.push_back(callback); }
			
			static void AddMouseButtonRawCallback(GLFWmousebuttonfun func) { myMouseEventRawCallbacks.push_back(func); }
			/*
				Adds a new callback for when the user has scrolled the mouse
				@param callback The callback to invoke when the mouse has been scrolled
			*/
			static void AddMouseScrollCallback(std::function<void(void* userParam, double xOffset, double yOffset)> callback) { InputManager::myMouseScrollCallbacks.push_back(callback); }

			/*
				Adds a new callback for when a joystick or controller has been connected or disconnected
				@param callback The callback to invoke when a joystick has been conencted or disconnected
			*/
            static void AddJoystickConnectionCallback(std::function<void(void* userParam, int joystick, int action)> callback) { InputManager::myJoystickConnectionCallbacks.push_back(callback); }
            /*
				Adds a callback to listen for button state changes on a joystick button
				@param callback The callback to invoke on button state change
			*/
			static void AddJoystickButtonCallback(std::function<void(void* userParam, int joystick, int button, ButtonState state)> callback) { InputManager::myJoystickButtonCallbacks.push_back(callback); }

        private:
			friend class Window;
			// Stores a pointer to the source window
            static const Window* myWindow;

			// Stores the keyboard, mouse, and joystick states
			static ButtonState* myKeyboardState;
			static ButtonState* myMouseState;
            static JoystickData** myJoystickData;

			// Whether or not we update joystick information upon polling
            static bool    isGamePadEnabled;

			// Stores the mouse information
			static glm::dvec2 myPrevMousePos, myMousePos, myMouseDelta;
			static int myMouseMode;

			// Stores the customizable user parameter
            static void*   myUserParam;

			// Stores lists of all the callbacks
            static std::vector<std::function<void(void*, glm::vec2, glm::vec2)>>          myMouseMoveCallbacks;
            static std::vector<std::function<void(void*, int, int, ButtonState, int)>>    myKeyEventCallbacks;
            static std::vector<std::function<void(void*, uint, int)>>                     myTextEventCallbacks;
            static std::vector<std::function<void(void*, int, ButtonState, int)>>         myMouseEventCallbacks;
            static std::vector<std::function<void(void*, double, double)>>                myMouseScrollCallbacks;
            static std::vector<std::function<void(void*, int, int)>>                      myJoystickConnectionCallbacks;
            static std::vector<std::function<void(void*, int, int, ButtonState)>>         myJoystickButtonCallbacks;

			static std::vector<GLFWmousebuttonfun> myMouseEventRawCallbacks;

			/*
				Set the handle for the window to receive input from (Will be called when creating a new Window instance)
				@param window The window to listen to input from
			*/
			static void SetWindow(const Window *window);
			/*
				Initializes the input manager and readys it for polling
			*/
			static void Init();
			/*
				Polls the input devices and updates the state
			*/
			static void Poll();
			/*
			Notifies the input manager that a new frame has been rendered and that deltas should be reset
			*/
			static void NotifyFrame();
			/*
				Cleans up any resources used by the input manager
			*/
			static void Cleanup();

			// Internal handlers for GLFW events
            static void __handleGlfwKeyEvent(GLFWwindow* window, int key, int scancode, int action, int mods);
            static void __handleGlfwTextEvent(GLFWwindow* window, uint codepoint, int mods);
            static void __handleGlfwMouseButton(GLFWwindow* window, int button, int action, int mods);
            static void __handleGlfwMouseMove(GLFWwindow* window, double xPos, double yPos);
            static void __handleGlfwMouseScroll(GLFWwindow* window, double xPos, double yPos);
            static void __handleGlfwJoystickChanged(int joystick, int action);

    };

} } 

#endif // INPUTMANAGER_H
