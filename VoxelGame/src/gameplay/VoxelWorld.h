/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include <mutex>
#include <queue>
#include <tuple>

#include "Tweakables.h"

#include "glm_math.h"
#include "Voxel.h"
#include "VoxelChunk.h"

#include "graphics/Shader.h"
#include "graphics/NoShadowLight.h"
#include "graphics/Camera.h"

#include "tools/StretchyBuffer.h"

#include <unordered_map>

namespace gameplay {

	class VoxelWorld {
	public:
		VoxelWorld();
		~VoxelWorld();

		void Render(const graphics::Camera& camera, graphics::Shader *shader);
		void Update();

		void LoadLightsFromFile(const char* fileName);

		void ReserveLights(const uint32_t count) { myLights.Reserve(count); }
		void AddLight(const graphics::NoShadowLight& light);
		void RemoveLight(const int index);
		void ClearLights();
		graphics::NoShadowLight* GetLight(uint32_t index) const;
		uint32_t NumLights() const { return myLightCount; }
		
		void ForceLoad(glm::ivec3 chunkPos);

	private:
		void __LoadChunk(glm::ivec3 chunkPos);

		std::unordered_map<glm::ivec3, VoxelChunk*> myChunks;

		StretchyBuffer<graphics::NoShadowLight> myLights;
		uint32_t                   myLightCount;

		struct GeneratedVoxelData {
			VoxelChunk*        Chunk;
			graphics::MeshData MeshData;
			glm::ivec3         Position;
		};

		std::mutex       *myToAddMutex;
		std::queue<GeneratedVoxelData> myToAddChunkQueue;
	};

}