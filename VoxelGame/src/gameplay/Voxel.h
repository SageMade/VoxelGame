/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include "glm_math.h"
#include <cstdint>

namespace gameplay {

	enum VoxelFace {
		EAST   = 0,
		WEST   = 1,
		NORTH  = 2,
		SOUTH  = 3,
		TOP    = 4,
		BOTTOM = 5
	};

	const glm::ivec3 FACE_NORMALS[6] = {
		glm::ivec3( 1,  0,  0),
		glm::ivec3(-1,  0,  0),
		glm::ivec3( 0,  1,  0),
		glm::ivec3( 0, -1,  0),
		glm::ivec3( 0,  0,  1),
		glm::ivec3( 0,  0, -1)
	};

	typedef const char* cstr;
	const cstr FACE_NAMES[] = {
		"EAST",
		"WEST",
		"NORTH",
		"SOUTH",
		"TOP",
		"BOTTOM"
	};

	inline VoxelFace Invert(VoxelFace face) {
		switch (face) {
		case EAST:
		case NORTH:
		case TOP:
			return (VoxelFace)((int)face + 1);
		case WEST:
		case SOUTH:
		case BOTTOM:
			return (VoxelFace)((int)face - 1);
		}
	}

	inline VoxelFace DirectionToFacing(const glm::vec3& dir) {
		float max = -1;
		int axis = -1;
		for (int ix = 0; ix < 3; ix++) {
			if (abs(dir[ix]) > max) {
				max = abs(dir[ix]);
				axis = ix;
			}
		}
		return (VoxelFace)(axis * 2 + (signbit((float)dir[axis]) ? 1 : 0));
	}

	struct VoxelFaceData {
		uint8_t   Type;
		VoxelFace Face;
		bool      IsVisible;

		VoxelFaceData() : Type(0), Face(EAST), IsVisible(false) {}

		bool equals(const VoxelFaceData& other) {
			return IsVisible == other.IsVisible && Type == other.Type;
		}
	};

	struct Voxel {
		uint8_t BlockID;
		uint8_t MetaData;

		Voxel() : BlockID(0), MetaData(0) {}
		Voxel(uint8_t id) : BlockID(id), MetaData(0) {}
		Voxel(uint8_t id, uint8_t meta) : BlockID(id), MetaData(meta) {}
	};

}