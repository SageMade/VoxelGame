/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
	Modified by:
		Ivan Fedorov 
			March 19, 2018 - Added loading lights from file
*/

#include "VoxelWorld.h"

#include "tools\VoxelMesher.h"
#include "tools\WorldGen.h"

#include "ServiceProvider.h"

#include "tools\ThreadPool.h"
#include "tools\StretchyBuffer.h"
#include "tinyxml2.h"

#include "audio\AudioEngine.h"

#include "Logger.h"

#include <iostream>

namespace gameplay {
	
	using namespace graphics;

	VoxelWorld::VoxelWorld() {
		myToAddMutex = new std::mutex();
		
		myLightCount = 0;
	}

	VoxelWorld::~VoxelWorld() {

	}
	
	void VoxelWorld::Render(const gameplay::Camera& camera, graphics::Shader *shader) {

		for (auto e : myChunks) {
			if (e.second) {
				glm::mat4 world = glm::translate((glm::vec3)(e.first * CHUNK_SIZE));
				shader->SetUniform("xViewWorld", camera.GetView() * world);
				shader->SetUniform("xNormalMatrix", glm::mat3(glm::inverseTranspose(world * camera.GetView())));
				e.second->Render();
			}
		}
	}

	void VoxelWorld::Update() {
		{

			myToAddMutex->lock();
			if (myToAddChunkQueue.size() > 0) {
				GeneratedVoxelData data = myToAddChunkQueue.front();
				myToAddChunkQueue.pop();
				myToAddMutex->unlock();

#if LOG_MESH_GENERATION
				double startTime = glfwGetTime();
#endif

				data.Chunk->UpdateMesh(data.MeshData);
				myChunks[data.Position] = data.Chunk;

#if LOG_MESH_GENERATION
				double endTime = glfwGetTime();
				FILE_LOG(logINFO) << "Took " << (endTime - startTime) << " seconds to upload";
#endif

				data.MeshData.Delete();
			}
			else {
				myToAddMutex->unlock();
			}
		}

		/*
		for (int ix = -5; ix <= 5; ix++) {
			for (int iy = -5; iy <= 5; iy++) {
				for (int iz = -5; iz <= 5; iz++) {
					if (myChunks.find(glm::ivec3(ix, iy, iz)) == myChunks.end()) {
						ForceLoad(glm::ivec3(ix, iy, iz));
					}
				}
			}
		}
		*/

	}

	void VoxelWorld::AddLight(const NoShadowLight & light) {
		myLights[myLightCount] = light;
		myLightCount++;
	}

	void VoxelWorld::RemoveLight(const int index) {
		myLights.Delete(index);
		myLightCount--;
	}

	void VoxelWorld::ClearLights() {
		myLights.Clear(myLightCount);
		myLightCount = 0;
	}

	NoShadowLight * VoxelWorld::GetLight(uint32_t index) const {
		return myLights.First() + index;
	}

	void VoxelWorld::ForceLoad(glm::ivec3 chunkPos) {

		if (myChunks.find(chunkPos) == myChunks.end()) {
			myChunks[chunkPos] = nullptr;// new VoxelChunk();
			ServiceProvider::Get<tools::ThreadPool>()->Enqueue<VoxelWorld, glm::ivec3>(&VoxelWorld::__LoadChunk, this, chunkPos);
		}
	}

	void VoxelWorld::__LoadChunk(glm::ivec3 chunkPos) {
		VoxelChunk *chunk = new VoxelChunk();
		tools::WorldGen::Generate(chunk, chunkPos);
		graphics::MeshData mesh = tools::VoxelMesher::Generate(chunk);
		
		std::lock_guard<std::mutex> lock(*myToAddMutex);
		myToAddChunkQueue.push({chunk, mesh, chunkPos});
	}

	void VoxelWorld::LoadLightsFromFile(const char* fileName) {

		// Open XML doc

		tinyxml2::XMLDocument myLights;

		tinyxml2::XMLError error = myLights.LoadFile(fileName);



		if (error == tinyxml2::XML_SUCCESS)

		{

			tinyxml2::XMLNode* light = myLights.FirstChildElement()->FirstChildElement()->FirstChildElement();



			// Read number of nodes of type NoShadowLight

			//int node_count = 0;

			//while (light->NextSibling()) {

			//    node_count++;

			//}

			// Reserve lights

			ReserveLights(64);

			for (const tinyxml2::XMLNode* light = myLights.FirstChild()->FirstChild()->FirstChild(); light != 0; light = light->NextSibling()) {

				glm::vec3 temp_pos;
				glm::vec3 temp_color;
				float temp_radius;
				float temp_angle;
				LightVolumeType temp_type;

				glm::vec3 temp_euler_rot;
				for (const tinyxml2::XMLElement* element = light->FirstChildElement(); element != 0; element = element->NextSiblingElement()) {
					if (std::string(element->Name()) == "Type") {
						if (std::string(element->FirstAttribute()->Value()) == "Sphere")
							temp_type = LightVolumeType::SphereVolume;
						if (std::string(element->FirstAttribute()->Value()) == "Cone")
							temp_type = LightVolumeType::ConeVolume;
						if (std::string(element->FirstAttribute()->Value()) == "Cube")
							temp_type = LightVolumeType::CubeVolume;
					}

					if (std::string(element->Name()) == "Position") {

						temp_pos.x = element->FirstAttribute()->FloatValue();

						temp_pos.y = element->FirstAttribute()->Next()->FloatValue();

						temp_pos.z = element->FirstAttribute()->Next()->Next()->FloatValue();

					}

					if (std::string(element->Name()) == "Color") {

						temp_color.x = element->FirstAttribute()->FloatValue();

						temp_color.y = element->FirstAttribute()->Next()->FloatValue();

						temp_color.z = element->FirstAttribute()->Next()->Next()->FloatValue();
					}

					if (std::string(element->Name()) == "Radius") {

						temp_radius = element->FirstAttribute()->FloatValue();

					}

					if (std::string(element->Name()) == "Angle") {

						temp_angle = element->FirstAttribute()->FloatValue();

					}

					if (std::string(element->Name()) == "RotX") {

						temp_euler_rot.x = element->FirstAttribute()->FloatValue();

					}

					if (std::string(element->Name()) == "RotY") {

						temp_euler_rot.y = element->FirstAttribute()->FloatValue();

					}

					if (std::string(element->Name()) == "RotZ") {

						temp_euler_rot.z = element->FirstAttribute()->FloatValue();

					}
				}
				AddLight(NoShadowLight(temp_type, temp_pos, temp_color / 255.0f, temp_radius, temp_angle, temp_euler_rot));
				//std::cout << "RGB:  " << temp_color.x << " " << temp_color.y << " " << temp_color.z << std::endl;
			}
			//throw std::logic_error("The method or operation is not implemented.");

		}

	}

}
