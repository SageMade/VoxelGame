/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Global access point for registering and retreiving gameplay services
*/
#pragma once

#include <map>

class ServiceProvider {
public:
	template <typename T>
	static void Register(T* value) {
		myMap[&typeid(T)] = value;
	}

	template <typename T>
	static T* Get() {
		return ((T*)(myMap[&typeid(T)]));      
	}
	
private:
	static std::map<const type_info*, void*> myMap;    

};
