/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#pragma once

#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <GLM/gtx/rotate_vector.hpp>
#include <GLM/gtx/hash.hpp>

#define M_PI 3.14159265358979323846264338327950288

glm::vec3 mult(const glm::vec3& left, const glm::mat4& right);

glm::vec2 mult(const glm::vec2& left, const glm::mat3& right);

float randf(float min = 0.0f, float max = 1.0f);

template <typename T>
T wrap(T in, T min, T max) {
	return min + in % (max - min);
}

template <typename T, typename T2>
T wrap(T in, T2 min, T2 max) {
	return min + in % (max - min);
}