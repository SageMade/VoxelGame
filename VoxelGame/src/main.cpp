/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
*/
#include "Game.h"

/*
	A note on compilation!
	If you get runtime errors, make sure that the debugger working directory is set to OutDir
*/

/*
	Interesting files:
		Anything in the Audio Folder
		Game.cpp
			- Especially __InitAudio
			- Also lower portion of Render (ImGui portion)
		tools/AttribPool --> just 'cause it's neat
*/

int main() {
	Game game;
	game.Run();
}