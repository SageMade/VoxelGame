/**
    Implementation file for a collection of common utilities
    @date   July 27, 2017
    @author Shawn Matthews - 100412327
    @author Shaun McKinnon - 100642799
    @author Paul Puig      - 100656910
*/
#include <fstream>

//#include <mem.h>

namespace tools {
	// Implementaiton of readFile
	char* readFile(const char* filename) {
		// Declare and open the file stream
		std::ifstream file;
		file.open(filename, std::ios::binary);

		// Only read if the file is open
		if (file.is_open()) {
			// Get the starting location in the file
			uint64_t fileSize = file.tellg();
			// Seek to the end
			file.seekg(0, std::ios::end);
			// Calculate the file size from end to beginning
			fileSize = (uint64_t)file.tellg() - fileSize;
			// Seek back to the beginning of the file
			file.seekg(0, std::ios::beg);

			char* result = new char[fileSize + 1];
			// Read the entire file to our memory
			file.read(result, fileSize);

			// Make our text null-terminated
			result[fileSize] = '\0';

			// Close the file before returning
			file.close();
			return result;

		}
		// Otherwise, we failed to open our file, throw a runtime error
		else {
			throw std::runtime_error("We cannot open the file!");
		}
	}

	char* getFileExtension(const char* path, int &length) {
		length = 0;
		while (path[length] != '\0')
			length++;

		int cutFrom = length;
		while (path[cutFrom] != '.' && cutFrom > 0)
			cutFrom--;

		if (cutFrom == 0)
			return nullptr;

		cutFrom++;

		char* result = new char[length - cutFrom + 1];
		memcpy(result, path + cutFrom, length - cutFrom);
		result[length - cutFrom] = '\0';
		length -= cutFrom;
		return result;
	}

	char* getFileExtension(const char* path) {
		int length = 0;
		return getFileExtension(path, length);
	}

	// Implementation of getFilePath
	char* getFilePath(const char* path, int &pathLength) {
		pathLength = 0;
		while (path[pathLength] != '\0')
			pathLength++;

		while (pathLength > 0 && path[pathLength] != '/')
			pathLength--;

		if (pathLength > 0) {
			pathLength++;
			char* result = new char[pathLength + 1];
			memcpy(result, path, pathLength);
			result[pathLength] = '\0';

			return result;
		}
		else return NULL;
	}

	void Write(std::fstream & stream, void * data, size_t size) {
		stream.write(reinterpret_cast<char*>(data), size);
	}

	void Read(std::fstream & stream, void * data, size_t size) {
		stream.read(reinterpret_cast<char*>(data), size);
	}
}