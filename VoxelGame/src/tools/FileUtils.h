/**
	Defines multiple utilities for handling raw file access
	@date   July 27, 2017
	@author Shawn Matthews - 100412327
	@author Shaun McKinnon - 100642799
	@author Paul Puig      - 100656910
*/
#pragma once
#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <stdexcept>
#include <string>

namespace tools {

	/**
		Reads a file from the disk into a character buffer

		@param filename The name of the file to read
		@return The contents of the file

		@throw runtime_error Thrown when the file could not be opened
	*/
	char* readFile(const char* filename);

	template <typename T>
	T* readFile(const char* filename) {
		char* data = readFile(filename);
		return reinterpret_cast<T*>(data);
	}

	template <typename T>
	void Write(std::fstream& stream, const T& data) {
		stream.write(reinterpret_cast<const char*>(&data), sizeof(T));
	}

	template <typename T>
	void Write(std::fstream& stream, const T& data, size_t size) {
		stream.write(reinterpret_cast<const char*>(&data), size);
	}

	template <typename T>
	void Write(std::fstream& stream, const T* data) {
		stream.write(reinterpret_cast<const char*>(data), sizeof(T));
	}

	template <typename T>
	void Write(std::fstream& stream, const T* data, size_t size) {
		stream.write(reinterpret_cast<const char*>(data), size);
	}

	void Write(std::fstream& stream, void *data, size_t size);

	template <typename T>
	void Read(std::fstream& stream, T *data) {
		stream.read(reinterpret_cast<char*>(data), sizeof(T));
	}

	template <typename T>
	void Read(std::fstream& stream, T& data) {
		stream.read(reinterpret_cast<char*>(&data), sizeof(T));
	}

	template <typename T>
	void Read(std::fstream& stream, T *data, size_t size) {
		stream.read(reinterpret_cast<char*>(data), size);
	}

	template <typename T>
	void Read(std::fstream& stream, T& data, size_t size) {
		stream.read(reinterpret_cast<char*>(&data), size);
	}

	void Read(std::fstream& stream, void *data, size_t size);

	/**
		Gets the path part of a filename

		@param filename The name of the file to get the path for
		@param pathLength The length of the resulting text
	*/
	char* getFilePath(const char* filename, int &pathLength);

	char* getFileExtension(const char* path);
	char* getFileExtension(const char* path, int &length);
}

template <typename T> inline constexpr int sgn(T val) {
	return (T(0) < val) - (val < T(0));
}

class io_exception : public std::runtime_error
{
public:
	io_exception(const std::string& message = "A file access operation has failed") : runtime_error(message) {}
};

#endif // UTILS_H_INCLUDED
