/*
	Author: Shawn Matthews
	Date:   Feb 10, 2018
*/
#pragma once

#include <cstdint>
#include <memory>
#include <stdexcept>

template <typename ValueType, bool AutoResizeOnIndex = true>
class StretchyBuffer {
public:
	StretchyBuffer();
	~StretchyBuffer();
	StretchyBuffer(const StretchyBuffer&& other);

	void Reserve(int count);

	void Delete(const uint32_t index);
	void Delete(const ValueType& value);
	void Delete(const ValueType& value, bool(*predicate)(const ValueType&, const ValueType&));

	void Swap(const uint32_t l, const uint32_t r);

	uint32_t Size() const { return mySize; }
	uint32_t Length() const { return mySize; }

	ValueType* First() const { return myData; }
	ValueType* Last() const { return myData + (mySize - 1); }

	void Clear(const int count);

	ValueType& operator [](const int index);

private:
	ValueType *myData;
	uint32_t   mySize;

	ValueType *myCopyBuffer;

	void __Resize(const uint32_t newSize);
	void __Swap(const uint32_t l, const uint32_t r);
};

template <typename ValueType, bool AutoResizeOnIndex /*= true*/>
void StretchyBuffer<ValueType, AutoResizeOnIndex>::Clear(const int count) {
	for (int ix = 0; ix < count; ix++)
		myData[ix].~ValueType();
	memset(myData, 0, mySize * sizeof(ValueType));
}

template<typename ValueType, bool AutoResizeOnIndex>
inline StretchyBuffer<ValueType, AutoResizeOnIndex>::StretchyBuffer() {
	myData = nullptr;
	myCopyBuffer = (ValueType*)malloc(sizeof(ValueType));
	mySize = 0;
}

template<typename ValueType, bool AutoResizeOnIndex>
inline StretchyBuffer<ValueType, AutoResizeOnIndex>::~StretchyBuffer() {
	free(myData);
	free(myCopyBuffer);
	mySize = 0;
}

template<typename ValueType, bool AutoResizeOnIndex>
inline StretchyBuffer<ValueType, AutoResizeOnIndex>::StretchyBuffer(const StretchyBuffer && other) {
	myData = (ValueType*)malloc(other.myAllocatedCount * sizeof(ValueType));
	myCopyBuffer = (ValueType*)malloc(sizeof(ValueType));
	mySize = other.mySize;
	
	for (uint32_t ix = 0; ix < other.mySize; ix++)
		myData[ix] = ValueType(other.myData[ix]); // Very explicitly call copy constructor
}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::Reserve(int count) {
	if (count > 0) {
		uint32_t newSize = mySize + count;
		__Resize(newSize);
	}
}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::Delete(const uint32_t index) {
	if (index < mySize) {
		myData[index].~ValueType();
		if (index < mySize - 1)
			memmove(myData + index, myData + index + 1, (mySize - 1 - index) * sizeof(ValueType));
		memset(myData + mySize - 1, 0, sizeof(ValueType));
	}
}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::Delete(const ValueType & value) {
	ValueType* current = First();
	ValueType* last = Last();
	for (uint32_t ix = 0; current <= last; current++, ix++) {
		if (*current == value) {
			Delete(ix);
			ix--;
			current--;
		}
	}

}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::Delete(const ValueType & value, bool(*predicate)(const ValueType &, const ValueType &)) {
	ValueType* current = First();
	ValueType* last = Last();
	for (uint32_t ix = 0; current <= last; current++, ix++) {
		if (predicate(*current, value)) {
			Delete(ix);
			ix--;
			current--;
		}
	}
}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::Swap(const uint32_t l, const uint32_t r) {
	__Swap(l, r);
}

template<typename ValueType, bool AutoResizeOnIndex>
inline ValueType & StretchyBuffer<ValueType, AutoResizeOnIndex>::operator[](const int index) {
	if (index >= mySize && AutoResizeOnIndex)
		__Resize(index + 1);

	if (index < 0 || index >= mySize)
		throw std::range_error("Index element out of range");

	return *(myData + index);
}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::__Resize(const uint32_t newSize) {
	if (newSize > mySize) {
		ValueType* newPtr = (ValueType*)malloc(newSize * sizeof(ValueType));
		memcpy(newPtr, myData, sizeof(ValueType) * mySize);
		free(myData);
		myData = newPtr;
		mySize = newSize;
	}
}

template<typename ValueType, bool AutoResizeOnIndex>
inline void StretchyBuffer<ValueType, AutoResizeOnIndex>::__Swap(const uint32_t l, const uint32_t r) {
	if (l >= mySize || r >= mySize)
		throw std::range_error("Index element out of range");

	memcpy(myCopyBuffer, myData + l, sizeof(ValueType));
	memcpy(myData + l, myData + r, sizeof(ValueType));
	memcpy(myData + r, myCopyBuffer, sizeof(ValueType));
}
