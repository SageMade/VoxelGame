/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Provides tools for queueing jobs on multiple threads
*/
#pragma once
#include "Tweakables.h"

#include <functional>
#include <queue>
#include <thread>
#include <mutex>

namespace tools {

	/*
		Represents a collection of multiple threads that can be queued to run jobs in parallel
	*/
	class ThreadPool {
	public:
		ThreadPool();
		~ThreadPool();

		template <typename T, typename ... TArgs>
		void Enqueue(void(T::*function)(TArgs...), T* context, TArgs... args) {
			__RegisterJob(std::bind(function, context, args...));
		}
		template <typename ... TArgs>
		void Enqueue(void(*function)(TArgs...), TArgs... args) {
			__RegisterJob((std::function<void()>)([function, args...](){ function(args...);  }));
		}

	private:
		void __ThreadExecute(int ix);
		void __RegisterJob(std::function<void()> func);

		std::queue<std::function<void()>>   myTaskQueue;
		uint32_t                            myThreadCount;
		std::thread                        *myThreads;
		std::mutex                         *myMutex;
		std::condition_variable             myCondVar;
		bool                                isDying;

	};

}