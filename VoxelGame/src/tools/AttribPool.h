/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Provides tools for storing and fetching attributes via string keys. Type safety is ensured via the 
		typeid attribute
	Note:
		This is a header-only implementation
*/
#pragma once

#include <unordered_map>

namespace tools {

	// Typedef a destructor that takes in a void pointer and deletes it
	typedef void(*dtor)(void*);

	/*
		Templated delete for a value type (simply calls destructor)
	*/
	template <typename T>
	void __deleteValue(T& data) {
		data.~T();
	}
	
	/*
		Templated delete for a pointer type
	*/
	template <typename T>
	void __deletePtr(void* data) {
		// We need to strip the pointer off the type so we can get the destructor
		using derefed = typename std::remove_pointer<T>::type;
		// Get the referenced value
		T v = *(T*)data;
		// If it is not null, call delete on the de-referenced value
		if (v)
			__deleteValue(*v);
	}

	template <typename T>
	void __deleteValue(void* data) {
		(*(T*)data).~T();
	}

	template <typename T, typename std::enable_if_t<!std::is_pointer<T>::value>* = 0>
	dtor __makeDtor() {
		return __deleteValue<T>;
	}

	template <typename T, typename std::enable_if_t<std::is_pointer<T>::value>* = 0>
	dtor __makeDtor() {
		return __deletePtr<T>;
	}

	/*
		A pool of data that can store any arbitrary data types, indexed on strings
	*/
	class AttribPool {
	public:
		/*
			Gets a ref the the element with the given name, as type T, and if it does not exist,
			assign default value to the name and return value
		*/
		template <typename T>
		T& Get(const std::string& name, const T& defaultVal) {
			return myValueMap[name].Get<T>(defaultVal);
		}
		/*
			Gets a ref the the element with the given name as type T, and will default construct a new
			instance of T if required
		*/
		template <typename T>
		T& Get(const std::string& name) {
			return myValueMap[name].Get<T>();
		}
		/*
			Sets the element with the given name to the given type and value
		*/
		template <typename T>
		T& Set(const std::string& name, const T& value) {
			return myValueMap[name].Set<T>(value);
		}

		/*
			Returns true if there is an element with the given name
		*/
		template <typename T>
		bool Has(const std::string& name) {
			return myValueMap.find(name) != myValueMap.end();
		}
		
		/*
			Removes an element with the given name from the map and deletes it's underlying
			data resource (will also delete if pointer)
		*/
		void Remove(const std::string& name) {
			if (myValueMap.find(name) != myValueMap.end()) {
				myValueMap.erase(name);
			}
		}
		
	private:
		/*
			Internal container type for an attribute
		*/
		struct Attribute {
			Attribute() : myType(nullptr), myData(nullptr), myDestructor(nullptr) {};
			~Attribute() {
				// If there is data
				if (myData) {
					// Call the destructor on the data
					myDestructor(myData);
					// C-Style free 
					free(myData);
					// Clear the data ptr for easier debugging
					myData = nullptr;
				}
			}

			/*
				Creates a new attribute with the given type and data
			*/
			template <typename T>
			static Attribute Create(const T& data) {
				// Store type ID for later type checking
				myType = &typeid(T);
				// C-style allocations
				myData = malloc(sizeof(T));
				// Call the copy-constructor in place so that we are consistent
				new (myData) T(data);
				// Grab a destructor while the type is still well-defined
				myDestructor = __makeDtor<T>();
			}

			/*
				Gets the value as type T from this attribute
			*/
			template <typename T>
			T& Get() {
				// If we have no data, then just make a new T
				if (myData == nullptr) {
					// Grab typeID, destructor, and allocate memory
					myType = &typeid(T);
					myDestructor = __makeDtor<T>();
					myData = malloc(sizeof(T));
					// Perform an in-place constructor 
					new (myData) T();
					// Return the dereferenced value
					return *(T*)myData;
				}

				// Otherwise if our type does not match the requested type, throw an error
				if (myType != &typeid(T))
					throw;
				else
					return *(T*)(myData);
			}
			
			/*
				Sets the value of this attribute
			*/
			template <typename T>
			T& Set(const T& value) {
				// If we have no data, then just make a new T
				if (myData == nullptr) {
					// Grab typeID, destructor, and allocate memory
					myType = &typeid(T);
					myDestructor = __makeDtor<T>();
					myData = malloc(sizeof(T));
					// Perform an in-place constructor 
					new (myData) T(value);
					// Return the dereferenced value
					return *(T*)myData;
				}

				// Otherwise if our type does not match the requested type, throw an error
				if (myType != &typeid(T))
					throw;
				else {
					// If all is well, copy the data into the underlying storage
					*(T*)(myData) = value;
					return *(T*)(myData);
				}
			}

			/*
				Gets the underlying data as type T, and optionally sets data to default value if
				there is no backing data for this attribute yet
			*/
			template <typename T>
			T& Get(const T& defaultVal) {
				// If we have no data, then just make a new T
				if (myData == nullptr) {
					// Grab typeID, destructor, and allocate memory
					myType = &typeid(T);
					myDestructor = __makeDtor<T>();
					myData = malloc(sizeof(T));
					// Perform an in-place constructor 
					new (myData) T(defaultVal);
					// Return the dereferenced value
					return *(T*)myData;
				}

				if (myType != &typeid(T))
					throw;
				else
					return *(T*)(myData);
			}

		private:
			const type_info *myType;
			void            *myData;
			dtor             myDestructor;
		};

		std::unordered_map<std::string, Attribute> myValueMap;
	};
}