/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Handles filling a voxel chunk with data to represent the game world
*/
#include "WorldGen.h"

#include "Tweakables.h"

#include "Logger.h"

namespace tools {
	
	using namespace gameplay;
	
	float WorldGen::mySeed;

	float norm(double in) {
		return (in + 1.0) / 2.0f;
	}

	float map(float x, float in_min, float in_max, float out_min, float out_max)
	{
		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	void WorldGen::Init(uint32_t seed) {
		mySeed = seed % 512;
	}

	void WorldGen::Generate(gameplay::VoxelChunk * chunk, const glm::ivec3& chunkCoords) {

#if LOG_MESH_GENERATION
		double startTime = glfwGetTime();
#endif

		glm::ivec3 worldPos = chunkCoords * CHUNK_SIZE;

		for (int x = 0, xWorld = worldPos.x; x < CHUNK_SIZE; x++, xWorld++) {
			for (int y = 0, yWorld = worldPos.y; y < CHUNK_SIZE; y++, yWorld++) {
				int height = __TerrainHeight(xWorld, yWorld);

				for (int z = 0, zWorld = worldPos.z; z < CHUNK_SIZE; z++, zWorld++) {
					/*
					Voxel result = Voxel(0);
					// Do each pass here
					for (uint32_t passIx = 0; passIx < Passes.size(); passIX++) {
						Passes[passIx]->Generate(&result, x, y, z);
					}
					*/

					if (zWorld == height)
						chunk->SetLocal(x, y, z, Voxel(1));
					else if (zWorld < height & zWorld >= height - WORLDGEN_DIRT_DEPTH)
						chunk->SetLocal(x, y, z, Voxel(2));
					else if (zWorld < height - WORLDGEN_DIRT_DEPTH)
						chunk->SetLocal(x, y, z, Voxel(3));
					else
						chunk->SetLocal(x, y, z, Voxel(0));

					float caveDens = norm(stb_perlin_noise3(xWorld / 24.0f + mySeed, yWorld / 24.0f + mySeed, zWorld / 8.0f + mySeed, 4096, 4096, 4096));
					if (caveDens < 0.3f)
						chunk->SetLocal(x, y, z, Voxel(0));

				}
			}
		}


#if LOG_MESH_GENERATION
		double endTime = glfwGetTime();
		FILE_LOG(logINFO) << "Generated chunk world data in " << (endTime - startTime) << " seconds";
#endif
	}

	float WorldGen::__TerrainHeight(float x, float y) {
		//return map(__NoiseOctaves<2>(x, y, 1.0f), 0, 1, TERRAIN_MIN_HEIGHT, TERRAIN_MAX_HEIGHT);
		//return map(myNoise->noise(x, y, 1.0f), 0, 1, TERRAIN_MIN_HEIGHT, TERRAIN_MAX_HEIGHT);
		float biomeHeight = norm(stb_perlin_fbm_noise3(x / 679.0f + mySeed, y / 679.0f + mySeed, 0.2f, 2.0f, 0.5f, 6, 65536, 65536, 1024));
		return TERRAIN_MIN_HEIGHT + (biomeHeight * norm(stb_perlin_noise3(x / 6237.0f + mySeed, y / 6237.0f + mySeed, 0.2f, 1024, 1024, 1024))) * (TERRAIN_MAX_HEIGHT - TERRAIN_MIN_HEIGHT);
	}

}
