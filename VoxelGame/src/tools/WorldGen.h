/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Handles filling a voxel chunk with data to represent the game world
*/
#pragma once

#include "gameplay/VoxelChunk.h"

#include <stb_perlin.h>

namespace tools {

	class WorldGen {
	public:
		static void Init(uint32_t seed = 0);
		static void Generate(gameplay::VoxelChunk *chunk, const glm::ivec3& chunkCoords);

	private:
		static float __TerrainHeight(float x, float y);
		static float mySeed;
	};

}