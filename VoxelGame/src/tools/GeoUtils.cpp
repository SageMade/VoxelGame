/*
	Author: Shawn Matthews
	Date:   Feb 09, 2018
*/

#include "GeoUtils.h"

#include "graphics/VertexLayouts.h"

namespace GeoUtils {
	using namespace graphics;
	
	void Push(std::vector<VertexPositionColorNormal>& verts, const glm::vec3& pos, const glm::vec4& col) {
		VertexPositionColorNormal vert = VertexPositionColorNormal();
		vert.Position = pos;
		vert.Color = col;
		vert.Normal = glm::normalize(pos);
		verts.push_back(vert);
	}
	
	uint32_t pack(glm::vec4 color) {
		glm::u8vec4 vec = (glm::u8vec4)(color * 255.0f);
		uint32_t result = vec.r << 24 | vec.g << 16 | vec.b << 8 | vec.a;
		return result;
	}

	uint16_t getMiddlePoint(int p1, int p2, std::vector<VertexPositionColorNormal>& geometry, const glm::vec4& col)
	{
		uint16_t ret;

		glm::vec3 point1 = geometry[p1].Position;
		glm::vec3 point2 = geometry[p2].Position;
		glm::vec3 middle = {
			(point1.x + point2.x) / 2.0,
			(point1.y + point2.y) / 2.0,
			(point1.z + point2.z) / 2.0
		};

		// add vertex makes sure point is on unit sphere
		uint16_t i = geometry.size();
		Push(geometry, glm::normalize(middle), col);

		// store it, return index
		return i;
	}

	/*
		Algorithm modified from 
		http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
	*/
	MeshData CreateSphere(float radius, int tessalation)
	{
		struct face {
			uint16_t P1;
			uint16_t P2;
			uint16_t P3;
		};

		typedef VertexPositionColorNormal Vert;

		glm::vec4 color = glm::vec4(glm::vec3(0.5f), 1.0f);

		std::vector<Vert> vertices;
		std::vector<face> faces;
		float t = (1.0f + sqrt(5.0f)) / 2.0f;
		Push(vertices, glm::normalize(glm::vec3( -1,  t,  0 )), color);
		Push(vertices, glm::normalize(glm::vec3(  1,  t,  0 )), color);
		Push(vertices, glm::normalize(glm::vec3( -1, -t,  0 )), color);
		Push(vertices, glm::normalize(glm::vec3(  1, -t,  0 )), color);
		Push(vertices, glm::normalize(glm::vec3(  0, -1,  t )), color);
		Push(vertices, glm::normalize(glm::vec3(  0,  1,  t )), color);
		Push(vertices, glm::normalize(glm::vec3(  0, -1, -t )), color);
		Push(vertices, glm::normalize(glm::vec3(  0,  1, -t )), color);
		Push(vertices, glm::normalize(glm::vec3(  t,  0, -1 )), color);
		Push(vertices, glm::normalize(glm::vec3(  t,  0,  1 )), color);
		Push(vertices, glm::normalize(glm::vec3( -t,  0, -1 )), color);
		Push(vertices, glm::normalize(glm::vec3( -t,  0,  1 )), color);

		// Initial faces
		faces.push_back({ 0, 11,  5});
		faces.push_back({ 0,  5,  1});
		faces.push_back({ 0,  1,  7});
		faces.push_back({ 0,  7, 10});
		faces.push_back({ 0, 10, 11});
		faces.push_back({ 1,  5,  9});
		faces.push_back({ 5, 11,  4});
		faces.push_back({11, 10,  2});
		faces.push_back({10,  7,  6});
		faces.push_back({ 7,  1,  8});
		faces.push_back({ 3,  9,  4});
		faces.push_back({ 3,  4,  2});
		faces.push_back({ 3,  2,  6});
		faces.push_back({ 3,  6,  8});
		faces.push_back({ 3,  8,  9});
		faces.push_back({ 4,  9,  5});
		faces.push_back({ 2,  4, 11});
		faces.push_back({ 6,  2, 10});
		faces.push_back({ 8,  6,  7});
		faces.push_back({ 9,  8,  1});

		for (int i = 0; i < tessalation; i++)
		{
			auto faces2 = std::vector<face>();
			for(const face& tri: faces)
			{
				// replace triangle with 4 triangles
				uint16_t a = getMiddlePoint(tri.P1, tri.P2, vertices, color);
				uint16_t b = getMiddlePoint(tri.P2, tri.P3, vertices, color);
				uint16_t c = getMiddlePoint(tri.P3, tri.P1, vertices, color);

				faces2.push_back({tri.P1, a, c});
				faces2.push_back({tri.P2, b, a});
				faces2.push_back({tri.P3, c, b});
				faces2.push_back({ a, b, c });
			}
			faces = faces2;
		}

		uint16_t *dataI = new uint16_t[faces.size() * 3];

		uint16_t ix = 0;
		for(const auto& tri :faces)
		{
			dataI[ix + 0] = tri.P1;
			dataI[ix + 1] = tri.P2;
			dataI[ix + 2] = tri.P3;
			ix += 3;
		}

		Vert* dataV = new Vert[vertices.size()];
		memcpy(dataV, vertices.data(), sizeof(Vert) * vertices.size());
		graphics::MeshData result;
		result.Initialize(dataV, (uint16_t)vertices.size(), dataI, (uint16_t)faces.size() * 3, &VertPosColNorm);
		return result;
	}

	graphics::MeshData CreateCube(const glm::vec3& offset, const glm::vec3& halfExtents, const glm::vec4& color) {
		
		typedef VertexPositionColorNormal Vert;
		
		Vert *vertices = new Vert[6 * 4]; // 6 faces, 4 verts a peice 
		USHORT *indices = new USHORT[6 * 2 * 3]; // 6 faces, 2 tris per face, 3 indices per tri
		
		vertices[0].Position =  glm::vec3(-halfExtents.x, halfExtents.y, -halfExtents.z);
		vertices[0].Color = color;
		vertices[1].Position = glm::vec3(-halfExtents.x, -halfExtents.y, -halfExtents.z);
		vertices[1].Color = color;
		vertices[2].Position = glm::vec3(halfExtents.x, -halfExtents.y, -halfExtents.z);
		vertices[2].Color = color;
		vertices[3].Position = glm::vec3(halfExtents.x, halfExtents.y, -halfExtents.z);
		vertices[3].Color = color;

		vertices[0].Normal = vertices[1].Normal = vertices[2].Normal = vertices[3].Normal = glm::vec3(0.0f, 0.0f, -1.0f);

		vertices[4].Position = offset + glm::vec3(-halfExtents.x, -halfExtents.y, -halfExtents.z);
		vertices[4].Color = color;
		vertices[5].Position = offset + glm::vec3(-halfExtents.x, -halfExtents.y, halfExtents.z);
		vertices[5].Color = color;
		vertices[6].Position = offset + glm::vec3(halfExtents.x, -halfExtents.y, halfExtents.z);
		vertices[6].Color = color;
		vertices[7].Position = offset + glm::vec3(halfExtents.x, -halfExtents.y, -halfExtents.z);
		vertices[7].Color = color;

		vertices[4].Normal = vertices[5].Normal = vertices[6].Normal = vertices[7].Normal = glm::vec3(0.0f, -1.0f, 0.0f);

		vertices[8].Position = offset + glm::vec3(-halfExtents.x, -halfExtents.y, halfExtents.z);
		vertices[8].Color = color;
		vertices[9].Position = offset + glm::vec3(-halfExtents.x, halfExtents.y, halfExtents.z);
		vertices[9].Color = color;
		vertices[10].Position = offset + glm::vec3(halfExtents.x, halfExtents.y, halfExtents.z);
		vertices[10].Color = color;
		vertices[11].Position = offset + glm::vec3(halfExtents.x, -halfExtents.y, halfExtents.z);
		vertices[11].Color = color;

		vertices[8].Normal = vertices[9].Normal = vertices[10].Normal = vertices[11].Normal = glm::vec3(0.0f, 0.0f, 1.0f);

		vertices[12].Position = offset + glm::vec3(halfExtents.x, halfExtents.y, -halfExtents.z);
		vertices[12].Color = color;
		vertices[13].Position = offset + glm::vec3(halfExtents.x, halfExtents.y, halfExtents.z);
		vertices[13].Color = color;
		vertices[14].Position = offset + glm::vec3(-halfExtents.x, halfExtents.y, halfExtents.z);
		vertices[14].Color = color;
		vertices[15].Position = offset + glm::vec3(-halfExtents.x, halfExtents.y, -halfExtents.z);
		vertices[15].Color = color;

		vertices[12].Normal = vertices[13].Normal = vertices[14].Normal = vertices[15].Normal = glm::vec3(0.0f, 1.0f, 0.0f);

		vertices[16].Position = offset + glm::vec3(-halfExtents.x, halfExtents.y, -halfExtents.z);
		vertices[16].Color = color;
		vertices[17].Position = offset + glm::vec3(-halfExtents.x, halfExtents.y, halfExtents.z);
		vertices[17].Color = color;
		vertices[18].Position = offset + glm::vec3(-halfExtents.x, -halfExtents.y, halfExtents.z);
		vertices[18].Color = color;
		vertices[19].Position = offset + glm::vec3(-halfExtents.x, -halfExtents.y, -halfExtents.z);
		vertices[19].Color = color;

		vertices[16].Normal = vertices[17].Normal = vertices[18].Normal = vertices[19].Normal = glm::vec3(-1.0f, 0.0f, 0.0f);

		vertices[20].Position = offset + glm::vec3(halfExtents.x, -halfExtents.y, -halfExtents.z);
		vertices[20].Color = color;
		vertices[21].Position = offset + glm::vec3(halfExtents.x, -halfExtents.y, halfExtents.z);
		vertices[21].Color = color;
		vertices[22].Position = offset + glm::vec3(halfExtents.x, halfExtents.y, halfExtents.z);
		vertices[22].Color = color;
		vertices[23].Position = offset + glm::vec3(halfExtents.x, halfExtents.y, -halfExtents.z);
		vertices[23].Color = color;

		vertices[20].Normal = vertices[21].Normal = vertices[22].Normal = vertices[23].Normal = glm::vec3(1.0f, 0.0f, 0.0f);

		/*
		for (int ix = 0; ix < 6 * 4; ix += 4) {
			vertices[ix + 0].Texture = glm::vec2(0, 0);
			vertices[ix + 1].Texture = glm::vec2(0, 1);
			vertices[ix + 2].Texture = glm::vec2(1, 1);
			vertices[ix + 3].Texture = glm::vec2(1, 0);
		}
		*/

		for (int ix{ 0 }, vx{ 0 }; ix < 6 * 2 * 3; ix += 6, vx += 4) {
			indices[ix + 0] = vx + 0;
			indices[ix + 1] = vx + 2;
			indices[ix + 2] = vx + 1;
			indices[ix + 3] = vx + 0;
			indices[ix + 4] = vx + 3;
			indices[ix + 5] = vx + 2;
		}

		// Create and return the mesh structure
		graphics::MeshData result;
		result.Initialize(vertices, (uint16_t)24, indices, (uint16_t)36, &VertPosColNorm);
		return result;
		
	}

	graphics::MeshData CreateCone(const float height, const float angle, const uint16_t tesselation) {// Create an array of vertices

		typedef VertexPositionColorNormal Vert;

		int segments = 3 + tesselation * 3;

		uint16_t vertexCount = segments * 2 + 2;
		Vert *vertices = new Vert[vertexCount];

		glm::vec4 color = glm::vec4(0.5f, 0.5f, 0.5f, 1.0f);

		//int segments = 3 * (1 + tesselation);
		float radius = height * tan(glm::radians(angle));
		
		/*
		 /---\
		/\ | /\
		|_\|/_|   This would be a circle with 8 segments
		| /|\ |   arc is the angle per segment
		\/ | \/
		 \_|_/

		*/

		// Determine the size of each arc, basically we divide the total angle (2pi)
		double arc = M_PI * 2 / segments;

		// Set our center vertex to be at the offset, and set it's UV coord to the center of the texture
		vertices[0].Position = glm::vec3(height, 0.0f, 0.0f);
		vertices[0].Color = color;
		vertices[0].Normal = glm::vec3(0.0f, 0.0f, 1.0f);

		vertices[1].Position = glm::vec3(0.0f);
		vertices[1].Color = color;
		vertices[1].Normal = glm::vec3(0.0f, 0.0f, 0.0f);

		uint16_t firstSlot = segments + 2;
		uint16_t firstSlotPlane = 2;

		// Iterate over each segment in our circle
		for (int i = 0; i < segments; i++) {
			// Determine the position of the vertex, using some basic trig
			vertices[firstSlotPlane + i].Position.x = height;
			vertices[firstSlotPlane + i].Position.y = cos(arc * i) * radius;
			vertices[firstSlotPlane + i].Position.z = sin(arc * i) * radius;
			vertices[firstSlotPlane + i].Color = vertices[0].Color;
			vertices[firstSlotPlane + i].Normal = glm::vec3(1.0f, 0.0f, 0.0f);

			vertices[firstSlot + i].Position.x = height;
			vertices[firstSlot + i].Position.y = cos(arc * i) * radius;
			vertices[firstSlot + i].Position.z = sin(arc * i) * radius;
			vertices[firstSlot + i].Color = vertices[1].Color;
			vertices[firstSlot + i].Normal = glm::vec3(0.0f, 0.0f, 0.0f);
		}

		// Create our index array
		uint16_t indexCount = ((segments) * 2) * 3;
		USHORT *indices = new USHORT[indexCount];

		firstSlot -= 1;

		// Iterate over our segments, from 0 to our second-to last
		for (int seg = 0; seg < segments - 1; seg++) {
			// Each segment has the first vertex at the center
			indices[(firstSlotPlane * 3) + seg * 3 + 0] = 0;
			indices[(firstSlotPlane * 3) + seg * 3 + 1] = firstSlotPlane + seg;
			indices[(firstSlotPlane * 3) + seg * 3 + 2] = firstSlotPlane + seg + 1;

			indices[(firstSlot * 3) + seg * 3 + 0] = 1;
			indices[(firstSlot * 3) + seg * 3 + 1] = firstSlot + seg + 1;
			indices[(firstSlot * 3) + seg * 3 + 2] = firstSlot + seg;
		}

		// The last segment (which we store in the first segment slot (tricksy little buggers)
		// is comprised of the center vertex, and the first and last edge vertices
		indices[0] = 0;
		indices[1] = firstSlot;
		indices[2] = firstSlotPlane;

		indices[3] = 1;
		indices[4] = firstSlot;
		indices[5] = firstSlot + segments - 1;

		graphics::MeshData result;
		result.Initialize(vertices, vertexCount, indices, indexCount, &VertPosColNorm);
		RecalculateNormals(result);
		return result;
	}

	void RecalculateNormals(graphics::MeshData& mesh) {
		size_t offset = mesh.VDecl->GetOffset<glm::vec3>(VType_Normal);
		size_t size = mesh.VDecl->ElementSize;

		// Initialize all normals to 0
		void* dataPtr = (char*)mesh.Vertices + offset;
		for (int ix = 0; ix < mesh.VertexCount; ix++)
			*((glm::vec3*)dataPtr) = glm::vec3(0.0f);

		// foreach three indices in triangles list of mesh :
		for (int ix = 0; ix < mesh.IndexCount; ix += 3) {
			glm::vec3 normal = CalculateSurfaceNormal(
				*mesh.ResolveVertexAttrib<glm::vec3>(mesh.Indices[ix + 0], offset),
				*mesh.ResolveVertexAttrib<glm::vec3>(mesh.Indices[ix + 1], offset),
				*mesh.ResolveVertexAttrib<glm::vec3>(mesh.Indices[ix + 2], offset));

			*(mesh.ResolveVertexAttrib<glm::vec3>(mesh.Indices[ix + 0], offset)) = normal;
			*(mesh.ResolveVertexAttrib<glm::vec3>(mesh.Indices[ix + 1], offset)) = normal;
			*(mesh.ResolveVertexAttrib<glm::vec3>(mesh.Indices[ix + 2], offset)) = normal;
		}

		dataPtr = (char*)mesh.Vertices + offset;
		for (int ix = 0; ix < mesh.VertexCount; ix++)
			*((glm::vec3*)dataPtr) = glm::normalize(*((glm::vec3*)dataPtr));
	}
	
}
