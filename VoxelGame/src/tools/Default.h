/*
	Author: Shawn Matthews
	Date:   Feb 07, 2018
	Modified From: https://stackoverflow.com/questions/2333728/stdmap-default-value#answer-11870240
*/

#pragma once

/*
	Provides a constructor that will default to the value provided via template argument,
	implicitly castable to the underlying type
*/
template<typename T, T X>
struct Default {
	Default() : Value(T(X)) {}
	Default(T const & val) : Value(val) {}
	operator T & () { return Value; }
	operator T const & () const { return Value; }
	T Value;
};