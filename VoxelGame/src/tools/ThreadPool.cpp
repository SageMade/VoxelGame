/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Provides tools for queueing jobs on multiple threads
*/
#include "ThreadPool.h"

#include "Logger.h"

namespace tools {

	ThreadPool::ThreadPool() : isDying(false){
		myThreadCount = max(std::thread::hardware_concurrency() - THREAD_POOL_RESERVED_THREADS, THREAD_POOL_DEFAULT_THREADS);

		FILE_LOG(logINFO) << "Initializing thread pool with " << myThreadCount << " threads";

		myThreads = new std::thread[myThreadCount];

		myMutex = new std::mutex();
		for (uint32_t ix = 0; ix < myThreadCount; ix++) {
			myThreads[ix] = std::thread(std::bind(&ThreadPool::__ThreadExecute, this, ix));
		}
	}

	ThreadPool::~ThreadPool() {
		{
			// Unblock any threads and tell them to stop
			std::unique_lock <std::mutex> l(*myMutex);

			isDying = true;
			myCondVar.notify_all();
		}

		// Wait for all threads to stop
		FILE_LOG(logINFO) << "Joining threads";
		for(uint32_t ix = 0; ix < myThreadCount; ix++)
			myThreads[ix].join();
	}

	void ThreadPool::__ThreadExecute(int ix) {
		std::function <void(void)> job;

		while (true)
		{
			{
				std::unique_lock <std::mutex> lock(*myMutex);

				while (!isDying && myTaskQueue.empty())
					myCondVar.wait(lock);

				if (myTaskQueue.empty())
				{
					// No jobs to do and we are shutting down
					FILE_LOG(logINFO) << "Thread " << ix << " has terminated" << std::endl;
					return;
				}

				job = std::move(myTaskQueue.front());
				myTaskQueue.pop();
			}

			// Do the job without holding any locks
			job();
		}
	}

	void ThreadPool::__RegisterJob(std::function<void()> func) {
		std::unique_lock<std::mutex> lock(*myMutex);
		myTaskQueue.emplace(std::move(func));
		myCondVar.notify_one();
	}

}
