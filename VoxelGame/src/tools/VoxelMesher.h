/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Handles creating a mesh from a collection of voxels. This is translated / modifed from a java sample found
		at https://github.com/roboleary/GreedyMesh/blob/master/src/mygame/Main.java
*/
#pragma once

#include "gameplay/VoxelChunk.h"
#include "graphics/MeshData.h"

namespace tools {

	class VoxelMesher {
	public:
		static graphics::MeshData Generate(const gameplay::VoxelChunk *chunk);
	private:
	};

}