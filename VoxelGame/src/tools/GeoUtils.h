/*
	Author: Shawn Matthews
	Date:   Feb 09, 2018
*/

#pragma once

#include "graphics/MeshData.h"
#include "glm_math.h"

namespace GeoUtils {

	graphics::MeshData CreateSphere(float radius, int tessalation = 2);
	graphics::MeshData CreateCube(const glm::vec3& offset, const glm::vec3& halfExtents, const glm::vec4& color);
	graphics::MeshData CreateCone(const float height, const float angle, const uint16_t tesselation = 2);

	void RecalculateNormals(graphics::MeshData& mesh);
	inline glm::vec3 CalculateSurfaceNormal(const glm::vec3& a, const glm::vec3& b, const glm::vec3& c) {
		return glm::cross(b - a, c - a);
	}
}