/*
	Author: Shawn Matthews
	Date: Jan 28, 2018
	Description:
		Handles creating a mesh from a collection of voxels. This is translated / modifed from a java sample found
		at https://github.com/roboleary/GreedyMesh/blob/master/src/mygame/Main.java
*/
#include "VoxelMesher.h"

#include <tuple>

#include "graphics/VertexLayouts.h"

#include "Logger.h"

#define LOG_MESH_GENERATION 1

#define CORRECTION_EPSILON 0.0001f;

namespace tools {

	using namespace graphics;
	using namespace gameplay;

	bool __CompareQuads(float x0, float y0, float w0, float h0, float x1, float y1, float w1, float h1) {
		if (y0 != y1) return y0 < y1;
		if (x0 != x1) return x0 < x1;
		if (w0 != w1) return w0 > w1;
		return h0 >= h1;
	}

	MeshData VoxelMesher::Generate(const VoxelChunk * chunk)
	{
#if LOG_MESH_GENERATION
		double startTime = glfwGetTime();
#endif

		struct QuadFace {
			uint8_t    Face;
			glm::vec3 P1;
			glm::vec3 P2;
			glm::vec3 P3;
			glm::vec3 P4;
			VoxelFace  Dir;
			int        TileWidth;
			int        TileHeight;
			bool       Backface;
		};

		std::vector<QuadFace> workingFaces = std::vector<QuadFace>();
		workingFaces.reserve(1024);

#if LOG_MESH_GENERATION
		double faceGenTime{ 0 }, faceGenStart{ 0 }, faceGenEnd{ 0 };
#endif

			/// The current location along the V axis
		int vLoc, 
			/// The current location along the U axis
			uLoc, 
			/// A height offset used when calculating height of quads
			hOff, 
			/// The width of the currently generating quad
			w, 
			/// The height of the currently generating quad
			h,
			/// One of two axis indexes for moving along the current plane (0 = x, 1 = y, 2 = z)
			u, 
			/// Two of two axis indexes for moving along the current plane (0 = x, 1 = y, 2 = z)
			v, 
			faceIx;

		VoxelFace side = EAST;

		glm::ivec3 position;
		glm::ivec3 faceOffset;
		glm::ivec3 deltaU;
		glm::ivec3 deltaV;
		glm::vec3 corrU;
		glm::vec3 corrV;

		uint8_t workingMask[CHUNK_LAYERSIZE];
		memset(workingMask, 0, sizeof(uint8_t) * CHUNK_LAYERSIZE);
		uint8_t face1, face2;

		// Generate the mask and quads for the entire block
		for (bool backFace = true, b = false; b != backFace; backFace = backFace && b, b = !b) {
			// Sweeps over 3 dimensions
			for (int dim = 0; dim < 3; dim++) {
				// Determine our lateral axes to the plane we are working on
				u = (dim + 1) % 3;
				v = (dim + 2) % 3;

				// Reset the position and face offset
				position.x = position.y = position.z = 0;
				faceOffset.x = faceOffset.y = faceOffset.z = 0;

				// Basically if we aren't in backface mode we need to offset our quads by 1 unit along the face
				// axis because math
				faceOffset[dim] = backFace ? 0 : 1;

				// East/west axis select (X axis)
				if (dim == 0)      { side = (backFace ? EAST : WEST); }
				// North/south axis select (Y axis)
				else if (dim == 1) { side = (backFace ? NORTH : SOUTH); }
				// Top/Bottom axis select (Z axis)
				else if (dim == 2) { side = (backFace ? TOP : BOTTOM); }

				for (position[dim] = 0; position[dim] < CHUNK_SIZE; position[dim]++) {
					faceIx = 0;

#if LOG_MESH_GENERATION
					faceGenStart = glfwGetTime();
#endif

					// Mask generation for layer
					for (position[v] = 0; position[v] < CHUNK_SIZE; position[v]++) {
						for (position[u] = 0; position[u] < CHUNK_SIZE; position[u]++) {
							// Cool, the chunker handles all the visibility tests
							workingMask[faceIx++] = chunk->IsVisible(position, side) ? chunk->GetLocalID(position) : 0;
						}
					}

#if LOG_MESH_GENERATION
					faceGenEnd = glfwGetTime();

					faceGenTime += faceGenEnd - faceGenStart;
#endif

					// Mesh Generation
					faceIx = 0;

					// Iterate along the u portion of our mask
					for (uLoc = 0; uLoc < CHUNK_SIZE; uLoc++) {
						// Iterate along the v portion
						for (vLoc = 0; vLoc < CHUNK_SIZE;) {
							// If the face has data
							if (workingMask[faceIx] != 0) {
								/*
									+-+===+
									|x|x|x|y x
									+-+===+
									 x x x x x
								*/

								// Get the width of the tile by moving along the mask and looking for differing faces
								for (w = 1; vLoc + w < CHUNK_SIZE && workingMask[faceIx + w] != 0 && workingMask[faceIx + w] == workingMask[faceIx]; w++) {}

								// Now we get the height
								bool done = false;
								// We start with a height of 1, max out at the edge of the chunk
								for (h = 1; uLoc + h < CHUNK_SIZE; h++) {
									// For each tile along the height, check all of the tiles in the row

									/*
									+-+===+
									|x|x|x|y x
									|x|x|x|x x
									+=====+
									*/

									for (hOff = 0; hOff < w; hOff++) {
										// If there are any differing tiles, break
										if (workingMask[faceIx + hOff + h * CHUNK_SIZE] == 0 || workingMask[faceIx + hOff + h * CHUNK_SIZE] != workingMask[faceIx]) {
											done = true; 
											break; 
										}
									}
									if (done) break;
								}

								// Our starting point becomes our i and j vectors along the face (the primary axis is already set)
								position[u] = vLoc;
								position[v] = uLoc;

								// reset du and dv
								deltaU.x = deltaU.y = deltaU.z = 0;
								deltaV.x = deltaV.y = deltaV.z = 0;
								corrU.x = corrU.y = corrU.z = 0;
								corrV.x = corrV.y = corrV.z = 0;
								// Our du value is set to the width along our face u and the dv the height
								deltaU[u] = w;
								deltaV[v] = h;
								corrU[u] = CORRECTION_EPSILON;
								corrV[v] = CORRECTION_EPSILON;

								// Push back the resulting face
								QuadFace resultFace = { workingMask[faceIx],
									(glm::vec3)(faceOffset + position) - corrU - corrV,
									(glm::vec3)(faceOffset + position + deltaU) + corrU - corrV,
									(glm::vec3)(faceOffset + position + deltaU + deltaV) + corrU + corrV,
									(glm::vec3)(faceOffset + position + deltaV) - corrU + corrV,
									side,
									w, h, backFace };
								workingFaces.push_back(resultFace);

								// Zero the mask
								for (int ix = 0; ix < h; ++ix) {
									for (hOff = 0; hOff < w; ++hOff) workingMask[faceIx + hOff + ix * CHUNK_SIZE] = 0;
								}

								// We move our vloc over by our height and add the number of faces handled to the 
								// face indexer
								vLoc += w;
								faceIx += w;
							}
							else {
								// The face was zero, pass a single tile
								vLoc++;
								faceIx++;
							}
						}
					}
				}
			}
		}
	
		graphics::VertexPositionNormalTexture3D *verts = new VertexPositionNormalTexture3D[workingFaces.size() * 4];
		uint16_t* indices = new uint16_t[workingFaces.size() * 6];

		uint16_t size = workingFaces.size();
		const QuadFace *workingFacePtr = workingFaces.data();

		struct quad {
			glm::vec2 tl, tr, bl, br;

			quad() {}
			quad(float l, float r, float t, float b) :
				tl({ l, t }),
				tr({ r, t }),
				bl({ l, b }),
				br({ r, b }) {}

			void set(float l, float r, float t, float b) {
				tl.x = bl.x = l;
				tr.x = br.x = r;
				tl.y = tr.y = t;
				bl.y = br.y = b;
			}

			void flipHor() {
				glm::vec2 temp = tl;
				tl = tr;
				tr = temp;
				temp = bl;
				bl = br;
				br = temp;
			}

			void flipVert() {
				glm::vec2 temp = tl;
				tl = bl;
				bl = temp;
				temp = br;
				br = tr;
				tr = temp;
			}

			void rotL() {
				static glm::mat2 rot = {
					0, -1,
					1,  0
				};
				tl = tl * rot;
				bl = bl * rot;
				tr = tr * rot;
				br = br * rot;
			}
			
			void rotR() {
				static glm::mat2 rot = {
					0,  1,
					-1,  0
				};
				tl = tl * rot;
				bl = bl * rot;
				tr = tr * rot;
				br = br * rot;
			}
		};

		quad uvs;

		/*
			ix:  Index of face
			ixi: Start index of quad
			ixv: Start vertex of quad
		*/
		float texId;
		for (uint16_t ix = 0, ixi = 0, ixv = 0; ix < size; ix++, ixi+=6, ixv+=4) {
			QuadFace face = workingFacePtr[ix];

			// Calculate the face normal
			glm::vec3 norm = glm::normalize(glm::cross((glm::vec3)(face.P2 - face.P1), (glm::vec3)(face.P3 - face.P1))) *
				(face.Backface ? -1.0f : 1.0f);

			texId = BlockRegistry::GetBlockFaceId(face.Face, face.Dir) / 255.0f;

			verts[ixv + 0].Position = face.P1;
			verts[ixv + 0].Normal   = norm;
			verts[ixv + 1].Position = face.P4;
			verts[ixv + 1].Normal = norm;
			verts[ixv + 2].Position = face.P2;
			verts[ixv + 2].Normal = norm;
			verts[ixv + 3].Position = face.P3;
			verts[ixv + 3].Normal = norm;
						
			uvs.set(0.0f, 1.0f * face.TileHeight, 0.0f, 1.0f * face.TileWidth);
			
			switch (face.Dir) {
				case NORTH:
				case SOUTH:
					uvs.flipVert();
					break;
				case TOP:
					break;
				case BOTTOM:
					break;
				case EAST:
					uvs.rotR();
					break;
				case WEST:
					uvs.rotR();
					break;
			}

			verts[ixv + 0].Texture = glm::vec3(uvs.tl, texId);
			verts[ixv + 1].Texture = glm::vec3(uvs.tr, texId);
			verts[ixv + 2].Texture = glm::vec3(uvs.bl , texId);
			verts[ixv + 3].Texture = glm::vec3(uvs.br, texId);

			if (face.Backface) {
				indices[ixi + 0] = ixv + 2;
				indices[ixi + 1] = ixv + 0;
				indices[ixi + 2] = ixv + 1;
				indices[ixi + 3] = ixv + 1;
				indices[ixi + 4] = ixv + 3;
				indices[ixi + 5] = ixv + 2;
			}
			else {
				indices[ixi + 0] = ixv + 2;
				indices[ixi + 1] = ixv + 3;
				indices[ixi + 2] = ixv + 1;
				indices[ixi + 3] = ixv + 1;
				indices[ixi + 4] = ixv + 0;
				indices[ixi + 5] = ixv + 2;
			}
		}

		MeshData result;
		result.Initialize<VertexPositionNormalTexture3D>(verts, size * 4, indices, size * 6, &VertPosNormTex3D);

#if LOG_MESH_GENERATION
		double endTime = glfwGetTime();
		FILE_LOG(logINFO) << "Generated mesh data in " << (endTime - startTime) << " seconds (" << faceGenTime << "s for faces)";
#endif

		return result;
	}
}