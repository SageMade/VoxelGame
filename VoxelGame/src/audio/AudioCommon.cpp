/*
	Author: Shawn Matthews
	Date:   Feb 11, 2018
*/
#include "AudioCommon.h"

#include <fmod\fmod.hpp>
#include <fmod\fmod_errors.h>

#include "Logger.h"

/*
	Handles logging and FMOD erros along with their filename and line number
*/
void FMODErr(FMOD_RESULT result, const char * filename, int line) {
	if (result != FMOD_OK) {
		FILE_LOG(logWARNING) << "FMOD failed in " << filename << " @ " << line << ":\n\t" << FMOD_ErrorString(result);
	}
}