/*
	Author: Shawn Matthews
	Date:   Feb 11, 2018
*/
#pragma once

#include <fmod\fmod_common.h>

/*
Handles logging and FMOD erros along with their filename and line number
*/
void FMODErr(FMOD_RESULT result, const char* filename, int line);

/// Macro for in-line error checking of FMOD functions
#define ERRCHECK(_result) FMODErr(_result, __FILE__, __LINE__)