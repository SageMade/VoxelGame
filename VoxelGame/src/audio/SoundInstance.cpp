/*
	Author: Shawn Matthews
	Date:   Feb 10, 2018
*/

#include "SoundInstance.h"

#include "AudioCommon.h"

namespace audio {

	SoundInstance::~SoundInstance() {
		// Stop the channel so FMOD knows to collect it
		ERRCHECK(myChannel->stop());
		myChannel = nullptr;
	}

	void SoundInstance::Play() {
		ERRCHECK(myChannel->setPaused(false));
	}

	void SoundInstance::Pause() {
		ERRCHECK(myChannel->setPaused(true));
	}

	void SoundInstance::SetLoopingEnabled(bool enabled) {
		// Basically we need to preserve mode except for the loop mode
		FMOD_MODE mode = 0;
		ERRCHECK(myChannel->getMode(&mode));
		// Take the and of mode and the inverse of the loop bits             // 1011 & ~(0110) --> 1011 & 1001 --> 1001
		mode = mode & ~(FMOD_LOOP_NORMAL | FMOD_LOOP_OFF | FMOD_LOOP_BIDI);
		// Bitwise or to set the loop bit
		mode = mode | enabled ? FMOD_LOOP_NORMAL : 0;                        // 1001 | 0100 --> 1101
		// Update channel's mode
		ERRCHECK(myChannel->setMode(mode));
	}

	void SoundInstance::TogglePlaying() {
		bool paused = false;
		ERRCHECK(myChannel->getPaused(&paused));
		ERRCHECK(myChannel->setPaused(!paused));
	}

	void SoundInstance::Stop() {
		// Pause the sound and reset playback head
		ERRCHECK(myChannel->setPaused(true));
		ERRCHECK(myChannel->setPosition(0, FMOD_TIMEUNIT_MS));
	}

	void SoundInstance::Restart() {
		ERRCHECK(myChannel->setPosition(0, FMOD_TIMEUNIT_MS));
	}

	bool SoundInstance::IsPlaying() const {
		bool paused = false;
		ERRCHECK(myChannel->getPaused(&paused));
		return !paused;
	}

	bool SoundInstance::IsLoopingEnabled() const {
		FMOD_MODE mode = 0;
		ERRCHECK(myChannel->getMode(&mode));
		return mode & FMOD_LOOP_NORMAL;
	}

	glm::vec2 SoundInstance::GetMinMaxDistance() const {
		glm::vec2 result = glm::vec2();
		ERRCHECK(myChannel->get3DMinMaxDistance(&result.x, &result.y));
		return result;
	}

	void SoundInstance::SetPitch(const float pitch) {
		ERRCHECK(myChannel->setPitch(pitch));
	}

	void SoundInstance::SetPan(const float pan) {
		ERRCHECK(myChannel->setPan(pan));
	}

	void SoundInstance::SetVolume(const float volume) {
		ERRCHECK(myChannel->setVolume(volume));
	}

	void SoundInstance::SetMinMaxDistance(const glm::vec2 & minMax) {
		ERRCHECK(myChannel->set3DMinMaxDistance(minMax.x, minMax.y));
	}

	void SoundInstance::SetRolloff(RolloffFunc func) {
		FMOD_MODE mode = 0;
		ERRCHECK(myChannel->getMode(&mode));
		mode = (mode & ~ROLLOFF_MASK) | func;
		ERRCHECK(myChannel->setMode(mode));
	}

	void SoundInstance::Set3D(const glm::vec3 & pos, const glm::vec3 & vel) {
		ERRCHECK(myChannel->set3DAttributes((FMOD_VECTOR*)&pos, (FMOD_VECTOR*)&vel));
	}

	float SoundInstance::Length() const {
		uint32_t length = 0;
		// Need to introspect the underlying sound instead of the channel
		FMOD::Sound* sound = nullptr;
		ERRCHECK(myChannel->getCurrentSound(&sound));
		ERRCHECK(sound->getLength(&length, FMOD_TIMEUNIT_MS));
		// We divide by 1000 to get back into seconds
		return length / 1000.0f;
	}
	
}
