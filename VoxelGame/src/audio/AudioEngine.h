/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#pragma once

#include <vector>
#include <unordered_map>
#include <string>

#include "glm_math.h"

#include "tools/Default.h"

#include <fmod\fmod.hpp>
#include <fmod\fmod_common.h>

#include "AudioCommon.h"
#include "SoundInfo.h"
#include "SoundInstance.h"

namespace audio {
	
	class SoundSource;

	/*
		Main system for handling audio loading and playback. Supports both 'Fire and Forget' as well as managed
		sounds
	*/
	class AudioEngine {
	public:

		AudioEngine();
		~AudioEngine();

		/*
			Handles actually updating the audio system.
			Call once per frame or once per update
		*/
		void Update();

		/*
			Sets the listener's position in 3D space in meters
		*/
		void SetListenerPos(const glm::vec3& pos);
		/*
			Sets the listener's forward facing vector in 3D space, basically along the nose of the listener
		*/
		void SetListenerForward(const glm::vec3& forward);
		/*
			Sets the listener's up vector in 3D space (out of top of head)
		*/
		void SetListenerUp(const glm::vec3& up);
		/*
			Sets both the forward and up vectors at the same time to avoid FMOD errors about
			non-perpendicular vectors
		*/
		void SetListenerAxes(const glm::vec3& forward, const glm::vec3& up);
		/*
			Sets the velocity of the listener in M/S.
			REMEMBER: This needs to be scaled by delta time
		*/
		void SetListenerVelocity(const glm::vec3& velocity);

		/*
			Gets the listener's position in 3D space, in meters
		*/
		glm::vec3 GetListenerPos() const { return myListener.Position; }
		/*
			Gets the normal vector pointing out of the listener's nose
		*/
		glm::vec3 GetListenerForward() const { return myListener.Forward; }
		/*
			Gets the normal vector pointing out the top of the listener's head
		*/
		glm::vec3 GetListenerUp() const { return myListener.Up; }
		/*
			Gets the velocity of the listener in M/S
		*/
		glm::vec3 GetListenerVelocity() const { return myListener.Velocity; }

		/*
			Fire the sound with the given ID and all default parameters
		*/
		void FireSound(const uint32_t soundID);
		/*
			Fire the sound with the given ID and all default parameters, except for pitch which becomes
			sound pitch + pitchShift.
		*/
		void FireSound(const uint32_t soundID, float pitchShift);
		/*
			Fire the sound with the given ID at a position and velocity in 3D space
		*/
		void FireSound(const uint32_t soundID, const glm::vec3& pos, const glm::vec3 & vel);
		/*
			Fire the sound with the given ID at a position and velocity in 3D space, also applying a pitch shift. 
			Final pitch is sound pitch + pitchShift
		*/
		void FireSound(const uint32_t soundID, const glm::vec3& pos, const glm::vec3 & vel, float pitchShift);

		/*
			Fires the sound with the given name with all default parameters
		*/
		inline void FireSound(const std::string& name) {
			int soundID = myNamedSoundLocators[name];
			if (soundID != -1)
				FireSound(soundID);
		}
		/*
			Fires the sound with the given name with all default parameters, except for pitch which becomes
			sound pitch + pitchShift.
		*/
		inline void FireSound(const std::string& name, float pitchShift) {
			int soundID = myNamedSoundLocators[name];
			if (soundID != -1)
				FireSound(soundID, pitchShift);
		}
		/*
			Fires the sound with the given name at a position and velocity in 3D space
		*/
		inline void FireSound(const std::string& name, const glm::vec3& pos, const glm::vec3 & vel) {
			int soundID = myNamedSoundLocators[name];
			if (soundID != -1)
				FireSound(soundID, pos, vel);
		}
		/*
			Fires the sound with the given name at a position and velocity in 3D space, also applying a pitch shift. 
			Final pitch is sound pitch + pitchShift
		*/
		inline void FireSound(const std::string& name, const glm::vec3& pos, const glm::vec3 & vel, float pitchShift) {
			int soundID = myNamedSoundLocators[name];
			if (soundID != -1)
				FireSound(soundID, pos, vel, pitchShift);
		}

		/*
			Creates a new instance of a sound effect for the sound given by ID
		*/
		SoundInstance* CreateInstance(const uint32_t id);
		/*
			Creates a new instance of a sound effect for a named sound given by name
		*/
		inline SoundInstance* CreateInstance(const std::string& name) {
			int soundID = myNamedSoundLocators[name];
			if (soundID != -1)
				return CreateInstance(soundID);
			else
				return nullptr;
		}

		/*
			Gets the ID of a named sound, or -1 if none exists
		*/
		int GetNamedSoundID(const std::string& name) { return myNamedSoundLocators[name]; }

		/*
			Unloads the sound with the given name
		*/
		void UnloadNamedSource(const std::string& name);

		/*
			Creates a new sound source from the given file name, by default will be a 3D sting, but can be changed in parameters
		*/
		uint32_t CreateSource(const char* fileName, SoundGroup group = SoundStings, bool is3D = true, bool isStreamed = false);
		/*
			Creates a new sound source from the given file name and adds a name entry for it, by default will be a 
			3D sting, but can be changed in parameters
		*/
		uint32_t CreateNamedSource(const char* fileName, const std::string& name, SoundGroup group = SoundStings, bool is3D = true, bool isStreamed = false);

		/*
			Creates a new sound source from the given sound info
		*/
		uint32_t CreateSource(const SoundInfo& info);
		/*
		Creates a new sound source from the given sound info and adds a name entry for it
		*/
		uint32_t CreateNamedSource(const std::string& name, const SoundInfo& info);

		/*
			Replace the source audio on an audio instance with the one with the given ID
		*/
		void SetInstanceSource(SoundInstance* instance, const uint32_t source);
		/*
			Replace the source audio on an audio instance with the one with the given Name
		*/
		void SetInstanceSource(SoundInstance* instance, const std::string& sourceName) {
			int soundID = myNamedSoundLocators[sourceName];
			if (soundID != -1)
				return SetInstanceSource(instance, soundID);

		}

		/*
			Sets the volume for a sound group
		*/
		void SetGroupVolume(const SoundGroup group, const float volume);
		/*
			Gets the volume for a sound group
		*/
		float GetGroupVolume(const SoundGroup group);

	private:
		friend class SoundSource;

		// Anonymous struct for listener data
		struct {
			glm::vec3 Position = glm::vec3();
			glm::vec3 Forward  = glm::vec3(1.0f, 0.0f, 0.0f);
			glm::vec3 Velocity = glm::vec3();
			glm::vec3 Up       = glm::vec3(0.0f, 0.0f, 1.0f);
		} myListener;

		// Simple structure for manipulating channel groups, may be expanded in the future
		struct ChannelGroup {
			FMOD::ChannelGroup *Group;
			float               Volume;
		};
		
		// Basic system info and the system handle
		FMOD::System *mySystem;
		uint32_t      myVersion;
		FMOD_RESULT   myError;

		// I hate the STL, but at least it's easy
		std::vector<SoundSource*>                 mySoundSources;
		std::unordered_map<std::string, Default<int, -1>> myNamedSoundLocators;

		// Internal function to look up a channel group pointer from enum
		ChannelGroup *__GetGroup(SoundGroup group);

		// Groups are explicitly defined ATM
		ChannelGroup myMusicGroup;
		ChannelGroup myStingGroup;

		// Internal function for updating the listener
		void __UpdateListener();
	};

}