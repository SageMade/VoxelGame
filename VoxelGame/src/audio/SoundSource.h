/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/
#pragma once

namespace audio { class SoundSource; }

#include "AudioEngine.h"
#include "SoundInstance.h"

#include <fmod\fmod.hpp>
#include <fmod\fmod_common.h>

namespace audio {

	/*
		Represents a sound source, basically a wrapper around FMOD::Sound to be used
		internally by the audio engine
	*/
	class SoundSource {
		public:
			friend class AudioEngine;

			~SoundSource() {}

		private:
			SoundSource() {}

			FMOD::Sound *mySound;
			SoundGroup   myGroup;
			uint32_t     myPriority;
	};

}