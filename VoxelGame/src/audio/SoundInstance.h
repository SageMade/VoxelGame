/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/
#pragma once

#include <fmod\fmod.hpp>
#include <fmod\fmod_common.h>

#include "glm_math.h"

namespace audio {

	// Forward declare the audio engine so we can befriend it later
	class AudioEngine;

	// Povide alternative names for the FMOD rollof functions
	enum RolloffFunc {
		Inverse        = FMOD_3D_INVERSEROLLOFF,
		Linear         = FMOD_3D_LINEARROLLOFF,
		LinearSquare   = FMOD_3D_LINEARSQUAREROLLOFF,
		InverseTapered = FMOD_3D_INVERSETAPEREDROLLOFF,
		Custom         = FMOD_3D_CUSTOMROLLOFF,

		ROLLOFF_MASK   = Inverse | Linear | LinearSquare | InverseTapered | Custom
	};

	/*
		Represents a sound instance to be managed by gameplay logic. 
	*/
	class SoundInstance {
	public:
		~SoundInstance();

	public:  // Mutators
		void Play();
		void Pause();

		/* 
			Sets whether or not this sound should loop
		*/
		void SetLoopingEnabled(bool enabled = true);

		void TogglePlaying();

		/*
			Stop the sound and resets the playhead back to the beginning
		*/
		void Stop();
		/*
			Sets playhead back to the beginning
		*/
		void Restart();

		void SetPitch(const float pitch);
		void SetPan(const float pitch);
		void SetVolume(const float pitch);

		void SetMinMaxDistance(const glm::vec2& minMax);
		/*
			Sets the rolloff function to use for this sound
		*/
		void SetRolloff(RolloffFunc func);

		/*
			Sets to 3D sound position and velocity.
			REMEMBER: velocity should be scaled!
		*/
		void Set3D(const glm::vec3& pos, const glm::vec3& vel = glm::vec3(0));

	public:  // Accessors

		/*
			Gets the length of the underlying sound in seconds
		*/
		float Length() const;

		bool IsPlaying() const;
		bool IsLoopingEnabled() const;

		glm::vec2 GetMinMaxDistance() const;

	private:
		friend class AudioEngine;

		SoundInstance(FMOD::Channel *channel) : myChannel(channel) { }
		
		FMOD::Channel *myChannel;
	};

}