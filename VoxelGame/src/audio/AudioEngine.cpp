/*
	Author: Shawn Matthews
	Date:   Jan 28, 2018
*/

#include "AudioEngine.h"

#include "Tweakables.h"
#include "Logger.h"
#include "SoundSource.h"

#include <fmod/fmod_errors.h>

namespace audio {
		
	AudioEngine::AudioEngine() {
		// Create and init system, get version, etc...
		ERRCHECK(FMOD::System_Create(&mySystem));
		ERRCHECK(mySystem->getVersion(&myVersion));
		ERRCHECK(mySystem->init(AUDIO_MAX_CHANNELS, FMOD_INIT_NORMAL | FMOD_INIT_3D_RIGHTHANDED, nullptr)); // Note the right-handed coords
		ERRCHECK(mySystem->set3DSettings(AUDIO_DOPPLER_SCALE, AUDIO_DISTANCE_FACTOR, AUDIO_ROLLOFF_FACTOR));

		// Make 2 new channel groups (hard coded for now)
		ERRCHECK(mySystem->createChannelGroup("Music", &myMusicGroup.Group));
		ERRCHECK(mySystem->createChannelGroup("Stings", &myStingGroup.Group));

		// Grab their volumes
		ERRCHECK(myMusicGroup.Group->getVolume(&myMusicGroup.Volume));
		ERRCHECK(myStingGroup.Group->getVolume(&myStingGroup.Volume));
	};

	AudioEngine::~AudioEngine() {
		// Determine how many sounds we have and loop over
		uint32_t numSounds = mySoundSources.size();
		for (uint32_t ix = 0; ix < numSounds; ix++) {
			// Release each sound and delete the source
			ERRCHECK(mySoundSources[ix]->mySound->release());
			delete mySoundSources[ix];
		}
		// Clear the vector and map just to be safe
		mySoundSources.clear();
		myNamedSoundLocators.clear();

		// Release the channel groups
		ERRCHECK(myMusicGroup.Group->release());
		ERRCHECK(myMusicGroup.Group->release());

		// Close and release the system
		ERRCHECK(mySystem->close());
		ERRCHECK(mySystem->release());
	}

	void AudioEngine::Update() {
		ERRCHECK(mySystem->update());
	}

	void AudioEngine::SetListenerPos(const glm::vec3 & pos) {
		myListener.Position = pos;
		__UpdateListener();
	}

	void AudioEngine::SetListenerForward(const glm::vec3 & forward) {
		myListener.Forward = glm::normalize(forward);
		__UpdateListener();
	}

	void AudioEngine::SetListenerVelocity(const glm::vec3 & velocity) {
		myListener.Velocity = velocity;
		__UpdateListener();
	}

	void AudioEngine::SetListenerAxes(const glm::vec3 & forward, const glm::vec3 & up) {
		myListener.Forward = glm::normalize(forward);
		myListener.Up      = glm::normalize(up);
		__UpdateListener();
	}

	void AudioEngine::SetListenerUp(const glm::vec3 & up) {
		myListener.Up = glm::normalize(up);
		__UpdateListener();
	}

	/*
		Note that for performance each FireSound is explicitly implemented, no proxying to another function

		TODO: Should not allow looped sounds to be in fire and forget mode
	*/

	void AudioEngine::FireSound(const uint32_t sound) {
		// Grab the source, fire the sound, and let FMOD do the rest
		FMOD::Channel *channel;
		SoundSource *soundInst = mySoundSources[sound];
		ERRCHECK(mySystem->playSound(soundInst->mySound, __GetGroup(soundInst->myGroup)->Group, false, &channel));
	}

	void AudioEngine::FireSound(const uint32_t soundID, float pitchShift) {
		// Grab the source, apply a pitch shift, then fire
		FMOD::Channel *channel;
		SoundSource *soundInst = mySoundSources[soundID];
		ERRCHECK(mySystem->playSound(soundInst->mySound, __GetGroup(soundInst->myGroup)->Group, true, &channel));
		float pitch = 0;
		ERRCHECK(channel->getPitch(&pitch));
		ERRCHECK(channel->setPitch(pitch + pitchShift));
		ERRCHECK(channel->setPaused(false));
	}

	void AudioEngine::FireSound(const uint32_t soundID, const glm::vec3 & pos, const glm::vec3 & vel) {
		// Grab the source, apply 3D attributes, then fire
		FMOD::Channel *channel;
		SoundSource *soundInst = mySoundSources[soundID];
		ERRCHECK(mySystem->playSound(soundInst->mySound, __GetGroup(soundInst->myGroup)->Group, true, &channel));
		ERRCHECK(channel->set3DAttributes((FMOD_VECTOR*)&pos, (FMOD_VECTOR*)&vel));
		ERRCHECK(channel->setPaused(false));
	}

	void AudioEngine::FireSound(const uint32_t soundID, const glm::vec3 & pos, const glm::vec3 & vel, float pitchShift) {
		// Grab the source, apply 3D attributes and a pitch shift, then fire
		FMOD::Channel *channel;
		SoundSource *soundInst = mySoundSources[soundID];
		ERRCHECK(mySystem->playSound(soundInst->mySound, __GetGroup(soundInst->myGroup)->Group, true, &channel));
		float pitch = 0;
		ERRCHECK(channel->getPitch(&pitch));
		ERRCHECK(channel->setPitch(pitch + pitchShift));
		ERRCHECK(channel->set3DAttributes((FMOD_VECTOR*)&pos, (FMOD_VECTOR*)&vel));
		ERRCHECK(channel->setPaused(false));
	}

	SoundInstance * AudioEngine::CreateInstance(const uint32_t id) {
		// Only create an instance if both the sound source and it's underlying sound both exist
		SoundSource *source = mySoundSources[id];
		if (source && source->mySound) {
			FMOD::Channel *channel;
			ERRCHECK(mySystem->playSound(source->mySound, __GetGroup(source->myGroup)->Group, true, &channel));
			ERRCHECK(channel->setPriority(128 + source->myPriority));
			return new SoundInstance(channel);
		}
		else
			return nullptr;
	}

	void AudioEngine::UnloadNamedSource(const std::string & name) {
		// Make sure it exists before we access 'cause STL map is garbo
		if (myNamedSoundLocators.find(name) != myNamedSoundLocators.end()) {
			uint32_t ix = myNamedSoundLocators[name];
			mySoundSources[ix]->mySound->release();
			delete mySoundSources[ix];
			mySoundSources.erase(mySoundSources.begin() + ix);
		}
	}

	uint32_t AudioEngine::CreateSource(const char * fileName, SoundGroup group, bool is3D, bool isStreamed) {
		
		// Make a new sound source
		SoundSource *result = new SoundSource();

		// Either completely load or stream, also pick 2D or 3D mode
		if (!isStreamed)
			ERRCHECK(mySystem->createSound(fileName, is3D ? FMOD_3D : FMOD_2D, 0, &result->mySound));
		else
			ERRCHECK(mySystem->createStream(fileName, is3D ? FMOD_3D : FMOD_2D, 0, &result->mySound));

		// Assign group and default priority
		result->myGroup = group;
		result->myPriority = 1;

		// If 3D is enabled, set the 3D parameters
		if (is3D) {
			result->mySound->set3DMinMaxDistance(1.0f, 500.0f);
			result->mySound->setMode(AUDIO_DEFAULT_3D_ROLLOFF);
		}

		// Grab the index before resizing, push, then return the index
		uint32_t loc = mySoundSources.size();
		mySoundSources.push_back(result);
		return loc;

	}

	uint32_t AudioEngine::CreateNamedSource(const char * fileName, const std::string & name, SoundGroup group, bool is3D, bool isStreamed) {
		// Make the source and add the name
		uint32_t result = CreateSource(fileName, group, is3D, isStreamed);
		myNamedSoundLocators.emplace(name, result);
		return result;
	}

	uint32_t AudioEngine::CreateSource(const SoundInfo & info)
	{
		// We use 128+ for managed sounds so they don't get grabbed
		if (info.Priority > 128)
			throw std::invalid_argument("Priorities over 128 are reserved for system use");

		// Create source, set 3D mode, yadda yadda
		SoundSource *result = new SoundSource;
		if (!info.IsStreamed)
			ERRCHECK(mySystem->createSound(info.Filename.c_str(), info.Is3D ? FMOD_3D : FMOD_2D, 0, &result->mySound));
		else
			ERRCHECK(mySystem->createStream(info.Filename.c_str(), info.Is3D ? FMOD_3D : FMOD_2D, 0, &result->mySound));

		// Grab group and priority from info
		result->myGroup = info.Group;
		result->myPriority = info.Priority;

		// Apply 3D if needed
		if (info.Is3D) {
			result->mySound->set3DMinMaxDistance(info.MinDistance, info.MaxDistance);
			result->mySound->setMode(info.Mode);
		}

		// Return the index after push_back
		uint32_t loc = mySoundSources.size();
		mySoundSources.push_back(result);
		return loc;
	}

	uint32_t AudioEngine::CreateNamedSource(const std::string & name, const SoundInfo & info) {
		// Create source, give it a name, return source
		uint32_t result = CreateSource(info);
		myNamedSoundLocators.emplace(name, result);
		return result;
	}

	void AudioEngine::SetInstanceSource(SoundInstance *instance, const uint32_t id) {
		// Grab the source and only continue if not null
		SoundSource *source = mySoundSources[id];
		if (source && source->mySound) {
			// Cache whether or not the sound is currently playing
			bool play = instance->IsPlaying();
			// Stop and kill the channel
			ERRCHECK(instance->myChannel->stop());
			// Update the channel ptr with a new one with the new sound
			ERRCHECK(mySystem->playSound(source->mySound, __GetGroup(source->myGroup)->Group, true, &instance->myChannel));
			// Set priority on the new channel
			ERRCHECK(instance->myChannel->setPriority(128 + source->myPriority));
			// Copy over our paused value
			ERRCHECK(instance->myChannel->setPaused(!play));
		}
	}

	void AudioEngine::SetGroupVolume(const SoundGroup group, const float volume) {
		switch (group)
		{
		case SoundMusic:
			ERRCHECK(myMusicGroup.Group->setVolume(volume));
			break;
		case SoundStings:
			ERRCHECK(myStingGroup.Group->setVolume(volume));
			break;
		default:
			break;
		}
	}

	float AudioEngine::GetGroupVolume(const SoundGroup group) {
		float result = 0.0f;
		switch (group) {
			case SoundMusic:
				ERRCHECK(myMusicGroup.Group->getVolume(&result));
				break;
			case SoundStings:
				ERRCHECK(myStingGroup.Group->getVolume(&result));
				break;
			default:
				break;
		}
		return result;
	}

	AudioEngine::ChannelGroup* AudioEngine::__GetGroup(SoundGroup group)
	{
		switch (group) {
			case SoundMusic:
				return &myMusicGroup;
			case SoundStings:
				return &myStingGroup;
			default:
				return nullptr;
		}
	}

	void AudioEngine::__UpdateListener() {
		ERRCHECK(mySystem->set3DListenerAttributes(0,
			(FMOD_VECTOR*)&myListener.Position,
			(FMOD_VECTOR*)&myListener.Velocity,
			(FMOD_VECTOR*)&myListener.Forward,
			(FMOD_VECTOR*)&myListener.Up));
	}
	
}
