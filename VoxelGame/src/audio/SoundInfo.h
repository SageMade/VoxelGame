/*
	Author: Shawn Matthews
	Date:   Feb 10, 2018
*/

#pragma once

#include <string>

#include <fmod\fmod.hpp>
#include <fmod\fmod_common.h>

namespace audio {
	
	/*
		Represents the channel group that a sound belongs to
	*/
	enum SoundGroup {
		SoundMusic,
		SoundStings
	};
	
	/*
		Represents all of the data required to construct a new sound instance
	*/
	struct SoundInfo {
		std::string Filename;
		SoundGroup  Group;
		bool        Is3D;
		bool        IsStreamed;
		FMOD_MODE   Mode;
		float       MinDistance;
		float       MaxDistance;
		int         Priority;

		SoundInfo() :
			Filename(""),
			Group(SoundStings),
			Is3D(true),
			IsStreamed(false),
			Mode(FMOD_3D_LINEARROLLOFF),
			MinDistance(1.0f),
			MaxDistance(500.0f),
			Priority(0) {}

		SoundInfo(
			const std::string& fileName,
			SoundGroup         group = SoundStings,
			bool	           is3D = true,
			bool               isStreamed = false,
			FMOD_MODE          mode = FMOD_3D_LINEARROLLOFF,
			float              minDist = 1.0f,
			float              maxDist = 500.0f,
			int                priority = 0) :
			Filename(fileName),
			Group(group),
			Is3D(is3D),
			IsStreamed(isStreamed),
			Mode(mode),
			MinDistance(minDist),
			MaxDistance(maxDist),
			Priority(priority) {}
	};
}