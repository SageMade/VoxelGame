#version 410

layout (location = 0) in vec2 inTexture;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec4 inViewPosition;

layout (location = 0) out vec4 frag_albedo;
layout (location = 1) out vec4 frag_normal;
layout (location = 2) out vec4 frag_viewPos;

uniform sampler2D xSampler;

void main() {
    //(texture(xTextureAtlas, inTexture) + xAdditiveColor * 
    frag_albedo = texture(xSampler, inTexture); //mix(normalFactor, rimLight, rimLight.a) * xColorMult;
    frag_normal = vec4(inNormal, 1.0);
    frag_viewPos = inViewPosition;
}