#version 410

in vec2 TexCoord;

layout (location = 0) out vec4 frag_diffuse;
layout (location = 1) out vec4 frag_specular; 


uniform sampler2D xAlbedo;
uniform sampler2D xNormals;
uniform sampler2D xViewSpace;
uniform sampler2D xShadowMap;

uniform vec2 xWindowSize;

uniform bool xDebugLights;


uniform sampler1D xDiffuseRamp;
uniform sampler1D xSpecularRamp;

uniform float     xDiffuseRampPower;
uniform float     xSpecularRampPower;

struct PointLight {
    vec4  Position;
    vec3  Color;
    float Radius;
};

uniform PointLight xLight;

uniform float xAttenuationFactor;

uniform mat4 xViewToLightSpace;
uniform mat4 xDepthMVP;
uniform mat4 xInvView;

void phong(vec3 viewPos, float alpha, vec3 normal, float visibility, vec4 specColor) {
    vec3 lightViewPos = xLight.Position.xyz;
    vec3 lightVec = lightViewPos - viewPos;
    float dist = length(lightVec);
    vec3 lightDir = lightVec / dist; // removes the distance leaving a normalize vector (direction)
    
    float attenuation = mix(1.0f, smoothstep(xLight.Radius, 0, dist), xAttenuationFactor);

    float NdotL = max(dot(normal, lightDir), 0.0);
    vec3 diffuse =  NdotL * attenuation * xLight.Color * mix(vec3(NdotL), texture(xDiffuseRamp, NdotL).rgb, xDiffuseRampPower);
    vec3 reflectDir = reflect(-lightDir, normal);
    float VdotR = max(dot(normalize(-viewPos), reflectDir), 0.0);
    VdotR = pow(VdotR, specColor.w);
    vec3 specular = mix(vec3(VdotR), texture(xSpecularRamp, VdotR).rgb, xSpecularRampPower) * attenuation * xLight.Color * specColor.rgb;
    
    frag_diffuse = visibility * vec4(diffuse, alpha);
    frag_specular = visibility * vec4(specular, alpha);
    //return vec4(diffuse + specular, 1.0);
}

const vec3 HALF = vec3(0.5);
const vec3 DOUBLE = vec3(2.0);
const float bias = 0.00001;

vec3 GetNormal(vec2 uv) {
    return (texture(xNormals, uv).xyz - HALF) * DOUBLE;
}

void main() {
    vec2 pos = TexCoord;
     
    vec4 viewPos = texture(xViewSpace, pos);
    viewPos /= viewPos.w;

    vec4 shadowPos = xViewToLightSpace * viewPos;
    // Perspective divide
    shadowPos /= shadowPos.w;

    if (shadowPos.x < 0 || shadowPos.x > 1 || 
        shadowPos.y < 0 || shadowPos.y > 1 || 
        shadowPos.z < 0 || shadowPos.z > 1000.0f) {
        discard;
    }

    float visibility = 1.0;
    float shadowDepth = texture( xShadowMap, shadowPos.xy).r;
    if (shadowDepth < shadowPos.z - bias){
        discard;
    }
    vec3 normal = GetNormal(pos);
    vec4 albedo  = texture(xAlbedo, pos);
    // hard code as we do not have material specular maps yet
    phong(viewPos.xyz, albedo.a, normal, visibility, vec4(vec3(0.1), 0.01));
    //}
}