#version 410

layout (location = 0) out vec4 frag_albedo;
layout (location = 1) out vec4 frag_normal;
layout (location = 2) out vec4 frag_viewPos;

in vec2 TexCoord;

uniform sampler2D xAlbedo;
uniform sampler2D xNormals;
uniform sampler2D xViewSpace;

uniform vec2 xResolution;

void main() {
	vec2 off = vec2(1) / xResolution;
	frag_albedo  = (texture(xAlbedo, TexCoord) + texture(xAlbedo, TexCoord + off)) / 2;
	frag_normal  = (texture(xNormals, TexCoord) + texture(xNormals, TexCoord + off)) / 2;
	frag_viewPos = (texture(xViewSpace, TexCoord) + texture(xViewSpace, TexCoord + off)) / 2;
}