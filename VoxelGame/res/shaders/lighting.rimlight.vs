#version 410

layout (location = 0) in vec3 inPosition;

layout (location = 0) out vec2 outScreenPos;

const vec2 OFFSET = vec2(0.5, 0.5);


void main() {
	//vec4 result = xProjection * xView  * xWorld * vec4(inPosition, 1.0f);
	outScreenPos = inPosition.xy*OFFSET + OFFSET;
	gl_Position = vec4(inPosition.xy, 0.0, 1.0);
}