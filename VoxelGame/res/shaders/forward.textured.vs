#version 410

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec2 inTexture; 
layout (location = 2) in vec3 inNormal;

layout (location = 0) out vec2 outTexture;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec4 outViewPosition;

uniform mat4 xWorld;
uniform mat4 xView;
uniform mat4 xProjection;
uniform mat4 xViewProjection;

uniform mat3 xNormalMatrix;

void main() {
	outViewPosition = xView * xWorld * vec4(inPosition, 1.0f);
	gl_Position = xProjection * outViewPosition;

	outNormal = xNormalMatrix * inNormal;
	outTexture = inTexture;
}