#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;

uniform float xNearPlane;
uniform float xFarPlane;

void main() {
	float sampled = texture2D(xSampler, TexCoord).x;
	float linear = (2 * xNearPlane) / (xFarPlane + xNearPlane - sampled * (xFarPlane - xNearPlane));
    frag_color = vec4(vec3(linear), 1.0);
}