#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;
uniform sampler3D xLut;
uniform float     xLutFactor;

void main() {
	vec4 sampledCol = texture(xSampler, TexCoord);
    frag_color = vec4(mix(sampledCol.rgb, texture(xLut, sampledCol.rgb).rgb, xLutFactor), sampledCol.a);
}