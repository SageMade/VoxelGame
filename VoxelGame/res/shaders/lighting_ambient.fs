#version 410

layout (location = 0) in vec2 inScreenPos;

layout (location = 0) out vec4 frag_diffuse;
layout (location = 1) out vec4 frag_specular; 

uniform sampler2D xDepth;
uniform vec4 xAmbientFactor;

void main() {
    frag_diffuse  = xAmbientFactor;
    frag_specular = vec4(0.0);

    gl_FragDepth =  texture(xDepth, inScreenPos).r;
}