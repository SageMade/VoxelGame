#version 410

layout (location = 0) in vec3 inTexture;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec4 inViewPosition;

layout (location = 0) out vec4 frag_albedo;
layout (location = 1) out vec4 frag_normal;
layout (location = 2) out vec3 frag_viewPos;

uniform sampler3D xTextureAtlas;
uniform sampler3D xNormalAtlas;

const vec3 HALF = vec3(0.5);

uniform mat4 xViewWorld;

mat3 cotangent_frame(vec3 N, vec3 p, vec2 uv)
{
    // get edge vectors of the pixel triangle
    vec3 dp1 = dFdx( p );
    vec3 dp2 = dFdy( p );
    vec2 duv1 = dFdx( uv );
    vec2 duv2 = dFdy( uv );
 
    // solve the linear system
    vec3 dp2perp = cross( dp2, N );
    vec3 dp1perp = cross( N, dp1 );
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;
 
    // construct a scale-invariant frame 
    float invmax = inversesqrt( max( dot(T,T), dot(B,B) ) );
    return mat3( T * invmax, B * invmax, N );
}


void main() {
    //(texture(xTextureAtlas, inTexture) + xAdditiveColor * 
    frag_albedo = texture(xTextureAtlas, inTexture); //mix(normalFactor, rimLight, rimLight.a) * xColorMult;
    vec3 sampledNorm = texture(xNormalAtlas, inTexture).xyz;
    sampledNorm = (sampledNorm - HALF) / HALF;
    mat3 tangentFrame = cotangent_frame(inNormal, normalize(inViewPosition).xyz, inTexture.xy);
    sampledNorm = tangentFrame * sampledNorm;
    frag_normal = vec4(normalize(sampledNorm) * HALF + HALF, 1.0);
    frag_viewPos = inViewPosition.xyz / inViewPosition.w;
};