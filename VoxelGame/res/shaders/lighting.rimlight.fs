#version 410

layout (location = 0) in vec2 inScreenPos;

layout (location = 0) out vec4 frag_diffuse;
layout (location = 1) out vec4 frag_specular; 

uniform sampler2D xAlbedo;
uniform sampler2D xNormals;
uniform sampler2D xViewSpace;

uniform sampler1D xRimFactor;

uniform float xRimWidth;
uniform float xRimAlpha;
uniform vec3  xRimColor;

uniform float xRimRampFactor;

vec4 rim(vec3 viewPos, vec3 normal) {
    vec3 viewDir = normalize(-viewPos);
    float intesity = min(max(xRimWidth - max(dot(viewDir, normal), 0.0), 0.0), 1.0);
    return vec4(mix(vec3(intesity), texture(xRimFactor, intesity).rgb, xRimRampFactor) * xRimColor, xRimAlpha);
}

const vec3 HALF = vec3(0.5);
const vec3 DOUBLE = vec3(2.0);

vec3 GetNormal(vec2 uv) {
	return (texture(xNormals, uv).xyz - HALF) * DOUBLE;
}

void main() {
    //(texture(xTextureAtlas, inTexture) + xAdditiveColor * 
    vec3 normal = GetNormal(inScreenPos);
    vec4 albedo = texture(xAlbedo, inScreenPos);
    vec3 viewPos = texture(xViewSpace, inScreenPos).xyz;

    frag_diffuse  = rim(viewPos, normal);
    frag_diffuse.a = min(albedo.a, frag_diffuse.a);
    frag_specular = vec4(0.0);
}