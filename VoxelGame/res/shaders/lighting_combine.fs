#version 410

in vec2 TexCoord;

layout (location = 0) out vec4 frag_color; 

uniform sampler2D xAlbedo;
uniform sampler2D xDiffuse;
uniform sampler2D xSpecular;

uniform float     xSpecularMult;
uniform float     xDiffuseMult;

void main() {	
    vec4 albedo    = texture(xAlbedo,    TexCoord);
    vec4 diffuse   = texture(xDiffuse,   TexCoord);
    vec4 specular  = texture(xSpecular,  TexCoord);

    vec3 gamma = vec3(1.0);//vec3(1.0 / 2.2);
    vec4 combined = albedo * vec4(mix(vec3(1), diffuse.rgb, xDiffuseMult) + specular.rgb * xSpecularMult, diffuse.a);

    frag_color    = vec4(pow(combined.xyz, gamma), combined.a);
    frag_color.a  = albedo.a;
}