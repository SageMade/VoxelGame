#version 410

layout (location = 0) in vec3 inPos;

out vec2 TexCoord;

const vec2 OFFSET = vec2(0.5, 0.5);

void main() {
	TexCoord = inPos.xy * OFFSET + OFFSET;
	gl_Position = vec4(inPos.xy, 0.0, 1.0);	
}