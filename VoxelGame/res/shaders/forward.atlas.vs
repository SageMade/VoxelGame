#version 410

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inTexture; 
layout (location = 2) in vec3 inNormal;

layout (location = 0) out vec3 outTexture;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec4 outViewPosition;

uniform mat4 xViewWorld;
uniform mat4 xProjection;
uniform mat4 xViewProjection;

uniform mat3 xNormalMatrix;

void main() {
	outViewPosition = xViewWorld * vec4(inPosition, 1.0f);
	gl_Position = xProjection * outViewPosition;

	outNormal = xNormalMatrix * inNormal;
	outTexture = inTexture;
}