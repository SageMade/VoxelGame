#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;

uniform vec3 xMin;
uniform vec3 xRange;

void main() {
	vec4 sampled = texture2D(xSampler, TexCoord);
    frag_color = vec4((sampled.xyz - xMin) / xRange, sampled.a);
}