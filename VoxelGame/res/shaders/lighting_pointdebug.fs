#version 410

layout (location = 0) in vec2 inScreenPos;

layout (location = 0) out vec4 frag_color;

uniform sampler2D xAlbedoSpec;
uniform sampler2D xNormals;
uniform sampler2D xViewSpace;

uniform vec2 xWindowSize;

uniform mat4 xView;

struct PointLight {
	vec3  Position;
	vec3  DiffuseColor;
	vec3  SpecularColor;
	float Constant;
	float Linear;
	float Quadratic;
};

uniform PointLight xLight;

void main() {
	vec2 pos = gl_FragCoord.xy / xWindowSize;
    frag_color   = vec4(xLight.DiffuseColor, 1);
}