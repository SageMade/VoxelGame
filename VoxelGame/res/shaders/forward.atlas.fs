#version 410

layout (location = 0) in vec3 inTexture;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec4 inViewPosition;

layout (location = 0) out vec4 frag_albedo;
layout (location = 1) out vec4 frag_normal;
layout (location = 2) out vec4 frag_viewPos;

uniform sampler3D xTextureAtlas;

const vec3 HALF = vec3(0.5);

void main() {
    //(texture(xTextureAtlas, inTexture) + xAdditiveColor * 
    frag_albedo = texture(xTextureAtlas, inTexture); //mix(normalFactor, rimLight, rimLight.a) * xColorMult;
    frag_normal = vec4(normalize(inNormal) * HALF + HALF, 1.0);
    frag_viewPos = inViewPosition;
}