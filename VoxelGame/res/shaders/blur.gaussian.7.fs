#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;

uniform vec2 xResolution;
//uniform vec2 xDirection;

vec4 blur(vec2 uv, vec2 res, vec2 dir) {
	//0.00598    0.060626    0.241843    0.383103    0.241843    0.060626    0.00598
	vec4 result = vec4(0);

  	vec2 off1 = vec2(1.41176) * dir;
  	vec2 off2 = vec2(3.29411) * dir;
	vec2 off3 = vec2(5.17647) * dir;
	result += texture2D(xSampler, uv) * 0.383103;
	result += texture2D(xSampler, uv + (off1 / res)) * 0.241843;
	result += texture2D(xSampler, uv - (off1 / res)) * 0.241843;
	result += texture2D(xSampler, uv + (off2 / res)) * 0.060626;
	result += texture2D(xSampler, uv - (off2 / res)) * 0.060626;
	result += texture2D(xSampler, uv + (off3 / res)) * 0.00598;
	result += texture2D(xSampler, uv - (off3 / res)) * 0.00598;
	return result;
}

void main() {
	frag_color = (
		blur(TexCoord, xResolution, vec2(1, 0)) + 
		blur(TexCoord, xResolution, vec2(1, 0))) / 2;
}