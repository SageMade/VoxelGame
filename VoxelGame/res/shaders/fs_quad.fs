#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;

void main() {
    frag_color = texture2D(xSampler, TexCoord);
}