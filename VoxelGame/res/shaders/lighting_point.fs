#version 410

layout (location = 0) in vec2 inScreenPos;
layout (location = 1) in vec4 inViewPos;

layout (location = 0) out vec4 frag_diffuse;
layout (location = 1) out vec4 frag_specular; 


uniform sampler2D xAlbedo;
uniform sampler2D xNormals;
uniform sampler2D xViewSpace;

uniform vec2 xWindowSize;

uniform bool xDebugLights;

uniform mat4 xView;

uniform sampler1D xDiffuseRamp;
uniform sampler1D xSpecularRamp;

uniform float     xDiffuseRampPower;
uniform float     xSpecularRampPower;

struct PointLight {
    vec4  Position;
    vec3  Color;
    float Radius;
};

uniform PointLight xLight;

void phong(vec3 viewPos, float alpha, vec3 normal, vec4 specColor) {
    vec3 lightViewPos = xLight.Position.xyz;
    vec3 lightVec = lightViewPos - viewPos;
    float dist = length(lightVec);
    vec3 lightDir = lightVec / dist; // removes the distance leaving a normalize vector (direction)
    
    float attenuation = smoothstep(xLight.Radius, 0, dist);

    float NdotL = max(dot(normal, lightDir), 0.0);
    vec3 diffuse =  NdotL * attenuation * xLight.Color * mix(vec3(NdotL), texture(xDiffuseRamp, NdotL).rgb, xDiffuseRampPower);
    vec3 reflectDir = reflect(-lightDir, normal);
    float VdotR = max(dot(normalize(-viewPos), reflectDir), 0.0);
    VdotR = pow(VdotR, specColor.w);
    vec3 specular = mix(vec3(VdotR), texture(xSpecularRamp, VdotR).rgb, xSpecularRampPower) * attenuation * xLight.Color * specColor.rgb;
    
    frag_diffuse = vec4(diffuse, alpha);
    frag_specular = vec4(specular, alpha);
    //return vec4(diffuse + specular, 1.0);
}

const vec3 HALF = vec3(0.5);
const vec3 DOUBLE = vec3(2.0);

vec3 GetNormal(vec2 uv) {
	return (texture(xNormals, uv).xyz - HALF) * DOUBLE;
}

void main() {
    vec2 pos = gl_FragCoord.xy / xWindowSize;

	vec3 viewPos = texture(xViewSpace, pos).xyz;

    if (viewPos.z < inViewPos.z)
        discard;
    
	//(texture(xTextureAtlas, inTexture) + xAdditiveColor * 
	vec3 normal = GetNormal(pos);
	vec4 albedo  = texture(xAlbedo, pos);

	// hard code as we do not have material specular maps yet
	phong(viewPos, albedo.a, normal, vec4(vec3(0.1), 0.01));
}