#version 410

layout (location = 0) in vec3 inPosition;

uniform mat4 xViewWorld;
uniform mat4 xProjection;
uniform mat4 xViewProjection;

void main() {
	gl_Position = xProjection * xViewWorld * vec4(inPosition, 1.0f);
}