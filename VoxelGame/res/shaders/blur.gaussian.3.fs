#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;

uniform vec2 xResolution;
//uniform vec2 xDirection;

vec4 blur(vec2 uv, vec2 res, vec2 dir) {
	vec4 result = vec4(0);
	vec2 off = vec2(1.3333) * dir;
	result += texture2D(xSampler, uv) * 0.44198;
	result += texture2D(xSampler, uv + (off / res)) * 0.27901;
	result += texture2D(xSampler, uv - (off / res)) * 0.27901;
	return result;
}

void main() {
	frag_color = (
		blur(TexCoord, xResolution, vec2(1, 0)) + 
		blur(TexCoord, xResolution, vec2(1, 0))) / 2;
}