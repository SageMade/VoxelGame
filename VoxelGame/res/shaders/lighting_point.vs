#version 410

layout (location = 0) in vec3 inPosition;

layout (location = 0) out vec2 outScreenPos;
layout (location = 1) out vec4 outViewPos;

uniform mat4 xWorld;
uniform mat4 xView;
uniform mat4 xProjection;
uniform mat4 xViewProjection;

void main() {
	outViewPos = xView  * xWorld * vec4(inPosition, 1.0f);
	vec4 result  = xProjection * outViewPos;
	outScreenPos = result.xy * result.w;
	gl_Position  = result;
}