#version 410

in vec2 TexCoord;

out vec4 frag_color;

uniform sampler2D xSampler;

uniform float xBloomThreshold;

void main() {
	vec4 sampled = texture2D(xSampler, TexCoord);
	frag_color = clamp((sampled - vec4(xBloomThreshold)) / (vec4(1) - vec4(xBloomThreshold)), vec4(0), vec4(1));
	frag_color.a = sampled.a;
}